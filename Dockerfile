FROM node:alpine as client-build
RUN apk --no-cache add git

ADD --chown=node:node ["client/package.json","client/package-lock.json", "/app/"]
USER node
WORKDIR /app
RUN npm ci
ADD --chown=node:node ["client","/app/"]
RUN npm run build

FROM node:alpine
RUN apk --no-cache add git
RUN apk --no-cache add chromium
USER node

ADD --chown=node:node ["server/package.json","server/package-lock.json","/app/"]
WORKDIR /app

RUN npm ci

COPY --from=client-build --chown=node:node /app/dist /app/public
ADD --chown=node:node ["server", "/app"]
RUN npm run build
CMD ["npm", "run", "start"]
module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ["plugin:vue/essential"],
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-async-in-computed-properties": "off",
    "camelcase": "off",
    "no-labels": "off",
    "vue/no-use-v-if-with-v-for": "off",
    "eqeqeq": "off",
    "semi":  [2, "always"]
  },
  parserOptions: {
    parser: "babel-eslint"
  }
};

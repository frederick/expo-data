const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin');

module.exports = {
  chainWebpack: config => {
    config.module.rule('eslint').use('eslint-loader').options({
      fix: true
    });
    config.plugins.delete('prefetch');
  },
  configureWebpack: config => {
    config.plugins.push(new MonacoWebpackPlugin({
      languages: ['pgsql', 'html'],
      output: 'monaco'
    }));
		//config.devtool = 'eval';
  },
  devServer: {
    proxy: {
      '^/expo-data/api': {
        target: 'http://localhost:3000'
      },
      '^/expo-data/graphql': {
        target: 'http://localhost:3000'
      }
    }
  },

  publicPath: '/expo-data'
};

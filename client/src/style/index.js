import Vue from 'vue';
import './bootstrap.scss';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue';
require('billboard.js/dist/billboard.css');
require('@mdi/font/scss/materialdesignicons.scss');

Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);

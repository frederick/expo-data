import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store/';
import Currency from '@/components/Currency.vue';
import DecimalInput from '@/components/DecimalInput.vue';
import VueApollo from 'vue-apollo';
import apolloClient from './client/apollo';
import { cur, dec } from '@/utils/decimals';
import moment from 'moment';

import VCalendar from 'v-calendar';

Vue.use(VueApollo);
let apolloProvider = new VueApollo({
	defaultClient: apolloClient,
	errorHandler(err) {
		Vue.config.errorHandler(err);
	}
});


//import 'v-calendar/lib/v-calendar.min.css';
Vue.use(VCalendar, {
	locale: 'fr',
	datePicker: {
		popover: {
			visibility: "focus"
		}
	}
});
require('@/style');
require('@/utils/dom');
Vue.config.productionTip = false;
Vue.component('currency', Currency);
Vue.component('decimal-input', DecimalInput);
Vue.filter('cur', cur);
Vue.filter('dec', dec);
Vue.filter('weekday', v => moment(v).format('dd, DD/MM/YYYY'));
Vue.filter('fullweekdatetime', v => moment(v).format('dd, DD/MM/YYYY HH:mm'));
Vue.config.errorHandler = function (err, vm, info) {
	if (err.graphQLErrors?.length) {
		err = err.graphQLErrors;
	}
	if (err.networkError) {
		err = err.networkError;
		if (err.result) {
			err = err.result;
		}
	}
	if (err.errors?.[0]) {
		err = err.errors[0];
	}
	if (Array.isArray(err)) {
		err = err[0];
	}
	store.commit('setError', { error: err && (err.message) });
	throw err;
};
Vue.options.components.vDatePicker.options.components.DatePicker.options.props.attributes = {
	type: Array,
	default: function () {
		return [{
			dot: {
				backgroundColor: '#4d4dff'
			},
			dates: new Date()
		}];
	}
};

new Vue({
	router,
	store,
	apolloProvider,
	render: h => h(App)
}).$mount('#app');

// do some cleanup of no longer used localstorage keys - can be removed after a while
localStorage.removeItem('username');
localStorage.removeItem('password');
localStorage.removeItem('wad:refreshToken');

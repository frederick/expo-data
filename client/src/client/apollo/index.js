import { ApolloClient, HttpLink, makeVar } from '@apollo/client/core';

import gql from "graphql-tag";
import { ApolloCache, InMemoryCache } from '@apollo/client/cache';

export const baseURL = `${window.origin}/expo-data/graphql`;
import * as gclient from "@/queries/client";
import { READ_STOCKS } from "@/queries/stocks";
import fetchAPI from '../fetchAPI';

const link = new HttpLink({
	fetch: fetchAPI.baseFetch,
	uri: baseURL,
});

const stockVar = makeVar(null);

const apolloClient = new ApolloClient({
	link,
	cache: new InMemoryCache({

		typePolicies: {
			Query: {
				fields: {
					selectedStock: {
						read() {
							return stockVar();
						}
					}
				}
			},
			Category: {
				keyFields: ["code"]
			},
			StockItemVariant: {
				keyFields: ['id','variant_id']
			},
			StockCountItem: {
				keyFields: ['id','stock_item_id','stock_item_variant_id']
			},
			StockReference: {
				keyFields: ['stock_item_id','stock_item_variant_id','stock_id']
			}
		}
	})
});
apolloClient.watchQuery({
	query: READ_STOCKS,
	fetchPolicy: 'cache-only'
}).subscribe({
	next({ data: { stocks } }) {
		if (stocks) {
			if (!stockVar()) {
				stockVar(stocks[0]);
			}
		}
	}
});
apolloClient.vars = {
	stockVar
};

window.apollo = apolloClient;
export default apolloClient;

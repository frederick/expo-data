import createAPI from "./createAPI";


var baseURL = `${window.origin}/expo-data/api`;

const fetchAPI = createAPI({ baseURL, application: 'expo-data' });

fetchAPI.baseURL = baseURL;

export default fetchAPI;
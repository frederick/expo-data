function createAPI({ baseURL, application }) {
	var refreshPromise = Promise.resolve();

	const event_listeners = {};

	const urlParams = new URLSearchParams(window.location.search);

	var loginURL;
	const refreshTokenKey = `wad:refreshToken-${application}`;
	const accessTokenKey = `wad:accessToken-${application}`;


	const config = {
		baseURL: baseURL,
		token: null,
		refreshToken: localStorage.getItem(refreshTokenKey),
		decoded: null,
		setToken(t) {
			this.token = t;
			this.decoded = null;
			if (t) {
				var tvar = t.split('.')[1];
				if (tvar) {
					this.decoded = JSON.parse(atob(tvar));
				}
			}
		}
	};

	function redirectToLogin() {
		config.refreshToken = null;
		localStorage.removeItem(refreshTokenKey);


		if (loginURL) {
			localStorage.removeItem(accessTokenKey);

			config.token = null;
			if (application === 'webauthd-login') {
				window.location.href = `${loginURL}/?keep=true`;
			} else {
				window.location.href = `${loginURL}/?return_to=${encodeURIComponent(
					window.location.href
				)}&application=${application}`;
			}
		} else {
			if (!config.token) {
				config.setToken(localStorage.getItem(accessTokenKey));
				fire('config', config);
			}
			if (config.token) {
				// token was set in another stack
				return true;
			}
			const token = window.prompt(`No login URL defined for application ${application}.\n\nYou can still log in by providing a Base64 encoded access token below:`);

			config.setToken(token);
			if (token) {
				localStorage.setItem(accessTokenKey, token);
			} else {
				localStorage.removeItem(accessTokenKey);
			}

			fire('config', config);

			if (!token) {
				window.alert(`Login cancelled.`);
			} else {
				return true;
			}
		}
	}

	const refresh = urlParams.get('refresh') || config.refreshToken;
	/**
	 * Fetch wrapper which deals with authentication
	 * @param {RequestInfo} input 
	 * @param {RequestInit} init 
	 * @returns {Promise<Response>}
	 */
	async function baseFetch(input, init, {
		token,
		refresh,
		direct,
	} = {}) {
		input;
		if (!direct) {
			await refreshPromise;
		}

		let headers = init.headers ?? input.headers ?? {};

		if (!token) {
			token = config.token;
		}
		if (!refresh) {
			refresh = config.refreshToken = localStorage.getItem(refreshTokenKey);
		}
		if (token) {
			headers['Authorization'] = `Bearer ${token}`;
		}
		try {

			fire('loading', true);

			var response = await fetch(
				input,
				{
					...(init ?? {}),
					headers: {
						'content-type': 'application/json',
						'accept': 'application/json',
						...headers
					}
				});
		} finally {
			fire('loading', false);
		}

		if (!direct && response.status === 403 && response.headers.get('content-type').indexOf('application/json') === 0) {
			let json = await response.clone().json();
			if (json.errors) {
				// graphql-style response
				json = json.errors[0];
			}
			if (json && json.error === 'TokenExpiredError') {
				// we got an token expired error. Try to refresh the token.
				// if that does not work redirect to login.
				if (refresh) {
					const oldToken = config.token;
					await refreshPromise;
					// perhaps a request is already running to get a new token,
					// only renew if the token is still the same
					if (oldToken === config.token) {
						await refreshToken(refresh);
					}
					return baseFetch(input, init, {
						direct: true,
					});
				} else {
					loginURL = json && json.login;
					if (redirectToLogin() === true) {
						return baseFetch(input, init, {
							direct: true,
						});
					}
				}
			}
		}

		return response;
	}
	async function fetchAPI({
		endpoint,
		body,
		method,
		params,
		headers,
		token,
		refresh,
		direct,
		fireError = true,
		host = config.baseURL
	} = {}) {

		if (!method) {
			method = 'GET';
		}

		const url = new URL(`${host}/${endpoint}`);
		if (params) {
			Object.keys(params).forEach(key => {
				if (params[key] !== undefined) {
					url.searchParams.append(key, params[key]);
				}
			});
		}
		headers = headers || {};

		try {
			var response = await baseFetch(
				url,
				{
					method,
					body,
					headers: {
						'content-type': 'application/json',
						...headers
					}
				},
				{ direct, token, refresh }
			);
			if (response.headers.get('content-type').indexOf('application/json') === 0) {
				var json = await response.json();
			} else {
				json = { data: await response.blob() };
			}

		} catch (e) {
			if (fireError) {
				fire('error', e);
			}
			throw e;
		} finally {
		}

		if (response.status < 400) {
			return json;
		} else {

			var s = (json && json.status) || response.status;
			var e = new Error(json && (json.message || json.reason));
			e.status = s;
			e.json = json;
			if (fireError) {
				fire('error', e);
			}
			throw e;
		}
	}
	function refreshToken(refresh) {
		return (refreshPromise = new Promise(async (resolve, reject) => {
			try {
				var response = await fetchAPI({
					direct: true,
					endpoint: 'refresh-token'
				});
				let url = response && response.refresh;
				if (url) {
					const tokenR = await fetchAPI({
						host: url,
						direct: true,
						endpoint: 'token',
						token: refresh,
						method: 'POST'
					});
					config.setToken(tokenR.token);
					config.refreshToken = tokenR.refreshToken;

					localStorage.setItem(refreshTokenKey, config.refreshToken);

					fire('config', config);
				} else {
					redirectToLogin();
				}
				resolve();
			} catch (e) {
				if (response) {
					loginURL = response.login;
					redirectToLogin();
				} else {
					reject(e);
				}
			}
		}));
	}
	if (refresh) {
		(async () => {
			await refreshToken(refresh);
			var p = new URLSearchParams(window.location.search);
			p.delete('refresh');
			var q = p.toString();
			window.history.replaceState({}, null, q.length ? `?${q}` : window.location.href.split('?')[0]);
		})();
	}

	function fire(event, pl) {
		var ls = event_listeners[event];
		if (ls) {
			for (let l of ls) {
				try {
					l(pl);
				} catch (e) {
					//eslint-disable-next-line
					console.trace(e);
				}
			}
		}
	}
	function on(event, cb) {
		var ls = event_listeners[event];
		if (!ls) {
			ls = event_listeners[event] = [];
		}

		ls.push(cb);
	}

	fetchAPI.on = on;
	fetchAPI.config = config;

	fetchAPI.refreshTokenKey = refreshTokenKey;

	fetchAPI.logout = () => {
		refreshTokenKey, config.refreshToken = config.token = null;
		localStorage.removeItem(refreshTokenKey);
		localStorage.removeItem(accessTokenKey);
		fire('logout');
		window.location.reload();
	};

	fetchAPI.baseFetch = baseFetch;
	return fetchAPI;
}

export default createAPI;

import fetchAPI from './fetchAPI';


const client = {
  baseURL: fetchAPI.baseURL,
  fetchAPI,
  
  stocks: {
    list() {
      return fetchAPI({ endpoint: 'stocks', method: 'GET' });
    },
    get(stock) {
      return fetchAPI({ endpoint: `stocks/${stock}`, method: 'GET' });
    },
    create(stock) {
      return fetchAPI({
        endpoint: `stocks`,
        method: 'POST',
        body: JSON.stringify(stock)
      });
    },
    update(id, stock) {
      return fetchAPI({
        endpoint: `stocks/${id}`,
        method: 'PUT',
        body: JSON.stringify(stock)
      });
    },
    delete(id) {
      return fetchAPI({
        endpoint: `stocks/${id}`,
        method: 'DELETE',
        body: JSON.stringify({})
      });
    },
    getStockProducts(stock) {
      return fetchAPI({ endpoint: `stocks/${stock}/products`, method: 'GET' });
    },
    updateStockProducts(stock, products) {
      return fetchAPI({ endpoint: `stocks/${stock}/products`, method: 'PUT', body: JSON.stringify(products) });
    },
    getStockProduct(stock, stockProduct) {
      return fetchAPI({ endpoint: `stocks/${stock}/products/${stockProduct}`, method: 'GET' });
    },
    addProducts(id, productIds) {
      return fetchAPI({
        endpoint: `stocks/${id}/products`,
        method: 'POST',
        body: JSON.stringify(productIds)
      });
    },
    removeProducts(id, productIds) {
      return fetchAPI({
        endpoint: `stocks/${id}/products`,
        method: 'DELETE',
        body: JSON.stringify(productIds)
      });
    },
    countingLists: {
      list(stock) {
        return fetchAPI({ endpoint: `stocks/${stock}/counting-lists`, method: 'GET' });
      },
      get(stock, id) {
        return fetchAPI({ endpoint: `stocks/${stock}/counting-lists/${id}`, method: 'GET' });
      },
      remove(stock, id) {
        return fetchAPI({ endpoint: `stocks/${stock}/counting-lists/${id}`, method: 'DELETE' });
      },
      update(stock, id, list) {
        return fetchAPI({
          endpoint: `stocks/${stock}/counting-lists/${id}`,
          method: 'PUT',
          body: JSON.stringify(list)
        });
      },
      create(stock, list) {
        return fetchAPI({
          endpoint: `stocks/${stock}/counting-lists`,
          method: 'POST',
          body: JSON.stringify(list)
        });
      }
    }
  },
  reports: {
    run(report, today, inventory) {
      return fetchAPI({ endpoint: `reports/${report}/run`, method: 'GET', params: { date: today, inventory } });
    }
  },
  accounting: {
    pastequeSalesJournal(inventoryId) {
      return fetchAPI({ endpoint: `accounting/sales-journal/pasteque?inventory=${inventoryId}`, method: 'GET' });
    },
    confirmPastequeSales(sales) {
      return fetchAPI({ endpoint: `accounting/sales-journal/pasteque`, method: 'POST', body: JSON.stringify(sales) });
    },
    analyzeTransactions(transactions) {
      return fetchAPI({ endpoint: `accounting/analyze-transactions`, method: 'POST', body: JSON.stringify({ transactions }) });
    }
  },
  webshop: {
    orders: {
      list({ status = ['processing'] } = {}) {
        const params = new URLSearchParams({
          status
        });
        return fetchAPI({ endpoint: `webshop/orders?${params.toString()}`, method: 'GET' });
      },
      listGroups() {
        return fetchAPI({ endpoint: `webshop/orders-groups`, method: 'GET' });
      },
      async assignGroup(groupname, orders) {
        await fetchAPI({ endpoint: `webshop/orders/assign_group/${groupname}`, method: 'POST', body: JSON.stringify({ orders }) });

      },
      async printPDF(orders) {
        const { data } = await fetchAPI({ endpoint: `webshop/orders/pdf`, method: 'POST', body: JSON.stringify({ orders }) });
        return data;
      },
      items(order_id) {
        return fetchAPI({ endpoint: `webshop/orders/${order_id}/items`, method: 'GET' });
      },
      print(order_id) {
        return fetchAPI({ endpoint: `webshop/orders/${order_id}/print`, method: 'POST' });
      },
      saveComment(order_id, comment) {
        return fetchAPI({ endpoint: `webshop/orders/${order_id}/comment`, method: 'PUT', body: JSON.stringify({ comment }) });
      }
    },
    boxes: {
      list() {
        return fetchAPI({ endpoint: `webshop/boxes`, method: 'GET' });
      },
    }
  },
  history: {
    load(entity, id) {
      return fetchAPI({ endpoint: `history/${entity}/${id}`, method: 'GET' });
    }
  }
};

window.client = client;

export default client;
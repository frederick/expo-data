const WEBPRINT_URL = `http://localhost:3283`;
var formatter = new Intl.NumberFormat("fr-FR", {
  style: "currency",
  currency: "EUR",
  minimumFractionDigits: 2,
});
let prober = null;
export const status = {
};
resetStatus();
export function startProber() {
  endProber();
  window.expoProber = prober = setInterval(async () => {
    try {
      await probe();
    } catch (e) {

    }
  }, 5000);
  probe();
}
export function endProber() {
  if (prober) {
    clearInterval(prober);
    clearInterval(window.expoProber);
    prober = null;
  }
}
function resetStatus() {
  Object.assign(status, {
    accessible: false,
    exists: false,
  });
}
export async function probe() {
  try {
    const response = await fetch(`${WEBPRINT_URL}/status`);
    const probe = await response.json();
    Object.assign(status, probe);
    return probe;
  } catch (e) {
    resetStatus();
  }
}

export async function print(data) {
  const response = await fetch(`${WEBPRINT_URL}/webprint`, {
    body: data,
    headers: {
      'content-type': 'text/plain'
    },
    method: 'POST'
  });
  const r = await response.json();
  if (!response.ok || response.status === 500) {
    throw new Error(r?.reason);
  }
  return r;
}

export function renderLabel(barcode, name, price, count = 1) {
  name = (name ?? '').replace(/_/g, '_5f').replace(/~/g, '_7e').replace(/\^/g, '_5e');
  price = formatter.format(parseFloat(price) || 0).replace('€', '_15').replace(/\s/g,' ');
  return `
^XA
^MMP
^JUS
^FWN
^LH300,25
^CFD,11
^FO10,10
^FH^FD${name}^FS

^FO30,35^BY2

^BEN,85,Y,N

^FD${barcode}^FS


^FO220,150,1
^FH^FDPrix: ${price}^FS
^PQ${count}
^XZ
`;
}
import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import ProductList from './views/product/ProductList.vue';
import Product from './views/product/Product.vue';
import ProductVariants from './views/product/ProductVariants.vue';
import ProductCustomAttributes from './views/product/ProductCustomAttributes.vue';
import ProductNav from './views/product/ProductNav.vue';
import ProductAttributes from './views/product/Attributes.vue';
import ProductAttribute from './views/product/Attribute.vue';
import Tickets from './views/Tickets.vue';
import Ticket from './views/Ticket.vue';
import Receipts from './views/Receipts.vue';
import Receipt from './views/Receipt.vue';
import Reports from './views/Reports.vue';
import ReportRun from './views/ReportRun.vue';
import Inventory from './views/inventory/Inventory.vue';
import Inventories from './views/inventory/Inventories.vue';
import InventoryNav from './views/inventory/InventoryNav.vue';
import Stocks from './views/inventory/Stocks.vue';
import Stock from './views/inventory/Stock.vue';
import StockItems from './views/inventory/StockItems.vue';
import StockItem from './views/inventory/StockItem.vue';
import StockCounts from './views/inventory/StockCounts.vue';
import StockCount from './views/inventory/StockCount.vue';
import StockMovements from './views/inventory/StockMovements.vue';
import StockMovement from './views/inventory/StockMovement.vue';
import Accounting from './views/accounting/Accounting.vue';
import SalesJournal from './views/accounting/SalesJournal.vue';
import TransactionChecker from './views/accounting/TransactionChecker.vue';

import Webshop from './views/webshop/Index.vue';
import WebshopOrders from './views/webshop/Orders.vue';
import WebshopBoxes from './views/webshop/Boxes.vue';
import client from './client/apollo';
import { gql } from '@apollo/client/core';
import store from './store';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/products',
      name: 'product-list',
      components: {

        default: ProductList,
        subnav: ProductNav
      }
    },

    {
      path: '/product/:product',
      name: 'product',
      components: {
        default: ProductList,
        details: Product,
        subnav: ProductNav
      }
    },
    {
      path: '/product/:product/variants',
      name: 'product-variants',
      components: {
        default: ProductList,
        details: ProductVariants,
        subnav: ProductNav
      }
    },
    {
      path: '/product/:product/attributes',
      name: 'product-custom-attributes',
      components: {
        default: ProductList,
        details: ProductCustomAttributes,
        subnav: ProductNav
      }
    },
    {
      path: '/product-by-number/:number',
      name: 'product-by-number',
      components: {
        default: ProductList,
        details: Product,
        subnav: ProductNav
      },
      async beforeEnter(to, from, next) {

        let { data: { productByNumber } } = await client.query({
          fetchPolicy: 'network-only',
          query: gql`
            query ($number: Int!) {
              productByNumber(number: $number) { id }
            }
          `,
          variables: {
            number: parseInt(to.params.number)
          }
        });
        if (productByNumber) {
          next({ name: 'product', params: { product: productByNumber.id } });
        } else {
          store.commit('setError', {error: new Error('Product not found')});
        }

      }

    },
    {
      path: '/products/attributes',
      name: 'product-attributes',
      components: {
        default: ProductAttributes,
        subnav: ProductNav
      }
    },

    {
      path: '/products/attribute/:attribute',
      name: 'product-attribute',
      components: {
        default: ProductAttributes,
        details: ProductAttribute,
        subnav: ProductNav
      }
    },
    {
      path: '/tickets',
      name: 'tickets',
      component: Tickets
    },

    {
      path: '/ticket/:ticket',
      name: 'ticket',
      components: {
        default: Tickets,
        details: Ticket
      }

    },
    {
      path: '/receipts',
      name: 'receipts',
      component: Receipts
    },

    {
      path: '/receipt/:receipt',
      name: 'receipt',
      components: {
        default: Receipts,
        details: Receipt
      }

    },

    {
      path: '/inventory/stocks',
      name: 'stocks',
      components: {
        subnav: InventoryNav,
        default: Stocks
      }
    },
    {
      path: '/inventory/stock/:stock',
      name: 'stock',
      components: {
        subnav: InventoryNav,
        default: Stocks,
        details: Stock
      }
    },
    {
      path: '/inventory/stock/:stock/items',
      name: 'stock-items',
      components: {
        subnav: InventoryNav,
        default: StockItems
      }
    },
    {
      path: '/inventory/stock/:stock/items/:stockItem',
      name: 'stock-item',
      components: {
        subnav: InventoryNav,
        default: StockItems,
        details: StockItem
      }
    },
    {
      path: '/inventory/stock/:stock/stock-counts',
      name: 'stock-counts',
      components: {
        subnav: InventoryNav,
        default: StockCounts
      }
    },
    {
      path: '/inventory/stock/:stock/stock-counts/:count',
      name: 'stock-count',
      components: {
        subnav: InventoryNav,
        default: StockCounts,
        details: StockCount
      }
    },
    {
      path: '/inventory/stock/:stock/stock-movements',
      name: 'stock-movements',
      components: {
        subnav: InventoryNav,
        default: StockMovements
      }
    },
    {
      path: '/inventory/stock/:stock/stock-movements/:movement',
      name: 'stock-movement',
      components: {
        subnav: InventoryNav,
        default: StockMovements,
        details: StockMovement
      }
    },
    {
      path: '/inventory/',
      name: 'inventories',
      components: {
        subnav: InventoryNav,
        default: Inventories
      }

    },
    {
      path: '/inventory/:inventory',
      name: 'inventory',
      components: {
        subnav: InventoryNav,
        default: Inventories,
        details: Inventory
      }

    },
    {
      path: '/reports',
      name: 'reports',
      component: Reports
    },

    {
      path: '/reports/:report/run',
      name: 'report-run',
      components: {
        default: Reports,
        details: ReportRun
      }

    },

    {
      path: '/reports/:report',
      name: 'report',
      components: {
        default: Reports,
        details: () => import('./views/Report.vue')
      }

    },
    {
      path: '/accounting',
      name: 'accounting',
      components: {

        details: Accounting
      },
      children: [
        {
          path: 'payment-journal/:inventory',
          name: 'payment-journal',
          components: {
            default: SalesJournal
          }
        },
        {
          path: 'transaction-checker',
          name: 'transaction-checker',
          components: {
            default: TransactionChecker
          }
        }
      ]
    },
    {
      path: '/webshop',
      name: 'webshop',
      components: {

        default: Webshop
      },
      children: [
        {
          path: '/webshop/orders',
          name: 'webshop-orders',
          components: {
            default: WebshopOrders
          }
        },
        {
          path: '/webshop/boxes',
          name: 'webshop-boxes',
          components: {
            default: WebshopBoxes
          }
        }
      ]
    }

  ]
});

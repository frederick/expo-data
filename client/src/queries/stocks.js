import gql from "graphql-tag";



export const READ_STOCKS = gql`
  query {
    stocks {
      id, version,	name, description, product_count
    }
  }
`;

export const GET_STOCK = gql`
  query GetStock($id: Int!) {
    stock(id: $id) {
      id, version,	name, description, product_count
    }
  }
`;


export const UPDATE_STOCK = gql`
  mutation UpdateStock($id: Int!, $version: Int!, $stock: StockInput!) {
    updateStock(id: $id, version: $version, stock: $stock)
  }
`;

export const DELETE_STOCK = gql`
  mutation DeleteStock($id: Int!, $version: Int!) {
    deleteStock(id: $id, version: $version)
  }
`;


export const CREATE_STOCK = gql`
  mutation CreateStock($stock: StockInput!) {
    createStock(stock: $stock)
  }
`;


const STOCK_OPERATION_FIELDS = gql`
  fragment StockOperationFields on StockOperation {
    stock { id, name }
    stock_id
    date
    type
    comment
    reference
    created_by
    adjustments {
      id, stock_item_id, product_id, variant_id, stock_item_variant_id, adjustment, old_quantity, new_quantity
      
      variant {
        sku
        name
        code
        id
        values {
          id
          attribute{
            id
            code
            name
            type
          }
          value{
            id
            value
            code
            color
          }
        }
      }
      product {
        id
        name
        number
      }
    }
  }
`;
const STOCK_ITEM_FRAGMENT = gql`
  fragment StockItemFields on StockItem {
    id
    quantity
    notes
    variants {
      variant {
        sku
        name
        code
        id
        values {
          id
          attribute{
            id
            code
            name
            description
            type
          }
          value{
            id
            value
            code
            color
          }
        }
      }
      quantity
      notes
      id
      variant_id
    }
    product {
      name
      number
      criteria
      category_code
      barcode
      price
      variant_count
      active
      has_pasteque_product
      id
      version
      label
      sku
      category {
        rate
        code
        name
        pasteque_tax
      }
      variants{
        sku
        name
        code
        id
        values {
          id
          attribute{
            id
          }
          value{
            id
          }
        }
      }
    }
  }
`;

export const GET_STOCK_ITEM = gql`
  ${STOCK_ITEM_FRAGMENT}
  query GetStockItem($id: Int!) {
    stockItem(id: $id) {
      ...StockItemFields
      adjustments {
        id, stock_item_id, product_id, variant_id, stock_item_variant_id, adjustment, old_quantity, new_quantity, comment
        operation {
          stock_id
          date
          type
          comment
          reference
          created_by
        }
        variant {
          sku
          name
          code
          id
          values {
            id
            attribute{
              id
              code
              name
              type
            }
            value{
              id
              value
              code
              color
            }
          }
        }
      }
    }
  }
`;

export const GET_STOCK_ITEM_QUANTITIES = gql`
  query GetStockItem($id: Int!) {
    stockItem(id: $id) {
      id
      quantity
      variants {
        variant {
          sku
          name
          code
          id
          values {
            id
            attribute{
              id
              code
              name
              type
            }
            value{
              id
              value
              code
              color
            }
          }
        }
        quantity
        id
        variant_id
      }
    }
  }
`;

export const GET_STOCK_WITH_ITEMS = gql`
  ${STOCK_ITEM_FRAGMENT}
  query GetStockWithItems($id: Int!) {
    stock(id: $id) {
      id, version,	name, description, product_count,
      items {
        ...StockItemFields
      }
    }
  }
`;

export const UPDATE_STOCK_ITEM = gql`
  mutation UpdateStockItem($id: Int!, $stockItem: StockItemInput!) {
    updateStockItem(id: $id, item: $stockItem)
  }
`;
//updateStockItemQuantity(stock_item_id: Int!, variant_id: Int, quantity: Float!, adjustmentComment: String)
export const UPDATE_STOCK_ITEM_QUANTITIES = gql`
  mutation updateStockItemQuantities($stock_item_id: Int!, $stock_item_quantities: [StockItemQuantityInput!]!, $adjustmentComment: String) {
    updateStockItemQuantities(stock_item_id: $stock_item_id, stock_item_quantities: $stock_item_quantities, adjustmentComment: $adjustmentComment)
  }
`;

export const CREATE_STOCK_ITEMS = gql`
  mutation CreateStockItems($stock_id: Int!, $product_ids: [Int!]!) {
    createStockItems(stock_id: $stock_id, product_ids: $product_ids)
  }
`;
export const REMOVE_STOCK_ITEMS = gql`
  mutation RemoveStockItems($ids: [Int!]!) {
    removeStockItems(ids: $ids)
  }
`;


export const READ_STOCK_COUNTS = gql`
  query GetStockCounts($stock_id: Int!){
    stockCounts(stock_id: $stock_id) {
      id,stock_id,name, date, inventory_id, comment, validated
    }
  }
`;

export const GET_STOCK_COUNT = gql`
  query GetStockCounts($id: Int!){
    stockCount(id: $id) {
      id,stock_id,name, date, inventory_id, comment, validated,transferred,
      stock {name},
      items {
        id, comment, updated_at, stock_item_id, stock_item_variant_id,
				stock_item {
					id
					quantity
          variant_count
					product{ 
						name
						number
						criteria
						category_code
						barcode
						price
						variant_count
						active
						has_pasteque_product
						id
						version
						label
						sku
						category {
							rate
							code
							name
							pasteque_tax
						}
					}
				},
				stock_item_variant{
					variant {
								sku
								name
								code
								id
								values {
									id
									attribute{
										id
										code
										name
										type
									}
									value{
										id
										value
										code
										color
									}
								}
							}
							quantity
							id
							variant_id
				},
				quantity
				
      }
    }
  }
`;
export const UPDATE_STOCK_COUNT = gql`
  mutation UpdateStockCount($id: Int!, $count: StockCountInput!) {
    updateStockCount(id: $id, count: $count)
  }
`;

export const UPDATE_STOCK_COUNT_ITEMS = gql`
	mutation UpdateStockCountItems($stock_count_id: Int!, $counts: [StockCountItemInput!]!) {
		updateStockCountItems(stock_count_id: $stock_count_id, counts: $counts)

	}
`;

export const DELETE_STOCK_COUNT = gql`
  mutation DeleteStockCount($id: Int!) {
    deleteStockCount(id: $id)
  }
`;


export const CREATE_STOCK_COUNT = gql`
  mutation CreateStockCount($stock_id: Int!, $count: StockCountInput!) {
    createStockCount(stock_id: $stock_id, count: $count)
  }
`;

export const TRANSFER_STOCK_COUNT = gql`
	mutation transferStockCount($stock_count_id: Int!) {
		transferStockCount(stock_count_id: $stock_count_id)

	}
`;

// movements
export const READ_STOCK_MOVEMENTS = gql`
  query GetStockMovements($stock_id: Int!){
    stockMovements(stock_id: $stock_id) {
      id,stock_id,name, date, comment, executed,
      destination_stock {
        name, id
      }
    }
  }
`;

export const GET_STOCK_MOVEMENT = gql`
  ${STOCK_OPERATION_FIELDS}
  query GetStockMovement($id: Int!){
    stockMovement(id: $id) {
      id,stock_id,name, date, comment, executed,
      stock {id,name},
      destination_stock {id,name},
      destination_stock_id,
      items {
        id, comment, updated_at, stock_item_id, stock_item_variant_id,
        destination_reference{
          stock_id
          stock_item_id
          stock_item_variant_id
          quantity
        }
				stock_item {
					id
					quantity
          variant_count
					product{ 
						name
						number
						criteria
						category_code
						barcode
						price
						variant_count
						active
						has_pasteque_product
						id
						version
						label
						sku
						category {
							rate
							code
							name
							pasteque_tax
						}
					}
				},
				stock_item_variant{
					variant {
								sku
								name
								code
								id
								values {
									id
									attribute{
										id
										code
										name
										type
									}
									value{
										id
										value
										code
										color
									}
								}
							}
							quantity
							id
							variant_id
				},
				quantity
				
      }
      operation {
        ...StockOperationFields
      }
      destination_operation {
        ...StockOperationFields
      }
    }
  }
`;
export const UPDATE_STOCK_MOVEMENT = gql`
  mutation UpdateStockMovement($id: Int!, $movement: StockMovementInput!) {
    updateStockMovement(id: $id, movement: $movement)
  }
`;

export const UPDATE_STOCK_MOVEMENT_ITEMS = gql`
	mutation UpdateStockMovementItems($stock_movement_id: Int!, $items: [StockMovementItemInput!]!) {
		updateStockMovementItems(stock_movement_id: $stock_movement_id, items: $items)

	}
`;

export const DELETE_STOCK_MOVEMENT = gql`
  mutation DeleteStockMovement($id: Int!) {
    deleteStockMovement(id: $id)
  }
`;
export const DELETE_STOCK_MOVEMENT_ITEM = gql`
  mutation DeleteStockMovementItem($stock_movement_id: Int!,$stock_movement_item_id: Int!) {
    deleteStockMovementItem(stock_movement_id: $stock_movement_id,stock_movement_item_id: $stock_movement_item_id)
  }
`;


export const CREATE_STOCK_MOVEMENT = gql`
  mutation CreateStockMovement($stock_id: Int!, $movement: StockMovementInput!) {
    createStockMovement(stock_id: $stock_id, movement: $movement)
  }
`;

export const EXECUTE_STOCK_MOVEMENT = gql`
	mutation executeStockMovement($stock_movement_id: Int!) {
		executeStockMovement(stock_movement_id: $stock_movement_id)
	}
`;
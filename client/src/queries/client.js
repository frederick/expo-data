import gql from "graphql-tag";

export const GET_TODAY = gql`
  query {
    today @client
  }
`;

export const SET_TODAY = gql`
   mutation($today: Date!) {
    setToday(today: $today) @client
  }
`;
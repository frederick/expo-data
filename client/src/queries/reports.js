import gql from "graphql-tag";



export const READ_REPORTS = gql`
  query ReadReports {
    reports {
      id
      version
      name
      description
    }
  }
`;

export const GET_REPORT = gql`
  query GetReport($id: Int!) {
    report(id: $id) {
      id
      version
      name
      description
      type
      datepicker
      sql
      template
    }
  }
`;

export const UPDATE_REPORT = gql`
  mutation UpdateReport($id: Int!, $version: Int!, $report: ReportInput!) {
    updateReport(id: $id, version: $version, report: $report)
  }
`;


export const CREATE_REPORT = gql`
  mutation CreateReport($report: ReportInput!) {
    createReport(report: $report)
  }
`;

export const DELETE_REPORT = gql`
mutation DeleteReport($id: Int!, $version: Int!) {
  deleteReport(id: $id, version: $version)
}
`;
import gql from "graphql-tag";



export const READ_TICKETS = gql`
  query readTickets($date: Date!) {
      tickets(date: $date) {
        id,	number,	date,	total, finalprice, cash_register,	items, total_ht
      }
  }
`;

export const GET_TICKET = gql`
  query getTicket($id: Int!) {
      ticket(id: $id) {
        id,	number,	date,	total, finalprice, cash_register,	items, total_ht,
        rawTicket
      }
  }
`;
export const GET_TICKET_STATS = gql`
  query ticketStats($year: Int!, $month: Int!) {
    ticketStats(year: $year,month: $month){
      date
      count
      cash_registers
    },
  }
`;

import gql from "graphql-tag";



export const READ_RECEIPTS = gql`
  query readReceipts {
    receipts {
      id,	number,	date, ticket, total,
      customer {
        id, firstname, lastname
      }
    }
  }
`;

export const GET_RECEIPT = gql`
  query GetReceipt($id: Int!) {
    receipt(id: $id) {
      id, version,	number,	date, ticket, total, ticket_id,payment_details,note,
      customer {
        id, firstname, lastname, version,
        email, phone1, phone2, fax, address, zipcode, city, region, country, note
      }
    }
  }
`;
import gql from "graphql-tag";



export const READ_PAYMENT_METHODS = gql`
  query {
    paymentMethods {
      id
      code
      name
      display_name
      barcode
      polimod
      pasteque_reference
      pasteque_id
    }
  }
`;


export const GET_CONFIGURATION_SETTINGS = gql`
  query GetConfigurationSetting($keys: [String!]) {
    configurationSettings(keys: $keys) {
      key
      value
    }
  }
`;
export const GET_WEBSHOP_ORDERS_URL = gql`
  query {
    webshopOrdersUrl
  }
`;

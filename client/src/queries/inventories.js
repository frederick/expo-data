import gql from "graphql-tag";



export const READ_INVENTORIES = gql`
  query ReadInventories {
    inventories{
      id
      version
      date
      period_start
      inventory_type
      comments
      start_date
    }
  }
`;

export const GET_INVENTORY = gql`
  query GetInventory($id: Int!) {
    inventory(id: $id) {
      id
      version
      date
      period_start
      inventory_type
      comments
      start_date
    }
  }
`;

export const UPDATE_INVENTORY = gql`
  mutation UpdateInventory($id: Int!, $version: Int!, $inventory: InventoryInput!) {
    updateInventory(id: $id, version: $version, inventory: $inventory)
  }
`;


export const CREATE_INVENTORY = gql`
  mutation CreateInventory($inventory: InventoryInput!) {
    createInventory(inventory: $inventory)
  }
`;


export const DELETE_INVENTORY = gql`
mutation DeleteInventory($id: Int!, $version: Int!) {
  deleteInventory(id: $id, version: $version)
}
`;
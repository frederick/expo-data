import gql from "graphql-tag";

export const PRODUCT_INPUT_FIELDS = ['name', 'sku', 'label', 'number', 'barcode', 'criteria', 'price', 'category_code', 'supplier', 'active', 'pasteque_product_id'];


export const READ_PRODUCTS = gql`
  query {
      products {
        name
        number
        criteria
        category_code
        barcode
        price
        variant_count
        active
        has_pasteque_product
        id
        sku
      }
  }
`;
export const READ_MISSING_PRODUCTS = gql`
  query {
      missingPastequeProducts {
          id
          reference
          label
          barcode
          pricesell
          visible
          category
          tax_rate
          tax
          price
      }
  }
`;
export const READ_NOT_SYNCHRONIZED = gql`
  query {
      unsynchronizedProducts {
        name
        number
        criteria
        category_code
        barcode
        price
        variant_count
        active
        has_pasteque_product
        id
				syncStats
        sku
      }
  }
`;
export const GET_PRODUCT = gql`
  query GetProduct($id: Int!){
      product(id : $id) {
        name
        number
        criteria
        category_code
        barcode
        price
        variant_count
        active
        has_pasteque_product
        id
        version
        label
        sku
        category {
          rate
          code
          name
          pasteque_tax
        }
        pasteque {
          id
          reference
          label
          barcode
          pricesell
          visible
          category
          tax_rate
          tax
          price
        }
      }
  }
`;


export const GET_PRODUCT_VARIANTS = gql`
  query GetProduct($id: Int!){
      product(id : $id) {
        id
        name
        number
        version
        variant_count
        sku
        variants{
          sku
          name
          code
          id
          values {
            id
            attribute{
              id
              code
              type
              name
            }
            value{
              id
              value
              code
              color
            }
          }
        }
      }
  }
`;

export const GET_PRODUCT_CUSTOM_ATTRIBUTES = gql`
  query GetProduct($id: Int!){
      product(id : $id) {
        id
        name
        number
        sku
        attributes
        version
      }
  }
`;

export const PRODUCT_ATTRIBUTES = gql`
  query {
    productAttributes {
      id
      code
      name
      description
      type
      values{
        id
        value
        code
        color
      }
    }
  }`;
export const PRODUCT_ATTRIBUTE = gql`
query GetProductAttribute($id: Int!){
	productAttribute(id: $id) {
		id
		code
		name
		description
		type
		values{
			id
			value
			code
			color
		}
	}
}`;
export const UPDATE_PRODUCT = gql`
mutation updateProduct($id: Int!, $version: Int!, $product: ProductInput!) {
  updateProduct(id: $id, version: $version, product: $product)
}
`;
export const CREATE_PRODUCT = gql`
mutation createProduct($product: ProductInput!) {
  createProduct(product: $product)
}
`;

export const CREATE_ATTRIBUTE = gql`
mutation createProductAttribute($productAttribute: ProductAttributeInput!) {
  createProductAttribute(productAttribute: $productAttribute)
}
`;


export const UPDATE_ATTRIBUTE = gql`
mutation updateProductAttribute($id: Int!, $productAttribute: ProductAttributeInput!) {
  updateProductAttribute(id: $id, productAttribute: $productAttribute)
}
`;
export const CHANGE_PRODUCT_ATTRIBUTE_VALUE_POSITION = gql`
mutation changeProductAttributeValuePosition($product_attribute_id: Int!, $product_attribute_value_id: Int!, $position: Int!) {
	changeProductAttributeValuePosition(product_attribute_id: $product_attribute_id, product_attribute_value_id: $product_attribute_value_id, position:$position )
}
`;
export const DELETE_ATTRIBUTE = gql`
mutation deleteProductAttribute($id: Int!) {
  deleteProductAttribute(id: $id)
}
`;


export const DELETE_PRODUCT = gql`
mutation deleteProduct($id: Int!, $version: Int!) {
	deleteProduct(id: $id, version: $version)
}
`;
export const CREATE_PASTEQUE_PRODUCT = gql`
mutation createPastequeProduct($product_id: Int!, $product_version: Int!, $pasteque_product: PastequeProductInput!) {
  createPastequeProduct(product_id: $product_id, product_version: $product_version, pasteque_product: $pasteque_product)
}
`;
export const UPDATE_PASTEQUE_PRODUCT = gql`
mutation updatePastequeProduct($product_id: Int!, $product_version: Int!, $pasteque_product_id: Int!, $pasteque_product: PastequeProductInput!) {
  updatePastequeProduct(product_id: $product_id, product_version: $product_version, pasteque_product_id: $pasteque_product_id, pasteque_product: $pasteque_product)
}
`;

export const UPDATE_VARIANT = gql`
mutation updateVariant($product_id: Int!, $version: Int!, $variant_id: Int!, $variant: ProductVariantInput!) {
  updateProductVariant(product_id: $product_id, product_version: $version, variant_id: $variant_id, variant: $variant)
}
`;
export const CREATE_VARIANT = gql`
mutation createVariant($product_id: Int!, $version: Int!, $variant: ProductVariantInput!) {
  createProductVariant(product_id: $product_id, product_version: $version, variant: $variant)
}
`;

export const DELETE_VARIANT = gql`
mutation deleteVariant($product_id: Int!, $version: Int!, $variant_id: Int!) {
  deleteProductVariant(product_id: $product_id, product_version: $version, variant_id: $variant_id)
}
`;

export const UPDATE_CUSTOM_ATTRIBUTES = gql`
mutation updateProductCustomAttributes($product_id: Int!, $product_version: Int!, $attributes: JSON!) {
  updateProductCustomAttributes(product_id: $product_id, product_version: $product_version, attributes: $attributes)
}
`;

import gql from "graphql-tag";


export const GET_SALES_JOURNAL = gql`
  query getSalesJournalWithInventory($code: String!, $inventory_id: Int!) {
      salesJournal(code: $code) {
        id, name,	code, description,
        entries(inventory_id: $inventory_id) {
          date, method,	closed,	amount,	pasteque_amount, pasteque_session_amount,	diff
        }
      }
  }
`;

export const UPDATE_SALES_JOURNAL_ENTRIES = gql`
  mutation updateSalesJournalEntries($sales_journal_code: String!, $entries: [SalesJournalEntryInput!]!) {
    updateSalesJournalEntries(sales_journal_code: $sales_journal_code, entries: $entries) 
  }
`;
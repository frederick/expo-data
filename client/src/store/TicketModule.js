import client from '@/client';

export default {
  namespaced: true,
  state: {
    ticket: {},
    tickets: [],
    count: 0
  },
  getters: {
    ticket: state => state.ticket,
    tickets: state => state.tickets,
    count: state => state.count
  },
  actions: {
    async loadTicket ({ state }, id) {
      state.ticket = await client.tickets.get(id);
      return state.ticket;
    },
    async clearTicket ({ state }) {
      state.ticket = {};
    },
    async reloadTickets ({ state }, { date }) {
      const { count, tickets } = await client.tickets.list({ date });
      state.count = count;
      state.tickets = tickets;
    }
  }
};

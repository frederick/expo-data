import _ from 'lodash';
import moment from 'moment';
import apollo from '@/client/apollo';
import { READ_INVENTORIES } from '@/queries/inventories';

export default {
  namespaced: true,
  state: {
    inventories: [],
    inventory: null,
    today: new Date()
  },
  getters: {
    inventories: state => state.inventories,
    inventory: state => state.inventory,
    today: state => state.today,
    todaysInventory: state => {
      if (state.today) {
        for (let i of Array.from(state.inventories).reverse()) {
          if (i.inventory_type === 'continuous' && moment(i.date).endOf('day').isAfter(moment(state.today))) {
            return i;
          }
        }
      }
    },
    currentInventory: state => {
      for (let i of Array.from(state.inventories).reverse()) {
        var now = moment();
        if (moment(i.date).endOf('day').isAfter(now)) {
          return i;
        }
      }
    },
    inventoryRange: state => {
      if (state.today) {
        var is = _.orderBy(state.inventories, 'date');
        var bi = is.findIndex(i => new Date(i.date).getTime() > state.today.getTime());
        if (bi > 0) {
          return [moment(is[bi - 1].date).add(1, 'day').toDate(), new Date(is[bi].date)];
        }
      }
    }
  },
  mutations: {
  },
  actions: {
    async reloadInventories({ state, dispatch }) {
      const { data: { inventories } } = await apollo.query({
        query: READ_INVENTORIES,
        fetchPolicy: 'network-only'
      });
      state.inventories = inventories;
      dispatch('setToday', state.today);
    },
    setToday({ state, getters }, today) {
      state.today = today;
      const inventory = state.inventory;
      if (!inventory || moment(inventory.start_date).isAfter(today) || moment(inventory.date).isBefore(today)) {
        state.inventory = getters.todaysInventory;
      }
    },
    setInventory({ state }, inventory) {
      state.inventory = inventory;
      if (inventory) {
        const today = state.today;
        if (!today || moment(inventory.start_date).isAfter(today) || moment(inventory.date).isBefore(today)) {
          state.today = moment(state.inventory.date).startOf('day').toDate();
        }
      }

    }
  }
};

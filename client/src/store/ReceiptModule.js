import client from '@/client';
export default {
  namespaced: true,
  state: {
    receipts: null
  },
  getters: {
    receipts: state => state.receipts
  },
  actions: {
    async reloadReceipts ({ state }) {
      Object.assign(state, await client.receipts.list());
    }
  }
};

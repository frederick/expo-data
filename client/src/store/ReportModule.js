import client from '@/client';
// import _ from 'lodash'
export default {
  namespaced: true,
  state: {
    reports: []
  },
  getters: {
    reports: state => state.reports
  },
  actions: {
    async reloadReports ({ state }) {
      const reports = await client.reports.list();
      state.reports = reports;
    }
  }
};

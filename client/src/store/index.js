import Vuex from 'vuex';
// eslint-disable-next-line no-unused-vars
import VuexInit from './VuexInit';
import inventory from './InventoryModule';
import fetchAPI from '@/client/fetchAPI';

const store = new Vuex.Store({
  state: {
    loading: false,
    error: false,
    messages: [],
    maximized: false,
    user: null,
    groups: new Set()
  },
  getters: {
    isLoading: state => state.loading,
    error: state => state.error,
    messages: state => state.messages,
    user: state => state.user,

    groups: state => state.groups,
    maximized: state => state.maximized
  },
  mutations: {
    addMessage (state, message) {
      if (typeof message === 'string') {
        message = { message };
      }
      if (!message.level) {
        message.level = 'info';
      }
      if (!message.title) {
        message.title = 'Notification';
      }
      if (!message.key) {
        message.key = Math.random();
      }
      if (!message.timeout) {
        message.timeout = 5;
      }
      message.countdown = message.timeout;
      state.messages.push(message);
    },
    clearMessage (state, message) {
      state.messages.splice(state.messages.indexOf(message), 1);
    },
    setLoading (state, { loading }) {
      state.loading = loading;
    },
    setError (state, { error }) {
      state.error = error;
    },
    setMaximized (state, maximized) {
      state.maximized = maximized;
    },
    setUser (state, user) {
      state.user = user;
    },
    setGroups (state, groups) {
      state.groups = groups;
    },
    clearError (state) {
      state.error = false;
    }
  },
  modules: {
    inventory,
  }
});


fetchAPI.on('error', (error) => {
  store.commit('setError', { error });
});
fetchAPI.on('loading', function (loading) {
  store.commit('setLoading', { loading });
});
fetchAPI.on('config', function (config) {
  store.commit('setUser', config.decoded);
  fetchAPI({ endpoint: 'groups', method: 'GET' }).then(groups => {
    store.commit('setGroups', new Set(groups ?? []));
  });
});
export default store;

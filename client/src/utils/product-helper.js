
export const DEFAULT_FIELDS = ['pasteque_product_id', 'number', 'category_code', 'criteria', 'name', 'supplier', 'barcode', 'price', 'attributes', 'label', 'active'];
export const DEFAULT_ATTRIBUTES = ['Pu', 'VPC', 'Code', 'Pays', 'Poids', 'Reste', 'Stock', 'Groupe', 'PU_FRF', 'Remise', 'Statut', 'Critere', 'Val_VPC', 'Intitule', 'No_Piece', 'Codebarre', 'Livraisons', 'Production', 'Receptions', 'Ventes_ACC', 'Destruction', 'Fournisseur', 'NOM_ARTICLE', 'Reste_Stock', 'Prix_editeur', 'Prix_revient', 'Date_Creation', 'ServicePresse', 'Val_prod_mois', 'Val_Ventes_ACC', 'Val_Destruction', 'LivraisonSoiMeme', 'Prix_revient_FRF', 'Val_ServicePresse', 'Ventes_a_Facturer', 'Valeur_productions', 'Val_LivraisonSoiMeme', 'Val_Ventes_a_Facturer', 'LivraisonsDistributeurs', 'LivraisonResteMoisPreced', 'Stock_Presses_Communaute'];

export function createProduct () {
  var p = {};
  DEFAULT_FIELDS.forEach(a => { p[a] = null; });
  p.attributes = {};
  DEFAULT_ATTRIBUTES.forEach(a => { p.attributes[a] = null; });
  p.category_code = '0';
  return p;
}
export function compareProducts (p, pp) {
  var result = {};
  result.pasteque = pp;
  if (result.pasteque) {
    result.pstats = {};
    Object.assign(result.pstats, {
      barcodeMatches: p.barcode === pp.barcode,
      priceMatches: p.price.toFixed(4) === (pp.pricesell * (1 + pp.tax_rate)).toFixed(4),
      nameMatches: p.name === pp.label,
      categoryMatches: p.category?.name === pp.category,
      taxMatches: pp.tax_rate.toFixed(4) === (p.category?.rate / 100).toFixed(4),
      activeMatches: pp.visible === p.active
    });

    result.pstats.matchErr = Object.values(result.pstats).reduce((p, v) => p || !v, false);
  }
  return result;
}

export function variantName(variant, code = false) {
  if(!variant)
    return "No variant";
  let field = code ? 'code': 'value';
  if (variant.name) return variant.name;
  else if (variant.values?.length && variant.values.find(v => v.value?.id))
    return variant.values
      .map((v) => v.value?.id && `${this.avas && false ? this.avas[v.value?.id]?.value : v.value?.[field]}`)
      .filter(Boolean)
      .join(", ");
  else return "No name";
}

export function variantStyle(v) {
  let a = v?.values?.[0]?.value;
  let av = v?.values?.[0]?.attribute;
  if (a && av?.type === "color") {
    var c = a.color.substring(1); // strip #
    var rgb = parseInt(c, 16); // convert rrggbb to decimal
    var r = (rgb >> 16) & 0xff; // extract red
    var g = (rgb >> 8) & 0xff; // extract green
    var b = (rgb >> 0) & 0xff; // extract blue
    var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709
    let dark = false;
    if (luma < 80) {
      dark = true;
    }

    return (
      `color: ${dark ? '#ccc' : '#444'};  padding: 1px 5px; background-color: ${a.color};`
    );
  } else return "";
  
}

export function rest(item) {
  if(!item.variants?.length) return 0;
  return item.quantity - item.variants.reduce((a, b) => a + b.quantity, 0);
}
import { cur, dec } from '@/utils/decimals';
import _ from 'lodash';
import moment from 'moment';
import JsPDF from 'jspdf';
export function createReceipt() {
  return {
    id: null,
    ticket_id: null,
    note: "",
    ticket: null,
    number: null,
    date: moment().format('YYYY-MM-DD'),
    payment_details: "",
    customer: {
      id: null,
      firstname: "",
      lastname: "",
      email: "",
      phone1: "",
      phone2: "",
      fax: "",
      address: "",
      zipcode: "",
      city: "",
      region: "",
      country: "",
      note: ""
    }
  };
}
require('./fonts/Noto-Regular');
require('./fonts/Noto-Bold');
JsPDF.API.print = function (v, options, flags) {
  if (!v) return;
  var doc = this;
  options = options || {};
  if (options.firstLineBold) {
    var lf = v.indexOf('\n');
    this.print(
      v.substring(0, lf || v.length),
      {
        ...options,
        fontStyle: 'bold',
        fontSize: options.firstLineBig ? 14 : undefined,
        firstLineBold: false,
        marginY: 0.035
      },
      flags
    );
    if (lf) {
      this.print(
        v.substring(lf + 1),
        {
          ...options,
          fontStyle: 'normal',
          firstLineBold: false
        },
        flags
      );
    }
    return;
  }
  if (options.fontSize) {
    var fontSize = doc.internal.getFontSize();
    doc.setFontSize(options.fontSize);
  }
  if (options.fontStyle) {
    var font = doc.getFont();
    doc.setFont(doc.getFont().fontName, options.fontStyle);
  }
  doc.text(v, this.positionX, this.positionY, { ...flags, baseline: 'top' });
  var dim = this.getTextDimensions(v);
  var lines = v.split('\n').length;
  this.positionY += lines * doc.getLineHeight() / doc.internal.scaleFactor + (options.marginY || 3.5);

  if (fontSize) {
    doc.setFontSize(fontSize);
  }
  if (font) {
    doc.setFont(doc.getFont().fontName, font.fontStyle);
  }
  return dim;
};
require('jspdf-autotable');

export function renderReceipt(r, {paymentMode, header, location}) {
  const ticket = r.ticket.rawTicket ? r.ticket.rawTicket : r.ticket;


  var doc = new JsPDF('p', 'mm');

  doc.setFont('helvetica'); // set font
  var columns = [
    {
      header: 'Article',
      dataKey: 'label',
      style: {
        cellWidth: 74
      }
    },
    {
      header: 'PU',
      dataKey: 'pu',
      style: {
        cellWidth: undefined,
        halign: 'right'
      }
    },
    {
      header: 'Qté',
      dataKey: 'quantity',
      style: {
        cellWidth: undefined
      }
    },
    {
      header: 'TTC',
      dataKey: 'ttc',
      style: {
        cellWidth: undefined,
        halign: 'right'
      }
    },
    {
      header: 'Taux',
      dataKey: 'tax',
      style: {
        cellWidth: undefined,
        halign: 'right'
      }
    },
    {
      header: 'HT',
      dataKey: 'ht',
      style: {
        cellWidth: undefined,
        halign: 'right'
      }
    },
    {
      header: 'TVA',
      dataKey: 'tva',
      style: {
        cellWidth: undefined,
        halign: 'right'
      }
    }
  ];
  var lines = ticket.lines.map(l => ({
    label: l.productlabel,
    pu: cur(l.taxedunitprice),
    quantity: dec(l.quantity),
    ttc: cur(l.finaltaxedprice),
    tax: `${dec(l.taxrate * 100)}%`,
    ht: cur(l.finaltaxedprice / (1 + l.taxrate)),
    tva: cur(l.finaltaxedprice - l.finaltaxedprice / (1 + l.taxrate))
  }));
  var foot = [];
  var c1 = 'tax';
  var c2 = 'ht';
  foot.push({
    [c1]: 'Sous total',
    [c2]: cur(ticket.total_ht)
  });

  for (let t of ticket.taxes) {
    foot.push({
      [c1]: `TVA ${dec(t.taxrate * 100)}%`,
      [c2]: cur(t.amount)
    });
  }

  foot.push({
    [c1]: 'Total',
    [c2]: cur(ticket.total)
  });

  var columnStyles = columns.map(c => {
    return {
      key: c.dataKey,
      ...c.style
    };
  });
  columnStyles = _.keyBy(columnStyles, 'key');
  doc.autoTable({
    body: lines,
    foot,
    columns,
    theme: 'plain',
    columnStyles,
    footStyles: {
      halign: 'right'
    },
    headStyles: {
      halign: 'left'
    },

    startY: 112, // 103.46,
    didParseCell({ cell, column }) {
      if (cell.section === 'head') {
        if (['pu', 'ttc', 'tax', 'ht', 'tva'].includes(column.dataKey)) {
          cell.styles.halign = 'right';
          cell.styles.cellPadding = { top: 1.8, right: 3.6, bottom: 1.8, left: 1.8 };
        } else {
          cell.styles.halign = 'left';
        }
      }
    },
    didDrawPage(data) {
      if (data.pageCount === 1) {
        doc.positionX = 16;
        doc.positionY = data.cursor.y + 7;
        doc.print(
          `Montant réglé le ${moment(ticket.date).format('DD/MM/YYYY')} 
Mode de règlement : ${paymentMode}`, {
          fontSize: 10
        });
        doc.setFontSize(10);
        doc.positionX = 15;
        doc.positionY = 14;

        // doc.text("Order " + store.getters['stock/getStocksById'][order.stock].name + " (" + moment(order.orderDate).format('DD.MM.YYYY') + ") ", 15, 20);
        doc.print(header,
          {
            firstLineBold: true,
            firstLineBig: true,
            fontSize: 8
          }
        );

        doc.positionY = 14;

        doc.positionX = 190;

        doc.print(`Ticket #${r.number}`, {
          fontStyle: 'bold',
          fontSize: 14
        }, {
          align: 'right'
        });

        doc.positionX = 190;

        doc.positionY = 40;
        doc.print(
          `${location}, le ${moment(r.date).format('DD/MM/YYYY')} `, {}, { align: 'right' }
        );
        doc.positionX = 105;
        doc.positionY = 56;
        doc.print(
          `${r.customer.firstname} ${r.customer.lastname}
${r.customer.address}`, {
          firstLineBold: true,

          marginY: 2
        });
        doc.print(
          `${(r.customer.zipcode || '') + ' ' + (r.customer.city || '')}
${r.customer.country} ${r.customer.region ? `(${r.customer.region})` : ''}`, {
        });
        // fold marks
        doc.setDrawColor(10, 10, 10);
        doc.line(7, 106, 12, 106);
        doc.positionX = 16;
        doc.positionY += 2;
      }
    }
  });

  var width = doc.internal.pageSize.width;
  var height = doc.internal.pageSize.height;
  var headerText = `Ticket #${r.number}`;
  for (let i = 1; i <= doc.internal.getNumberOfPages(); i++) {
    doc.setPage(i);
    var text =
      headerText + '   -   Page ' + i + '/' + doc.internal.getNumberOfPages();

    doc.setFontSize(10);
    doc.text(text, width - doc.getTextDimensions(text).w - 16, height - 16);
    if (i > 1) {
      doc.text(text, width - doc.getTextDimensions(text).w - 16, 16);
    }
  }

  doc.save(`Receipt-${r.number}.pdf`);
}


export function renderWebshopOrders(orders, fields) {

  var doc = new JsPDF('l', 'mm');
  fields = fields.filter(f => f.key !== 'order_color' && f.key !== "show_details" && f.key !== 'state' && f.key !== 'grand_total' && f.key !== 'shipping_amount');
  orders = _.orderBy(orders, 'recipient');
  let body = orders.map(o => {
    return fields.map(f => {
      let val = o[f.key];
      if (f.key === 'created_at') {
        val = moment(val).format('YYYY-MM-DD');
      }
      return val;
    });
  });
  doc.autoTable({
    doc,
    rowPageBreak: 'avoid',
    styles: {
      fontSize: 8
    },
    head: [fields.map(f => {
      let val = f.label ?? f.key;
      if (f.key === 'country_id') {
        val = 'country';
      }
      return val;
    })],
    body
  });
  doc.setFont('helvetica'); // set font
  doc.save(`orders-exported-${moment().format('YYYY-MM-DD-HH-mm')}.pdf`);

}
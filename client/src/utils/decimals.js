export const cur_formatter = new Intl.NumberFormat('fr-FR', {
  style: 'currency',
  currency: 'EUR',
  minimumFractionDigits: 2
});

export const dec_formatter = new Intl.NumberFormat('fr-FR', {
  style: 'decimal',
  minimumFractionDigits: 0
});

export function cur (n) {
  return cur_formatter.format(n);
}
export function dec (n) {
  return dec_formatter.format(n);
}

export function decd (n, d) {
  var formatter = new Intl.NumberFormat('fr-FR', {
    style: 'decimal',
    minimumFractionDigits: d
  });
  return formatter.format(n);
}

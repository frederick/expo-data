window.addEventListener('keydown', function (ev) {
  // hotkey saving
  if (ev.target.closest('table.keynav')) {
    var table = ev.target.closest('table.keynav');
    /*eslint-disable*/
        // keyboard nav for tables
        let td;
        var rowDelta = 1;
        const inputs =
            "input:not([disabled]):not([readonly]),select:not([disabled]):not([readonly]),textarea:not([disabled]):not([readonly])";

        //var allinputs = Array.from(table.querySelectorAll(inputs));
        if (!ev.target.matches(inputs)) {
            return
        }
        var focus;
        for (let i = 0; i < table.rows.length; i++) {
            let row = table.rows[i];
            if (row.querySelector(":focus")) {
                var rinputs = Array.from(row.querySelectorAll(inputs));
                var idx = rinputs.indexOf(ev.target);

                switch (ev.code) {
                    case "ArrowUp":
                    case "ArrowDown":
                        var mult = 1;
                        if (ev.code === "ArrowUp") {
                            mult = -1;
                        }
                        while (table.rows[i + mult]) {
                            focus = table.rows[i + mult].querySelectorAll(inputs)[idx];
                            if (focus) {
                                break
                            } else if (mult < 0) mult--
                            else if (mult > 0) mult++
                        }
                        break;

                    case "ArrowLeft":
                    case "ArrowRight":
                        mult = 1;
                        if (ev.code === "ArrowLeft") {
                            mult = -1;
                        }
                        let len = ev.target.selectionEnd - ev.target.selectionStart
                        if (
                            !ev.target.value ||
                            mult === 1 && ev.target.selectionStart === ev.target.value.length ||
                            mult === -1 && ev.target.selectionStart === 0 && len === 0 ||
                            //ev.target.selectionStart === ev.target.selectionEnd ||
                            mult === 1 && len === ev.target.value.length ||
                            ev.target.selectionStart === undefined ||
                            ev.target.selectionStart === null
                        ) {
                            var idx = rinputs.indexOf(ev.target);
                            if (idx + mult >= 0 && idx + mult < inputs.length)
                                focus = rinputs[idx + mult];
                        }
                }
            }
        }
        if (focus) {
            focus.focus();
            focus.scrollIntoView({
                behavior: 'smooth',
                block: 'center',
                inline: 'center'
            })
            focus.select && setTimeout(() => focus.select(), 10);
            ev.preventDefault();
        }
    } else {
        return true;
    }
});
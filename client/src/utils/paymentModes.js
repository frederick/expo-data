module.exports = {
  methods: [
    {
      pasteque: 'magcard',
      polimod: 'C-BL',
      french: 'Carte bancaire',
      barcode: '9999999990038'
    },
    {
      pasteque: 'cash',
      french: 'Espèces',
      polimod: 'RECU',
      barcode: '9999999900006'
    },
    {
      pasteque: 'cheque',
      polimod: 'CHEQ',
      french: 'Chèque',
      barcode: '9999999990007'
    }
  ],
  methodNames: {
    'CT�': 'CT�', // ????
    'Cheque': 'CHEQ',
    'Credit Card': 'C-BL',
    'Cash': 'RECU'

  },
  methodCodes: {
    'CT�': 'CT�', // ????
    'Cheque': '9999999990007',
    'Credit Card': '9999999990038',
    'Cash': '9999999900006'

  }
};

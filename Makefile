build:
	podman build -t expo-data .

run:
	podman run --env-file=server/.env --rm -p 3000:3000 expo-data

public:
	bash -c "cd ../ && make public"
const Joi = require('@hapi/joi')

// add an upper bound
const saneString = Joi.string().max(2048)
const optionalString = saneString.optional().allow('', null).trim()

const id = Joi.number().integer().positive()

const idArray = Joi.array().items(id.required())

module.exports = {
  optionalString,
  saneString,
  id,
  idArray
}
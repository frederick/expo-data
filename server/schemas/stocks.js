const Joi = require('@hapi/joi')
const { optionalString, id } = require('./common')
const products = Joi.array().items(Joi.object().keys({
  id: id.required(),
  quantity: Joi.number().required(),
  notes: optionalString
}))

const stock =
  Joi.object().keys({
    name: optionalString.required(),
    description: optionalString,
    id
  })


const countingListKeys =
{
  id: id.allow(null),
  stock_id: id.required(),
  inventory_id: id.allow(null),
  title: Joi.string().required(),
  date: Joi.date().required(),
  comment: Joi.string(),
  validated: Joi.boolean()
}
let createCountingList = Joi.object().keys({
  ...countingListKeys,
})

let updateCountingList = Joi.object().keys({
  ...countingListKeys,
})

const product_ids = Joi.array().items(Joi.number().integer().required())

module.exports = {
  createStock: stock,
  updateStock: stock,
  product_ids,
  products,
  createCountingList,
  updateCountingList
}
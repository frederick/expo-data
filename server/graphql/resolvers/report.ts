
import gt from "../../types/graphql";
import knex from '../../db/knex';

import { GraphQLResolveInfo } from "graphql";
import Context from './loaders'
import moment from "moment";
import _ from "lodash";
import GraphQLContext from "../GraphQLContext";

class ReportResolver {
	id?: number;

	constructor(r) {
		Object.assign(this, r);
	}
}

const reportfields = ["name", "description", "type", "datepicker", "sql", "template"];
export default {
	async report(args: gt.QueryReportArgs, context: GraphQLContext, info: GraphQLResolveInfo) {
		context.requireGroups(['admin']);
		const report = await knex('report')
			.select(['id', 'version', ...reportfields])
			.where({ 'id': args.id })
			.first()
		return new ReportResolver(report)
	},
	async reports(args, context: GraphQLContext, info: GraphQLResolveInfo) {
		context.requireGroups(['reports']);
		const reports = await knex('report')
			.select(['id', 'version', ...reportfields])
			.orderBy('name', 'asc')
		return reports.map(i => new ReportResolver(i));
	},
	async createReport({ report }: gt.MutationCreateReportArgs, context, info) {
		context.requireGroups(['admin']);
		const tx = context.knexTransaction;

		const [id] = await knex('report').insert({
			..._.pick(report, reportfields),

			version: 1
		})
			.returning('id')
			.transacting(tx);
		return id;
	},
	async updateReport(args: gt.MutationUpdateReportArgs, context: GraphQLContext, info) {
		context.requireGroups(['admin']);
		const tx = context.knexTransaction;

		const [id] = await knex('report').update({
			..._.pick(args.report, reportfields),
		})
			.increment('version')
			.returning('id')
			.where({
				id: args.id
			})
			.transacting(tx);
		return id;
	},
	async deleteReport(args: gt.MutationDeleteReportArgs, context: GraphQLContext, info) {
		context.requireGroups(['admin']);
		const tx = context.knexTransaction;
		await knex('report')
			.delete()
			.where({ id: args.id })
			.transacting(tx);

		return args.id;
	}

}
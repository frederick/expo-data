
import gt from "../../types/graphql";
import knex from '../../db/knex';

import { GraphQLResolveInfo } from "graphql";
import Context from './loaders'
import moment from "moment";
import _ from "lodash";
import GraphQLContext from "../GraphQLContext";

class InventoryResolver {
	id?: number;

	constructor(r) {
		Object.assign(this, r);
	}
}

const inventoryfields = ["date", "period_start", "inventory_type", "comments"];
export default {
	async inventory(args: gt.QueryInventoryArgs, context: GraphQLContext, info: GraphQLResolveInfo) {
		context.requireGroups(['inventory']);
		const inventory = await knex('vw_inventory_with_start_date')
			.select('i.id', 'i.date', 'i.period_start', 'i.inventory_type', 'i.comments', 'i.version', 'i.start_date')
			.from({ i: 'vw_inventory_with_start_date' })
			.where({ 'i.id': args.id })
			.first()
		return new InventoryResolver(inventory)
	},
	async inventories(args, { loaderContext }: { loaderContext: Context }, info: GraphQLResolveInfo) {

		const inventories = await knex('vw_inventory_with_start_date')
			.select('i.id', 'i.date', 'i.period_start', 'i.inventory_type', 'i.comments', 'i.version', 'i.start_date')
			.from({ i: 'vw_inventory_with_start_date' })
			.orderBy('i.date', 'desc')
		return inventories.map(i => new InventoryResolver(i));
	},
	async createInventory({ inventory }: gt.MutationCreateInventoryArgs, context: GraphQLContext, info) {
		context.requireGroups(['rw', 'inventory']);
		const tx = context.knexTransaction;

		const [id] = await knex('inventory').insert({
			..._.pick(inventory, inventoryfields),

			version: 1
		})
			.returning('id')
			.transacting(tx);
		return id;
	},
	async updateInventory(args: gt.MutationUpdateInventoryArgs, context: GraphQLContext, info) {
		context.requireGroups(['rw', 'inventory']);
		const tx = context.knexTransaction;

		const [id] = await knex('inventory').update({
			..._.pick(args.inventory, inventoryfields),
		})
			.increment('version')
			.returning('id')
			.where({
				id: args.id
			})
			.transacting(tx);
		return id;
	},
	async deleteInventory(args: gt.MutationDeleteInventoryArgs, context: GraphQLContext, info) {
		context.requireGroups(['rw', 'inventory']);
		const tx = context.knexTransaction;
		await knex('inventory')
			.delete()
			.where({ id: args.id })
			.transacting(tx);

		return args.id;
	}

}
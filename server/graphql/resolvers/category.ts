
import { Category } from "../../types/graphql";
import { GraphQLResolveInfo } from "graphql";
import Context from './loaders'
import GraphQLContext from "../GraphQLContext";


export default {
	async categories(args, context: GraphQLContext, info: GraphQLResolveInfo) {
		context.requireGroups(['products'])
		return await context.loaderContext.categoryLoader.loadAll();
	}
}
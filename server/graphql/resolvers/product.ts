
import gt from "../../types/graphql";
import knex from '../../db/knex';

import { GraphQLResolveInfo } from "graphql";
import type Context from './loaders'
import { loadPastequeProducts } from './loaders/pasteque_product';
import { requireGroup, reqRW } from '../util/require-group';
import type GraphQLContext from "../GraphQLContext";
// (new Context().productLoader.load(1097).then(async (p:any) => console.log(await p.variants())))

export default {
	async products(args, { loaderContext, requireGroups }: GraphQLContext, info: GraphQLResolveInfo) {
		requireGroups(['products']);
		return await loaderContext.productLoader.loadAll();
	},
	async product({ id }, { loaderContext, requireGroups }: GraphQLContext, info: GraphQLResolveInfo) {
		requireGroups(['products']);
		return await loaderContext.productLoader.load(id);
	},
	async productByNumber({ number }, { loaderContext, requireGroups }: GraphQLContext, info: GraphQLResolveInfo) {
		requireGroups(['products']);
		let [id] = await knex('product').pluck('id').where({ number })
		if (id)
			return await loaderContext.productLoader.load(id);
		else
			return null;
	},
	async missingPastequeProducts(__, { loaderContext, requireGroups }: GraphQLContext) {
		requireGroups(['products']);
		let products = await knex('pasteque.products')
			.select([
				'p.id', 'p.reference', 'p.label', 'p.barcode',
				'p.pricesell', 'p.visible',
				knex.ref('c.reference').as('category'),
				knex.ref('t.rate').as('tax_rate'),
				knex.ref('t.label').as('tax'),
				knex.raw('p.pricesell * (1+t.rate) as price')
			])
			.from<gt.PastequeProduct>({ p: 'pasteque.products' })
			.whereNotExists(
				knex('product').select('id').where('number', knex.ref('p.id')))
			.leftJoin('pasteque.categories as c', 'c.id', '=', 'p.category_id')
			.leftJoin('pasteque.taxes as t', 't.id', '=', 'p.tax_id')
		return products;
	},
	async unsynchronizedProducts(__, { loaderContext, requireGroups }: GraphQLContext) {
		requireGroups(['products']);

		let products = await knex
			.raw(`SELECT p.* FROM vw_product_matches p WHERE p.pasteque_id IS NULL
		OR NOT(p.barcode_matches AND p.price_matches AND p.name_matches AND p.category_matches AND p.rate_matches AND p.active_matches)
		`)
		let result = await loaderContext.productLoader.loadMany(products.rows.map(r => r.id));
		return result.map((p, i) => ({
			...p,
			syncStats: {
				...products.rows[i]
			}
		}))
	},
	async createProduct({ product }: gt.MutationCreateProductArgs, context: GraphQLContext, info) {
		context.requireGroups(['products', 'rw']);

		const tx = context.knexTransaction;
		const [id] = await knex('product').insert({
			...product,
			version: 1
		})
			.returning('id')
			.transacting(tx);

		return id;
	},
	async updateProduct({ id, version, product }: gt.MutationUpdateProductArgs, context: GraphQLContext) {
		context.requireGroups(['products', 'rw']);
		const tx = context.knexTransaction;
		await knex('product').where({ id }).update({
			...product,
		}).increment('version')
			.transacting(tx);

		return id;
	},
	async deleteProduct({ id, version }: gt.MutationDeleteProductArgs, context: GraphQLContext) {
		context.requireGroups(['products', 'rw']);
		const tx = context.knexTransaction;
		await knex('product').where({ id }).delete()
			.transacting(tx);

		return id;
	},
	async updatePastequeProduct(args: gt.MutationUpdatePastequeProductArgs, context: GraphQLContext) {
		context.requireGroups(['products', 'rw']);
		const p = args.pasteque_product;
		await knex('pasteque.products')
			.update({
				reference: `${p.number} ${p.name}`,
				label: p.name,
				barcode: p.barcode,
				pricesell: p.price / (1 + p.tax_rate / 100),
				visible: p.active,
				category_id: knex('pasteque.categories').select('id').where('label', p.category),
				tax_id: p.tax_id,
			})
			.where({ id: args.pasteque_product_id })
			.transacting(context.knexTransaction);

		return args.pasteque_product_id;
	},
	async createPastequeProduct(args: gt.MutationCreatePastequeProductArgs, context: GraphQLContext) {
		context.requireGroups(['products', 'rw']);
		const p = args.pasteque_product;

		const [id] = await knex('pasteque.products')
			.insert({
				id: p.number,
				reference: `${p.number} ${p.name}`,
				label: p.name,
				barcode: p.barcode ?? 0,
				pricesell: p.price / (1 + p.tax_rate / 100),
				visible: p.active,
				category_id: knex('pasteque.categories').select('id').where('label', p.category),
				tax_id: p.tax_id,
				scaled: false,
				scaletype: 0,
				scalevalue: 0,
				disp_order: 0,
				hasimage: false,
				discountenabled: false,
				discountrate: 0,
				prepay: false,
				composition: false
			})
			.transacting(context.knexTransaction)
			.returning('id');

		return id;
	},
	async updateProductCustomAttributes({ product_id, product_version, attributes }: gt.MutationUpdateProductCustomAttributesArgs, context: GraphQLContext) {
		context.requireGroups(['products', 'rw']);
		const tx = context.knexTransaction;
		await knex('product').where({ id: product_id }).update({
			attributes,
		}).increment('version')
			.transacting(tx);

		return product_id;
	},
	async productAttributes(args, context: GraphQLContext, info) {
		context.requireGroups(['products']);
		const attributes = await knex('product_attribute')
			.select(['pa.id', 'pa.code', 'pa.name', 'pa.description', 'pa.type', 'pa.values'])
			.from('vw_product_attributes as pa')
			.orderBy('pa.id');
		return attributes;
	},
	async productAttribute(args: gt.QueryProductAttributeArgs, context: GraphQLContext, info): Promise<gt.ProductAttribute> {
		context.requireGroups(['products']);
		const attribute = await knex('product_attribute')
			.select(['pa.id', 'pa.code', 'pa.name', 'pa.description', 'pa.type', 'pa.values'])
			.from('vw_product_attributes as pa')
			.where('pa.id', args.id)
			.first();
		return attribute;
	},


	async createProductVariant({ product_id, product_version, variant }: gt.MutationCreateProductVariantArgs, context: GraphQLContext, info): Promise<Number> {
		context.requireGroups(['products', 'rw']);
		const tx = context.knexTransaction;
		await knex('product')
			.increment('version')
			.where('id', '=', product_id)
			.transacting(tx);
		const [variant_id] = await knex('product_variant')
			.insert({
				code: variant.code,
				name: variant.name,
				sku: variant.sku,
				product_id
			}).returning('id')
			.transacting(tx);
		if (variant.values.length) {
			await knex('product_variant_attribute_value')
				.insert(variant.values.map(v => ({
					variant_id,
					attribute_id: v.attribute_id,
					attribute_value_id: v.value_id
				})))
				.transacting(tx);

		}
		return variant_id;
	},
	async updateProductVariant({ product_id, product_version, variant_id, variant }: gt.MutationUpdateProductVariantArgs, context: GraphQLContext, info): Promise<Number> {
		context.requireGroups(['products', 'rw']);
		const tx = context.knexTransaction;
		await knex('product')
			.increment('version')
			.where('id', '=', product_id)
			.transacting(tx);
		const res = await knex('product_variant')
			.update({
				code: variant.code,
				name: variant.name,
				sku: variant.sku
			}, 'id')
			.where('id', variant_id)
			.andWhere('product_id', product_id)
			.transacting(tx);
		await knex('product_variant_attribute_value')
			.delete()
			.where({
				variant_id
			})
			.whereNotIn('id', variant.values.map(v => v.id) as readonly any[])
			.transacting(tx);
		if (res.length && variant.values.length) {
			await knex('product_variant_attribute_value')
				.insert(variant.values.map(v => ({
					variant_id,
					attribute_id: v.attribute_id,
					attribute_value_id: v.value_id
				})))
				.onConflict(['variant_id', 'attribute_id'])
				.merge()
				.transacting(tx);
		}
		return variant_id;
	},
	async deleteProductVariant({ product_id, product_version, variant_id, variant }: gt.MutationUpdateProductVariantArgs, context: GraphQLContext, info): Promise<Number> {
		context.requireGroups(['products', 'rw']);
		const tx = context.knexTransaction;

		await knex('product')
			.increment('version')
			.where('id', '=', product_id)
			.transacting(tx);
		await knex('product_variant')
			.delete().where('id', '=', variant_id);

		return variant_id;
	},
	async createProductAttribute({ productAttribute }: gt.MutationCreateProductAttributeArgs, context: GraphQLContext, info): Promise<Number> {
		context.requireGroups(['products', 'rw']);
		const tx = context.knexTransaction;
		const [id] = await knex('product_attribute')
			.insert({
				code: productAttribute.code,
				description: productAttribute.description,
				name: productAttribute.name,
				type: productAttribute.type,
			})
			.transacting(tx)
			.returning('id');

		return id;
	},
	async updateProductAttribute({ id, productAttribute: pa }: gt.MutationUpdateProductAttributeArgs, context: GraphQLContext, info): Promise<Number> {
		context.requireGroups(['products', 'rw']);
		const tx = context.knexTransaction;
		await knex('product_attribute')
			.update({
				code: pa.code,
				description: pa.description,
				name: pa.name,
				type: pa.type,
			})
			.where({ id })
			.transacting(tx);

		const ids = await knex('product_attribute_value')
			.insert(pa.values.map(v => ({
				value: v.value,
				code: v.code,
				color: pa.type === 'color' ? v.color : null,
				attribute_id: pa.id,
				id: v.id
			})))
			.onConflict(['id', 'attribute_id'])
			.merge(['value', 'code', 'color'])
			.returning('id')
			.transacting(tx)
			;

		await knex('product_attribute_value')
			.delete()
			.where('attribute_id', pa.id)
			.whereNotIn('id', ids as readonly number[])
			.transacting(tx);

		return id;
	},

	async deleteProductAttribute({ id }: gt.MutationDeleteProductAttributeArgs, context: GraphQLContext, info): Promise<Number> {
		context.requireGroups(['products', 'rw']);
		const tx = context.knexTransaction;
		await knex('product_attribute')
			.delete()
			.where({ id })
			.transacting(tx);


		return id;
	},


	async changeProductAttributeValuePosition(args: gt.MutationChangeProductAttributeValuePosistionArgs, context: GraphQLContext, info) {
		context.requireGroups(['products', 'rw']);
		const attribute = await this.productAttribute({ id: args.product_attribute_id }, context, info);
		let oldIndex = attribute.values.findIndex(v => v.id === args.product_attribute_value_id);
		if (oldIndex === -1) {
			throw new Error("Product attribute value not found.")
		}
		let attr = attribute.values[oldIndex];

		attribute.values = [...attribute.values.slice(0, oldIndex), ...attribute.values.slice(oldIndex + 1, attribute.values.length)];
		attribute.values.splice(args.position, 0, attr);
		for (let i in attribute.values) {
			let v = attribute.values[i];
			await knex('product_attribute_value').transacting(context.knexTransaction)
				.update({
					position: i
				}).where({
					id: v.id
				})
		}

		return true;
	},
}

import gt from "../../types/graphql";
import knex from '../../db/knex';

import { GraphQLResolveInfo } from "graphql";
import Context from './loaders'
import moment from "moment";
import GraphQLContext from "../GraphQLContext";

class TicketResolver {
	id?: number;

	constructor(t) {
		Object.assign(this, t);
	}
	async taxes() {
		return await knex('pasteque.tickettaxes')
			.select(['tt.tax_id', 'tax.label as tax_label', 'tt.taxrate', 'tt.base', 'tt.amount'])
			.from({ tt: 'pasteque.tickettaxes' })
			.leftJoin('pasteque.taxes as tax', 'tax.id', 'tt.tax_id')
			.where('ticket_id', this.id!)
	}

	async payment_mode() {
		const row = await knex('pasteque.ticketpayments')
			.select([knex.raw("STRING_AGG(pm.reference,', ') as references")])
			.from({ tp: 'pasteque.ticketpayments' })
			.leftJoin('pasteque.paymentmodes as pm', 'pm.id', 'tp.paymentmode_id')
			.where('ticket_id', this.id!)
			.first();
		return row.references
	}

	async lines() {
		return await knex('pasteque.ticketlines')
			.select('*')
			.where('ticket_id', this.id!);
	}
	async rawTicket() {
		// TODO rewrite joins as subquery
		const { rows } = await knex.raw(`
		SELECT 
		t.*, t.finaltaxedprice as total, t.finalprice as total_ht,
		c.reference as cash_register, SUM(tl.quantity) as items,
		(SELECT json_agg(tt.*) FROM pasteque.tickettaxes  tt WHERE tt.ticket_id=t.id)
		as taxes,
		string_agg(distinct pm.reference,', ') as payment_mode,
		string_agg(distinct pm.id::varchar,', ') as payment_mode_id,
		string_agg(distinct pm.label,', ') as payment_mode_label,
		json_agg(tl.*) as lines
		FROM pasteque.tickets t 
		LEFT JOIN pasteque.cashregisters c ON c.id=t.cashregister_id
		LEFT JOIN pasteque.ticketpayments tp ON tp.ticket_id=t.id
		LEFT JOIN pasteque.paymentmodes pm ON pm.id=tp.paymentmode_id,
		pasteque.ticketlines tl
		WHERE t.id=? AND tl.ticket_id=t.id
		GROUP BY ${ticket_fields.map(tf => knex.ref(`t.${tf}`))}, c.reference
		ORDER BY MAX(tl.disporder)
		;
		`, this.id!)

		return rows[0];
	}
}
const ticket_fields = 'id,cashregister_id,user_id,customer_id,tariffarea_id,discountprofile_id,sequence,number,date,custcount,discountrate,price,taxedprice,finalprice,finaltaxedprice,custbalance'.split(',');

export default {
	async tickets(args: gt.QueryTicketsArgs, context: GraphQLContext, info: GraphQLResolveInfo) {
		context.requireGroups(['tickets']);
		const tickets = await knex('pasteque.tickets')
			.select(['t.*', 't.finaltaxedprice as total', 't.finalprice as total_ht', 'c.reference as cash_register', knex.raw('SUM(tl.quantity) as items')])
			.from({ t: 'pasteque.tickets' })
			.leftJoin('pasteque.cashregisters as c', 'c.id', 't.cashregister_id')
			.leftJoin('pasteque.ticketlines as tl', 'tl.ticket_id', 't.id')
			.where(knex.raw("DATE(t.date) = ?", moment(args.date).format('YYYY-MM-DD')))
			.groupBy([...ticket_fields.map(tf => `t.${tf}`), 'total', 'total_ht', 'cash_register']);
		return tickets.map(t => new TicketResolver(t));
	},
	async ticket(args: gt.QueryTicketArgs, context: GraphQLContext, info: GraphQLResolveInfo) {
		context.requireGroups(['tickets']);

		const ticket = await knex('pasteque.tickets')
			.select(['t.*', 't.finaltaxedprice as total', 't.finalprice as total_ht', 'c.reference as cash_register', knex.raw('SUM(tl.quantity) as items')])
			.from({ t: 'pasteque.tickets' })
			.leftJoin('pasteque.cashregisters as c', 'c.id', 't.cashregister_id')
			.leftJoin('pasteque.ticketlines as tl', 'tl.ticket_id', 't.id')
			.where('ticket_id', args.id)
			.groupBy([...ticket_fields.map(tf => `t.${tf}`), 'total', 'total_ht', 'cash_register'])
			.first();
		if (!ticket) {
			return null;
		}
		return new TicketResolver(ticket);
	},
	async ticketStats(args: gt.QueryTicketStatsArgs, context: GraphQLContext, info: GraphQLResolveInfo) {
		context.requireGroups(['tickets']);

		const stats = await knex.raw(`
		SELECT
			s.tdate::TEXT as date,
			s.cash_registers,
			s.count
		FROM
		(
		SELECT 
		DATE(t.date) as tdate,
		count(t.id)::int,
		count(DISTINCT t.cashregister_id)::int as cash_registers
		FROM pasteque.tickets t
		
		WHERE t.date BETWEEN :m AND (:m::timestamp + INTERVAL '1 MONTH')

		GROUP BY tdate
		ORDER BY tdate

		) s
		;`, { m: `${args.year}-${args.month.toString().padStart(2, '0')}-01` })
		return stats.rows;
	},
}
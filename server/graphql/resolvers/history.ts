
import gt from "../../types/graphql";

import { GraphQLResolveInfo } from "graphql";
import GraphQLContext from "../GraphQLContext";
import knex from '../../db/knex';



export default {
	async historyChanges(args: gt.QueryHistoryChangesArgs, context: GraphQLContext, info: GraphQLResolveInfo) {
    var table, idColumn;
    switch (args.entity) {
      case "product":
        idColumn = "id";
        table = ["product", "tz_product"];
        break;
      case "receipt":
        idColumn = "id";
        table = ["receipt", "tz_receipt"];
        break;
      case "inventory":
          idColumn = "id";
          table = ["inventory", "tz_inventory"];
          break;
      
      case "report":
          idColumn = "id";
          table = ["report", "tz_report"];
          break;
      default:
        throw `Unknown entity type ${args.entity}`;
    }

    var events = await knex.raw(
      `SELECT e.action_tstamp_tx as date, COALESCE(e.app_user_name, e.session_user_name) as user, e.changed_fields, json_agg((SELECT av FROM (SELECT e2.schema_name, e2.table_name , e2.action,e2.row_data, e2.changed_fields) as av)) AS changed_objects
        FROM audit.logged_actions e
        LEFT JOIN audit.logged_actions e2 ON e2.action_tstamp_tx=e.action_tstamp_tx AND e2.transaction_id=e.transaction_id
        WHERE e.table_name=ANY(:table) AND e.row_data->:idColumn=:id
        GROUP BY e.event_id
        ORDER BY date DESC`,
      {table, id: args.id, idColumn}
    );
    events = events.rows;

    return events;
	}
}
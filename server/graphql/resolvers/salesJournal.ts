
import gt from "../../types/graphql";
import knex from '../../db/knex';

import { GraphQLResolveInfo } from "graphql";
import Context from './loaders'
import moment from "moment";
import _ from "lodash";
import GraphQLContext from "../GraphQLContext";

class SalesJournalResolver {
  id?: number;
  code?: string;

  constructor(r) {
    Object.assign(this, r);
  }

  async entries(args: gt.SalesJournalEntriesArgs) {
    var result = await knex.raw(`
    WITH 
    inv AS (SELECT i.* FROM vw_inventory_with_start_date i WHERE i.id=:inventory),
    time_series AS
      (SELECT d::date as date FROM 
        (SELECT generate_series(inv.start_date, LEAST(inv.date,now()::date),'1 days')  as d FROM inv) d
     )
    SELECT
      ts.date::TEXT,
      ppm.name as method,
      --se.amount as amount,
      MAX(t.total) IS NULL OR MIN(t.closedate) IS NOT NULL as closed,
      --ppm.name as method,
      COALESCE(SUM(sje.amount),0) as amount,
      COALESCE(SUM(t.total::numeric),0) as pasteque_amount,
      COALESCE(SUM(ses.total::numeric),0) as pasteque_session_amount,
      -COALESCE(SUM(t.total::numeric),0) + COALESCE(SUM(sje.amount),0) as diff
    FROM 
    time_series ts
    CROSS JOIN payment_method ppm
    LEFT JOIN (
      SELECT
      timezone('Europe/Paris'::text, timezone('utc'::text, t.date))::date as ldate,
      SUM(t.finaltaxedprice) as total,pm.id as pmethod,MAX(ses.closedate) AS closedate 
      FROM pasteque.tickets t
      LEFT JOIN pasteque.sessions ses ON t.sequence=ses.sequence AND t.cashregister_id=ses.cashregister_id
      INNER JOIN pasteque.ticketpayments p ON p.ticket_id=t.id 
      INNER JOIN pasteque.paymentmodes pm ON pm.id=p.paymentmode_id
      GROUP BY ldate,pm.id
      ) t ON t.ldate::date=ts.date AND (ppm.attributes->>'pasteque_id')::int4=t.pmethod
    
    LEFT JOIN (
        SELECT SUM(sp.amount) as total,
        timezone('Europe/Paris'::text, timezone('utc'::text, ses.closedate))::date AS ldate,
        sp.paymentmode_id as pmethod
        FROM pasteque.sessionpayments sp
        LEFT JOIN pasteque.sessions ses ON ses.id=sp.cashsession_id
        GROUP BY ldate, pmethod
      ) ses ON ses.ldate=ts.date AND (ppm.attributes->>'pasteque_id')::int4=ses.pmethod

    LEFT JOIN (
        SELECT sje.id,sje.date,sje.payment_method_id,sje.amount
        FROM sales_journal sj
        LEFT JOIN sales_journal_entry sje ON sje.sales_journal_id=sj.id
        WHERE sj.code=:code
        GROUP BY sje.id
      ) sje ON (sje.date=ts.date AND sje.payment_method_id=ppm.id)

    GROUP BY CUBE(ts.date, ppm.name)
    ORDER BY ts.date DESC, ppm.name;
    `, {
      inventory: args.inventory_id,
      code: this.code!
    });

    return result.rows;
  }
}


export default {
  async salesJournal(args: gt.QuerySalesJournalArgs, context: GraphQLContext, info: GraphQLResolveInfo) {
    context.requireGroups(['accounting']);
    const journal = await knex('sales_journal')
      .select(['id', 'code', 'name', 'description'])
      .where({ 'code': args.code })
      .first()

    return new SalesJournalResolver(journal);
  },
  async updateSalesJournalEntries(args: gt.MutationUpdateSalesJournalEntriesArgs, context: GraphQLContext, info: GraphQLResolveInfo) {
    context.requireGroups(['accounting', 'rw']);
    await knex('sales_journal_entry')
      .insert(args.entries.map(e => ({
        amount: e.amount,
        date: e.date,
        title: 'Confirmed payment',
        payment_method_id: knex('payment_method').select('id').where('name', e.method),
        sales_journal_id: knex('sales_journal').select('id').where('code', args.sales_journal_code)
      })))
      .onConflict(['sales_journal_id', 'payment_method_id', 'date'])
      .merge(['amount', 'title']);
    return true;
  }
}
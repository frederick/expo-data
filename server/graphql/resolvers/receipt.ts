
import gt from "../../types/graphql";
import knex from '../../db/knex';

import { GraphQLResolveInfo } from "graphql";
import Context from './loaders'
import moment from "moment";
import _ from "lodash";
import GraphQLContext from "../GraphQLContext";

class ReceiptResolver {
	id?: number;

	constructor(r) {
		Object.assign(this, r);
	}
}

const customerfields = ["firstname", "lastname", "email", "phone1", "phone2", "fax", "address", "zipcode", "city", "region", "country", "note"];
const receiptfields = ["ticket_id", "note", "number", "date", "ticket", "payment_details"];
export default {
	async receipt(args: gt.QueryReceiptArgs, context: GraphQLContext, info: GraphQLResolveInfo) {
		context.requireGroups(['receipts']);
		const receipt = await knex('receipt')
			.select('r.*', knex.raw('row_to_json(c.*) as customer'), knex.raw("r.ticket->>'finaltaxedprice' as total"))
			.from({ r: 'receipt' })
			.leftJoin('customer as c', 'c.id', 'r.customer_id')
			.where({ 'r.id': args.id })
			.first()
		return receipt;
	},
	async receipts(args, context: GraphQLContext, info: GraphQLResolveInfo) {
		context.requireGroups(['receipts']);
		const receipts = await knex('receipt')
			.select('r.*', knex.raw('row_to_json(c.*) as customer'), knex.raw("r.ticket->>'finaltaxedprice' as total"))
			.from({ r: 'receipt' })
			.leftJoin('customer as c', 'c.id', 'r.customer_id')
			.orderBy('r.date', 'desc')
		return receipts;
	},
	async createReceipt({ receipt }: gt.MutationCreateReceiptArgs, context: GraphQLContext, info) {
		context.requireGroups(['receipts','rw']);
		const tx = context.knexTransaction;
		const [customer_id] = await knex('customer').insert({
			..._.pick(receipt.customer, customerfields),
			version: 1
		})
			.returning('id')
			.transacting(tx);
		const [id] = await knex('receipt').insert({
			..._.pick(receipt, receiptfields),
			customer_id,
			version: 1
		})
			.returning('id')
			.transacting(tx);
		return id;
	},
	async updateReceipt(args: gt.MutationUpdateReceiptArgs, context: GraphQLContext, info) {
		context.requireGroups(['receipts', 'rw']);
		const tx = context.knexTransaction;
		await knex('customer').update({
			..._.pick(args.receipt.customer, customerfields),
		})
			.increment('version')
			.where({
				id: args.receipt.customer.id!
			})
			.andWhere(knex.raw(`id = (SELECT customer_id FROM receipt WHERE id=?)`, args.id!))
			.returning('id')
			.transacting(tx);
		const [id] = await knex('receipt').update({
			..._.pick(args.receipt, receiptfields),
		})
			.increment('version')
			.returning('id')
			.where({
				id: args.receipt.id
			})
			.transacting(tx);
		return id;
	},
	async deleteReceipt(args: gt.MutationDeleteReceiptArgs, context: GraphQLContext, info) {
		context.requireGroups(['receipts', 'rw']);
		const tx = context.knexTransaction;


		const [customer_id] = await knex('receipt')
			.delete()
			.where({ id: args.id })
			.returning('customer_id')
			.transacting(tx);

		await knex('customer')
			.delete()
			.where({ id: customer_id })
			.transacting(tx);

		return args.id;
	}

}
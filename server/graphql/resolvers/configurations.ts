
import gt from "../../types/graphql";

import { GraphQLResolveInfo } from "graphql";
import GraphQLContext from "../GraphQLContext";
import knex from '../../db/knex';


/**
 * misc configuration resolvers
 */
export default {
    async paymentMethods(args, context: GraphQLContext, info: GraphQLResolveInfo) {
        const pms = await knex('payment_method')
            .select(['id', 'name', 'code', 'display_name', 'pasteque_id', 'pasteque_reference', 'barcode', 'polimod'])
            .orderBy('code');
        return pms;
    },

    async configurationSettings(args: gt.QueryConfigurationSettingsArgs) {
        let query = knex('configuration_setting')
        .select('key', 'value')
        if(args.keys) {
            query = query.whereIn('key', args.keys)
        }
        query.orderBy('key');

        return await query;
    },

    webshopOrdersUrl(args, context: GraphQLContext) {
        context.requireGroups('webshop');
        return process.env.WEBSHOP_URL_ORDERS
    }
}
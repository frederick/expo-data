import DataLoader from "dataloader";
import gt from "../../../types/graphql";
import knex from '../../../db/knex';
import Context from ".";
import ExtendedDataLoader from "./ExtendedDataLoader";

export const productFields = ['p.version', 'p.number', 'p.category_code', 'p.criteria', 'p.name', 'p.supplier', 'p.barcode', 'p.price', 'p.import_date', 'p.label', 'p.active', 'p.id'];

export async function loadAttributes(product_ids?: readonly Number[]): Promise<any[]> {
	let query = knex('product')
		.select('p.id')
		.select('p.attributes')
		;
	if (product_ids) {
		query.whereIn('p.id', product_ids).from({ p: 'product' });
		let result = await query;
		return product_ids.map(pid => {
			let attrs = result.find(pa => pa.id == pid)?.attributes;
			if (attrs) {
				attrs = Object.fromEntries(Object.keys(attrs).sort().map(k => [k, attrs[k]]))
			}
			return attrs;
		});
	}else{
		return await query.from({ p: 'product' });
	}
}

export default function (context: Context): ExtendedDataLoader<Number, any> {
	let dl = new ExtendedDataLoader<Number, any>({
		batchLoadFn: loadAttributes,
		loadAllFn: loadAttributes
	});
	return dl;
}
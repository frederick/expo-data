import DataLoader from "dataloader";
import gt, { ProductVariant } from "../../../types/graphql";
import knex from '../../../db/knex';
import Context from ".";
import ExtendedDataLoader from "./ExtendedDataLoader";
import _ from "lodash";

export async function loadVariants(filter): Promise<gt.ProductVariant[]> {
	let query = knex('vw_product_variants')
		.select(['product_id', 'variant_id as id', 'name', 'variant_code as code', 'attribute_values as values', 'sku'])
		.whereNotNull('variant_id');
	query = filter(query);
	let variants = await query;
	variants ??= [];
	variants.forEach(v => {
		v.values = (v.values ?? []).map(vv => {
			let attribute = {
				name: vv.attribute_name,
				id: vv.attribute_id,
				code: vv.attribue_code,
				type: vv.attribute_type,
			};

			let value = {
				id: vv.value_id,
				code: vv.value_code,
				color: vv.color,
				value: vv.value,
				attribute
			}
			return { id: vv.id, attribute, value }
		})
	});
	return variants;
}

export async function loadVariantsByProduct(product_ids: readonly Number[]): Promise<gt.ProductVariant[]> {
	let variants = await loadVariants((knex) => knex.whereIn('product_id', product_ids));

	let variantsByProduct = _.groupBy(variants, 'product_id') as unknown as { [product_id: string]: ProductVariant };
	return product_ids.map(pid => variantsByProduct[pid.toString()] ?? []);
}

export async function loadVariantsById(ids: readonly Number[]): Promise<gt.ProductVariant[]> {
	let variants = await loadVariants((knex) => knex.whereIn('variant_id', ids));
	return ids.map(id => variants.find(v => v.id === id)!);
}


export default function (context: Context): ExtendedDataLoader<Number, any> {
	let dl = new ExtendedDataLoader<Number, any>({
		batchLoadFn: loadVariantsByProduct
	});
	return dl;
}

export function idLoader(context: Context): ExtendedDataLoader<Number, any> {
	let dl = new ExtendedDataLoader<Number, any>({
		batchLoadFn: loadVariantsById
	});
	return dl;
}
import GraphQLContext from "../../../GraphQLContext";

export default function(si_variant) {
  si_variant.variant = async function(args, context: GraphQLContext) {
		return await context.loaderContext.variantLoader.load(this.variant_id);
	}

  return si_variant;
}
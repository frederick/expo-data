import GraphQLContext from "../../../GraphQLContext";

export default function decorate(adjustment) {
  adjustment.variant = function (args, context: GraphQLContext) {
    if (!this.variant_id) return null
    else return context.loaderContext.variantLoader.load(this.variant_id);
  }

  adjustment.product = function (args, context: GraphQLContext) {
		if (!this.product_id) return null
		else return context.loaderContext.productLoader.load(this.product_id!);
	}

  adjustment.stock = function(args, context: GraphQLContext, info) {
		return context.loaderContext.stockLoader.load(this.stock_id! );
	}

  return adjustment
}
import GraphQLContext from "../../../GraphQLContext";
import knex from '../../../../db/knex';
import vdecorate from "./stock_item_variant";
import adecorate from "./stock_item_adjustment";
import Context from "..";

export default function decorate(c) {
  c.product = (args, context: GraphQLContext) => context.loaderContext.productLoader.load(c.product_id)
  c.adjustments = async function (args, context: GraphQLContext) {
    const sas = await knex('stock_adjustment')
      .pluck('stock_adjustment.id')
      .leftJoin('stock_operation', 'stock_operation.id','stock_adjustment.operation_id')
      .where('stock_item_id', this.id)
      .orderBy('stock_operation.date', 'desc')
      .orderBy('stock_adjustment.variant_id', 'asc');
    return context.loaderContext.stockAdjustmentLoader.loadMany(sas);
  }
  c.variants = c.variants.map(v => vdecorate(v))

  return c
}
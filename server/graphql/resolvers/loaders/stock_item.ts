import DataLoader from "dataloader";
import gt, { Stock } from "../../../types/graphql";
import knex from '../../../db/knex';
import Context from ".";
import ExtendedDataLoader from "./ExtendedDataLoader";
import decorate  from "./decorators/stock_item";

export const stock_fields = ["name", "description"];

function err(id): any {
  throw new Error(`The stock item with id ${id} could not be found`)
}


function itemQuery() {
  let query = knex('stock_item').select(
    ['i.id', 'i.quantity', 'i.notes', 'i.product_id', knex.count('v.id').as('variant_count'),
      knex.raw(`COALESCE(json_agg(json_build_object('id',v.id,'variant_id',pv.variant_id,'quantity',COALESCE(v.quantity,0),'notes',v.notes) ORDER BY pv.position) FILTER (WHERE pv.variant_id IS NOT NULL), '[]') as variants`)]
  )
    .from({ i: 'stock_item' })
    .leftJoin({ p: 'product' }, 'p.id', 'i.product_id')
    .leftJoin({ pv: 'vw_product_variants' }, 'pv.product_id', 'i.product_id')
    .leftJoin({ v: 'stock_item_variant' }, (b) =>
      b.on('v.stock_item_id', 'i.id')
        .andOn('v.variant_id', 'pv.variant_id')
    )
    .groupBy<gt.StockItem[]>(['i.id', 'p.number']);
  return query;
}


export async function loadStockItems(ids?: readonly number[]): Promise<(gt.StockItem)[]> {
  let query = itemQuery();
  if (ids) {
    query.whereIn('i.id', ids);
    let stockItems = await query;
    return ids.map(id => stockItems.find(s => s.id == id) ?? err(id));
  } else {
    let stockItems = await query.orderBy('p.number');
    return stockItems;
  }

}

interface StockItemDataLoader extends ExtendedDataLoader<number, gt.StockItem> {
}

export default function (context: Context): StockItemDataLoader {
  let dl = new ExtendedDataLoader<number, gt.StockItem>({
    batchLoadFn: loadStockItems,
    loadAllFn: loadStockItems,
    decorateFn: decorate
  });
  return dl;

}
import DataLoader from "dataloader";
import gt, { Stock } from "../../../types/graphql";
import knex from '../../../db/knex';
import Context from ".";
import ExtendedDataLoader from "./ExtendedDataLoader";
import GraphQLContext from "../../GraphQLContext";

export const stock_fields = ["name", "description"];

function err(id): any {
  throw new Error(`The stock adjustment with id ${id} could not be found`)
}
//

//.orderBy('date', 'desc')
//.orderBy('variant_id', 'asc');

export async function loadAdjustments(ids: readonly number[]): Promise<(gt.StockAdjustment)[]> {
  let query = knex('stock_adjustment')
      .select(['id', 'operation_id', 'stock_item_id', 'reference', 'product_id', 'variant_id', 'stock_item_variant_id', 'adjustment', 'old_quantity', 'new_quantity', 'comment'])
      .whereIn('id', ids)

  let adjustments = await query;
  return ids.map(id => adjustments.find(s => s.id == id) ?? err(id));
}


interface StockAdjustmentLoader extends ExtendedDataLoader<number, gt.StockAdjustment> {
}

export default function (context: Context): StockAdjustmentLoader {
  let dl = new ExtendedDataLoader<number, gt.StockAdjustment>({
    batchLoadFn: loadAdjustments,
    decorateFn: adjustment => {
      adjustment.variant = function (args, context: GraphQLContext) {
        if (!this.variant_id) return null
        else return context.loaderContext.variantLoader.load(this.variant_id);
      }
    
      adjustment.product = function (args, context: GraphQLContext) {
        if (!this.product_id) return null
        else return context.loaderContext.productLoader.load(this.product_id!);
      }
    
      adjustment.operation = function(args, context: GraphQLContext, info) {
        return context.loaderContext.stockOperationLoader.load(this.operation_id! );
      }
      return adjustment;
    }
  });
  return dl;

}
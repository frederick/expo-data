import DataLoader from "dataloader";
import gt, { Stock } from "../../../types/graphql";
import knex from '../../../db/knex';
import Context from ".";
import ExtendedDataLoader from "./ExtendedDataLoader";
import GraphQLContext from "../../GraphQLContext";

export const stock_fields = ["name", "description"];

function err(id): any {
  throw new Error(`The stock operation with id ${id} could not be found`)
}


export async function loadOperations(ids: readonly number[]): Promise<(gt.StockOperation)[]> {
  let query = knex('stock_operation')
    .select(['id','stock_id', 'type', 'name', 'date', 'comment', 'reference', 'created_by'])
    .whereIn('id' , ids);

  let operations = await query;
  return ids.map(id => operations.find(s => s.id == id) ?? err(id));
}

export async function loadAdjustments(operation_id) {
  let ids = await knex('stock_adjustment').pluck('id')
          .where('operation_id',operation_id)
          .orderBy('product_id', 'variant_id');

  return ids;
}

interface StockOperationLoader extends ExtendedDataLoader<number, gt.StockOperation> {
}

export default function (context: Context): StockOperationLoader {
  let dl = new ExtendedDataLoader<number, gt.StockOperation>({
    batchLoadFn: loadOperations,
    decorateFn: c => {
      c.stock = (args, context: GraphQLContext) => context.loaderContext.stockLoader.load(c.stock_id)
      c.adjustments = async function(args,context: GraphQLContext) {
        return context.loaderContext.stockAdjustmentLoader.loadMany(await loadAdjustments(c.id));
      }
      return c
    }
  });
  return dl;

}
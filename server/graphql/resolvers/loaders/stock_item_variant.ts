import DataLoader from "dataloader";
import gt, { Stock } from "../../../types/graphql";
import knex from '../../../db/knex';
import Context from ".";
import ExtendedDataLoader from "./ExtendedDataLoader";
import decorate from "./decorators/stock_item_variant";

export const stock_fields = ["name", "description"];

function err(id): any {
  throw new Error(`The stock item variant with id ${id} could not be found`)
}


export async function loadStockItemVariants(ids: readonly number[]): Promise<(gt.StockItemVariant)[]> {
  let query = knex('stock_item_variant')
    .select(['id', 'stock_item_id', 'variant_id', 'quantity', 'notes'])

  query.whereIn('id', ids);
  let variants = await query;
  return ids.map(id => variants.find(s => s.id == id) ?? err(id));
}

interface StockItemVariantDataLoader extends ExtendedDataLoader<number, gt.StockItemVariant> {
}

export default function (context: Context): StockItemVariantDataLoader {
  let dl = new ExtendedDataLoader<number, gt.StockItemVariant>({
    batchLoadFn: loadStockItemVariants,
    decorateFn: decorate
  });
  return dl;

}
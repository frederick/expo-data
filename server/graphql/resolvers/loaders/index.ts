import gt from "../../../types/graphql";
import ExtendedDataLoader from "./ExtendedDataLoader";
import productLoader from './product';
import pastequeProductLoader from './pasteque_product';
import productCustomAttributesLoader from './product_custom_attributes';
import stockLoader from './stock';
import stockItemLoader from './stock_item';
import stockItemVariantLoader from './stock_item_variant';
import stockOperationLoader from './stock_operation';
import stockAdjustmentLoader from './stock_adjustment';
import productVariantsLoader, { idLoader as variantLoader } from './product_variant';
import categoryLoader from './category';

export default class Context {

  loaders = new Map<string, any>()


  constructor() {
  }

  getLoader(name: string, factory: (Context) => ExtendedDataLoader<any,any>) {
    let l = this.loaders.get(name)
    if (l) {
      return l;
    } else {
      return this.loaders.set(name, factory(this)).get(name)
    }
  }

  get productLoader(): ExtendedDataLoader<Number, gt.Product> {
    return this.getLoader('product', productLoader);
  }

  get pastequeProductLoader(): ExtendedDataLoader<Number, gt.PastequeProduct> {
    return this.getLoader('pastequeProductLoader', pastequeProductLoader);

  }

  get productCustomAttributesLoader(): ExtendedDataLoader<Number, any> {
    return this.getLoader('productCustomAttributesLoader', productCustomAttributesLoader);
  }

  get productVariantsLoader(): ExtendedDataLoader<Number, any> {
    return this.getLoader('productVariantsLoader', productVariantsLoader);
  }

  get variantLoader(): ExtendedDataLoader<Number, any> {
    return this.getLoader('variantLoader', variantLoader);
  }

  get categoryLoader(): ExtendedDataLoader<Number, gt.Category> {
    return this.getLoader('categoryLoader', categoryLoader);
  }

  get stockLoader(): ExtendedDataLoader<Number, gt.Stock> {
    return this.getLoader('stockLoader', stockLoader);
  }

  get stockItemLoader(): ExtendedDataLoader<Number, gt.Stock> {
    return this.getLoader('stockItem', stockItemLoader);
  }

  get stockItemVariantLoader(): ExtendedDataLoader<Number, gt.StockItemVariant> {
    return this.getLoader('stockItemVariant', stockItemVariantLoader);
  }

  get stockOperationLoader(): ExtendedDataLoader<Number, gt.StockItemVariant> {
    return this.getLoader('stockOperation', stockOperationLoader);
  }

  get stockAdjustmentLoader(): ExtendedDataLoader<Number, gt.StockItemVariant> {
    return this.getLoader('stockAdjustment', stockAdjustmentLoader);
  }
}

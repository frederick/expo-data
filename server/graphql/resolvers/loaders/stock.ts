import DataLoader from "dataloader";
import gt from "../../../types/graphql";
import knex from '../../../db/knex';
import Context from ".";
import ExtendedDataLoader from "./ExtendedDataLoader";

export const stock_fields = ["name", "description"];

function err(id): any {
  throw new Error(`The stock with id ${id} could not be found`)
}

export async function loadStocks(ids?: readonly number[]): Promise<(gt.Stock)[]> {
  let query = knex<gt.Stock>('stock')
    .select('stock.id', 'stock.version', ...stock_fields.map(sf => `stock.${sf}`))
    .from('stock')
    .count('stock_item.id', { as: 'product_count' })
    .leftJoin('stock_item', 'stock_item.stock_id', 'stock.id')
    .groupBy<gt.Stock[]>('stock.id')

  if (ids) {
    query.whereIn('stock.id', ids);
    let stocks = await query;
    return ids.map(id => stocks.find(s => s.id == id) ?? err(id));
  } else {
    let stocks = await query.orderBy('name');
    return stocks;
  }

}



export default function (context: Context): ExtendedDataLoader<number, gt.Stock> {
  let dl = new ExtendedDataLoader<number, gt.Stock>({
    batchLoadFn: loadStocks,
    loadAllFn: loadStocks,
    decorateFn: function (c) {
      c.items = async () => {
        let ids = await knex('stock_item').select('stock_item.id')
          .where({ stock_id: c.id })
          .leftJoin({ p: 'product' }, 'p.id', 'stock_item.product_id')
          .orderBy('p.number').pluck('stock_item.id');
        return context.stockItemLoader.loadMany(ids);
      }
      return c;
    }
  });
  return dl;

}
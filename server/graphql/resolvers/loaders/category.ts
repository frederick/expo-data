import DataLoader from "dataloader";
import gt from "../../../types/graphql";
import knex from '../../../db/knex';
import Context from ".";
import ExtendedDataLoader from "./ExtendedDataLoader";


export async function loadCategories(category_codes?: readonly Number[]): Promise<gt.Category[]> {
  let query = knex('product_category')
    .select('c.code', 'c.version', 'c.name', 'c.rate', 'c.comment', 'c.pasteque_tax')

  query.from<gt.Category>({ c: 'product_category' })
  if (category_codes) {
    query.whereIn('code', category_codes);
    let cats = await query;
    return category_codes.map(k => cats.find(c => c.code == k));
  } else {
    let cats = await query;
    return cats;
  }

}



export default function (context: Context): ExtendedDataLoader<Number, gt.Category> {
  let dl = new ExtendedDataLoader<Number, gt.Category>({
    batchLoadFn: loadCategories,
    loadAllFn: loadCategories,
    decorateFn: function (c) {
      return c;
    }
  });
  return dl;

}
import DataLoader from "dataloader";
import gt from "../../../types/graphql";
import knex from '../../../db/knex';
import Context from ".";
import ExtendedDataLoader from "./ExtendedDataLoader";

export const productFields = ['p.version', 'p.number', 'p.category_code', 'p.criteria', 'p.name', 'p.supplier', 'p.barcode', 'p.price', 'p.import_date', 'p.label', 'p.active', 'p.id', 'p.sku'];

export async function loadProducts(product_ids?: readonly Number[]): Promise<gt.Product[]> {
	let query = knex('product')
		.leftJoin('product_variant as pv', 'pv.product_id', 'p.id')
		.select(productFields)
		.select(knex.raw('EXISTS(SELECT pp.id FROM pasteque.products pp WHERE pp.id=p.number) as has_pasteque_product'))
		.select(knex.raw('count(pv.id)::int4 as variant_count'));
	if (product_ids) {
		query.whereIn('p.id', product_ids)
	}

	let products = await query.from<gt.Product>({ p: 'product' })
		.orderBy('p.number')
		.groupBy('p.id')
		.limit(5000);

	return products;
}



export default function (context: Context): ExtendedDataLoader<Number, gt.Product> {
	let dl = new ExtendedDataLoader<Number, gt.Product>({
		batchLoadFn: async function (product_ids: readonly Number[]): Promise<gt.Product[]> {
			let products = await loadProducts(product_ids);
			let productsById = Object.fromEntries(products.map(p => [p.id, p]));
			return product_ids.map(pid => {
				return productsById[pid.toString()];
			});
		},
		loadAllFn: loadProducts,
		decorateFn: function (p) {
			if (!p) return p;
			p.pasteque = () => context.pastequeProductLoader.load(p.id)
			p.attributes = () => context.productCustomAttributesLoader.load(p.id)
			p.variants = () => context.productVariantsLoader.load(p.id)
			p.category = () => context.categoryLoader.load(p.category_code)
			return p;
		}
	});
	return dl;

}
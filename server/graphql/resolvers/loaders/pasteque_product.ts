import DataLoader from "dataloader";
import gt from "../../../types/graphql";
import knex from '../../../db/knex';
import Context from ".";
import ExtendedDataLoader from "./ExtendedDataLoader";

export const productFields = ['p.version', 'p.number', 'p.category_code', 'p.criteria', 'p.name', 'p.supplier', 'p.barcode', 'p.price', 'p.import_date', 'p.label', 'p.active', 'p.id'];

function buildBaseQuery() {
	return knex('products')
		.select(['op.id as product_id',
			'p.id', 'p.reference', 'p.label', 'p.barcode',
			'p.pricesell', 'p.visible',
			knex.ref('c.reference').as('category'),
			knex.ref('t.rate').as('tax_rate'),
			knex.ref('t.label').as('tax'),
			knex.raw('p.pricesell * (1+t.rate) as price')
		])
		.from<gt.Product>({ op: 'product' })
		.rightJoin('pasteque.products as p', 'p.id', '=', 'op.number')
		.leftJoin('pasteque.categories as c', 'c.id', '=', 'p.category_id')
		.leftJoin('pasteque.taxes as t', 't.id', '=', 'p.tax_id')
}
export async function loadPastequeProducts(product_ids: readonly Number[]): Promise<gt.PastequeProduct[]> {
	const pps = await buildBaseQuery()
		.whereIn('p.id', product_ids);
	return product_ids.map(id => pps.find(pp => pp.product_id === id));
}

export async function loadPastequeProductsByProductId(product_ids: readonly Number[]): Promise<gt.PastequeProduct[]> {
	const pps = await buildBaseQuery()
		.whereIn('op.id', product_ids);
	return product_ids.map(id => pps.find(pp => pp.product_id === id));
}

export default function (context: Context): ExtendedDataLoader<Number, gt.PastequeProduct> {
	let dl = new ExtendedDataLoader<Number, gt.PastequeProduct>({
		batchLoadFn: loadPastequeProductsByProductId
	});
	
	return dl;

}


import DataLoader from "dataloader";
import type Context from ".";

interface ExtendedDataLoaderArgs<K, V, C = K> extends DataLoader.Options<K, V, C> {
  batchLoadFn: DataLoader.BatchLoadFn<K, V>
  loadAllFn?: () => Promise<V[]>
  decorateFn?: (V) => V
  idField?: string | undefined
}
/**
 * Dataloader that supports load all and decorators
 */
export default class ExtendedDataLoader<K, V, C = K> extends DataLoader<K, V, C> {

  loadAllFn: () => Promise<V[]>;
  idField: string;
  decorateFn?: (V) => V;
  wrappedBatchLoadFn: DataLoader.BatchLoadFn<K, V>

  constructor(args: ExtendedDataLoaderArgs<K, V, C>) {
    super((args) => this.loadAndDecorate(args), args);
    this.loadAllFn = args.loadAllFn ?? (() => { throw new Error('not implemented') });
    this.idField = args.idField ?? 'id';
    this.wrappedBatchLoadFn = args.batchLoadFn;
    this.decorateFn = args.decorateFn;
  }

  async loadAndDecorate(keys: ReadonlyArray<K>): Promise<ArrayLike<V | Error>> {
    const values = await this.wrappedBatchLoadFn(keys) as V[];
    if(this.decorateFn !== undefined) {
      return (values ?? []).map(v => this.decorateFn!(v));
    } else {
      return values
    }
  }

  async loadAll(): Promise<Array<V | Error>> {
    const vs = await this.loadAllFn();
    for (let v of vs) {
      this.prime(v[this.idField] as K, this.decorateFn ? this.decorateFn(v): v);
    }
    return vs;
  }
  
}
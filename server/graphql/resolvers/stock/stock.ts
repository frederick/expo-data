
import gt from "../../../types/graphql";
import knex from '../../../db/knex';

import { GraphQLResolveInfo } from "graphql";
import { stock_fields } from "../loaders/stock";

import _, { size } from "lodash";
import GraphQLContext from "../../GraphQLContext";
import moment from "moment";
import decorate_adjustment from "../loaders/decorators/stock_item_adjustment"


export default {
	async stock(args: gt.QueryStockArgs, context: GraphQLContext, info: GraphQLResolveInfo) {
		context.requireGroups(['inventory']);
		const stock = await context.loaderContext.stockLoader.load(args.id);

		return (stock)
	},
	async stocks(args, context: GraphQLContext, info: GraphQLResolveInfo) {
		context.requireGroups(['inventory']);

		const stocks = await context.loaderContext.stockLoader.loadAll();
		return stocks;
	},
	async createStock({ stock }: gt.MutationCreateStockArgs, context: GraphQLContext, info) {
		context.requireGroups(['rw', 'inventory']);
		const tx = context.knexTransaction;

		const [id] = await knex('stock').insert({
			..._.pick(stock, stock_fields),

			version: 1
		})
			.returning('id')
			.transacting(tx);
		return id;
	},
	async updateStock(args: gt.MutationUpdateStockArgs, context: GraphQLContext, info) {
		context.requireGroups(['rw', 'inventory']);
		const tx = context.knexTransaction;

		const [id] = await knex('stock').update({
			..._.pick(args.stock, stock_fields),
		})
			.increment('version')
			.returning('id')
			.where({
				id: args.id
			})
			.transacting(tx);
		return id;
	},
	async deleteStock(args: gt.MutationDeleteStockArgs, context: GraphQLContext, info) {
		context.requireGroups(['rw', 'inventory']);
		const tx = context.knexTransaction;
		await knex('stock')
			.delete()
			.where({ id: args.id })
			.transacting(tx);

		return args.id;
	},

	async stockItem(args: gt.QueryStockItemArgs, context: GraphQLContext) {
		context.requireGroups(['inventory']);
		const item = await context.loaderContext.stockItemLoader.load(args.id);
		return item;
	},
	async updateStockItemQuantities(args: gt.MutationUpdateStockItemQuantitiesArgs, context: GraphQLContext) {
		context.requireGroups(['rw', 'inventory']);
		let comment: string | null = args.adjustmentComment ?? '';
		if (comment.trim() === '') {
			comment = null;
		}
		let changed = false;
		const si = await knex('stock_item')
			.transacting(context.knexTransaction)
			.forUpdate()
			.select(['id', 'quantity', 'product_id', 'stock_id'])
			.where({
				id: args.stock_item_id
			})
			.first();
		let siq = args.stock_item_quantities.find(siqx => siqx.variant_id === null);
		let sivq = args.stock_item_quantities.filter(siqx => siqx.variant_id !== null);
		sivq = _.uniqBy(sivq, siv => siv.variant_id);
		let [operation_id] = await knex('stock_operation')
			.insert({
				type: 'correction',
				date: context.date,
				comment,
				created_by: context.token.sub,
				stock_id: si.stock_id
			}).returning('id').transacting(context.knexTransaction);
		if (siq) {
			await knex('stock_item')
				.transacting(context.knexTransaction)
				.where({
					id: args.stock_item_id
				})
				.update({
					quantity: siq.quantity,
				})

			if (si.quantity !== siq.quantity) {
				changed = true;
				await knex('stock_adjustment')
					.transacting(context.knexTransaction)
					.insert({
						product_id: si.product_id,
						stock_item_id: si.id,
						adjustment: siq.quantity - si.quantity,
						old_quantity: si.quantity,
						new_quantity: siq.quantity,
						operation_id
					})
			}

			if (sivq.length) {
				let sivs = await knex('stock_item_variant')
					.transacting(context.knexTransaction)
					.forUpdate()
					.select(['id', 'quantity', 'variant_id'])
					.where('stock_item_id', args.stock_item_id)
					.whereIn('variant_id', sivq.map(sivqx => sivqx.variant_id!));

				let sivqw = sivq.map(sivqx => ({
					...sivqx,
					oldQuantity: sivs.find(sivsx => sivsx.variant_id === sivqx.variant_id)?.quantity ?? 0
				})).map(sivqx => ({
					...sivqx,
					diffQuantity: sivqx.quantity - sivqx.oldQuantity,
					id: null
				}));

				const updatedVariants = await knex('stock_item_variant')
					.transacting(context.knexTransaction)
					.insert(sivqw.map(siv => ({
						quantity: siv.quantity,
						variant_id: siv.variant_id,
						stock_item_id: args.stock_item_id
					})))
					.onConflict(['variant_id', 'stock_item_id']).merge(['quantity'])
					.returning(['id', 'variant_id']);
				for (let i of updatedVariants) {
					let sivqwx = sivqw.find(siqwy => siqwy.variant_id === i.variant_id);
					sivqwx!.id = i.id;
				}
				let diffs = sivqw.filter(sivqx => sivqx.id !== null && sivqx.diffQuantity !== 0);
				if (diffs.length) {
					changed = true;
					await knex('stock_adjustment')
						.transacting(context.knexTransaction)
						.insert(diffs.map(d => ({
							product_id: si.product_id,
							stock_item_id: si.id,
							stock_item_variant_id: d.id,
							variant_id: d.variant_id,
							adjustment: d.diffQuantity,
							old_quantity: d.oldQuantity,
							new_quantity: d.quantity,
							operation_id
						})));
				}
			}

			if (!changed) {
				throw new Error("No changes detected. Aborting.");
			}

			return true;
		}
	},
	async updateStockItem(args: gt.MutationUpdateStockItemArgs, context: GraphQLContext) {
		context.requireGroups(['rw', 'inventory']);

		await knex('stock_item')
			.transacting(context.knexTransaction)
			.where({
				id: args.id
			})
			.update({
				notes: args.item.notes,
			})
		if (args.item.variants.length) {
			await knex('stock_item_variant')
				.transacting(context.knexTransaction)
				.insert(args.item.variants.map(v => ({
					notes: v.notes,
					variant_id: v.variant_id,
					stock_item_id: args.id
				})))
				.onConflict(['variant_id', 'stock_item_id']).merge(['notes'])
		}

		return args.id;
	},

	async createStockItems(args: gt.MutationCreateStockItemsArgs, context: GraphQLContext) {
		context.requireGroups(['rw', 'inventory']);
		return await knex('stock_item')
			.transacting(context.knexTransaction)
			.insert(
				args.product_ids.map(p => ({
					stock_id: args.stock_id,
					product_id: p,
				}))
			)
			.onConflict(['stock_id', 'product_id']).ignore()
			.returning('id')

	},
	async removeStockItems(args: gt.MutationRemoveStockItemsArgs, context: GraphQLContext) {
		context.requireGroups(['rw', 'inventory']);
		await knex('stock_item')
			.transacting(context.knexTransaction)
			.delete()
			.whereIn('id', args.ids)
		return true;
	},

}

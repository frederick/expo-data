
import gt from "../../../types/graphql";
import knex from '../../../db/knex';

import { GraphQLResolveInfo } from "graphql";
import { stock_fields } from "../loaders/stock";

import _, { size } from "lodash";
import GraphQLContext from "../../GraphQLContext";
import moment from "moment";
import decorate_adjustment from "../loaders/decorators/stock_item_adjustment"


const stock_count_fields = ["name", "date", "inventory_id", "comment", "validated"];
const stock_count_item_fields = ["stock_item_id", "stock_count_id", "quantity", "comment", "stock_item_variant_id", 'stock_id'];




class StockCountResolver {
	id?: number
	stock_id?: number
	constructor(stockCount) {
		Object.assign(this, stockCount);
	}

	async items(args, context: GraphQLContext) {
		const query = knex('stock_item')
			.select(["si.id as stock_item_id", knex.raw("COALESCE(ci.stock_count_id, ?) as stock_count_id", [this.id!]), "ci.quantity", "ci.comment", "siv.id as stock_item_variant_id", 'si.stock_id', "ci.updated_at", "ci.id"])
			.from({ si: 'stock_item' })
			.joinRaw('left join ((select null as id,null as stock_item_id) union all (select id, stock_item_id from stock_item_variant)) siv ON siv.stock_item_id=si.id OR siv.id IS NULL')
			.leftJoin({ ci: 'stock_count_item' }, knex.raw('ci.stock_item_id=si.id AND (ci.stock_item_variant_id IS NOT DISTINCT FROM siv.id) AND ci.stock_count_id=?', [this.id!]))
			.leftJoin({ p: 'product' }, 'p.id', '=', 'si.product_id')
			.where({ 'si.stock_id': this.stock_id! })
			.orderBy(['p.number', 'siv.id']);

		const items = await query;

		return items.map(i => new StockCountItemResolver(i));
	}

	async stock(args, context: GraphQLContext, info) {
		return context.loaderContext.stockLoader.load(this.stock_id!);
	}
}

class StockCountItemResolver {
	id?: number;
	stock_item_id?: number;
	stock_item_variant_id?: number;

	constructor(stockCountItem) {
		Object.assign(this, stockCountItem);
	}

	async stock_item(args, context: GraphQLContext, info) {
		return context.loaderContext.stockItemLoader.load(this.stock_item_id!);
	}
	async stock_item_variant(args, context: GraphQLContext, info) {
		if (this.stock_item_variant_id) {
			return context.loaderContext.stockItemVariantLoader.load(this.stock_item_variant_id)
		} else {
			return null;
		}
	}
}

export default {
	async createStockCount(args: gt.MutationCreateStockCountArgs, context: GraphQLContext, info) {
		context.requireGroups(['rw', 'inventory']);
		const tx = context.knexTransaction;

		const [id] = await knex('stock_count').insert({
			..._.pick(args.count, stock_count_fields),
			stock_id: args.stock_id
		})
			.returning('id')
			.transacting(tx);
		return id;
	},
	async updateStockCount(args: gt.MutationUpdateStockCountArgs, context: GraphQLContext, info) {
		context.requireGroups(['rw', 'inventory']);
		const sc = await knex('stock_count').select(['id', 'transferred', 'name', 'date']).transacting(context.knexTransaction).where({ id: args.id }).forUpdate().first();
		if (sc.transferred) {
			throw new Error("Stock count has already been transferred on " + sc.transferred.toJSON())
		}

		const tx = context.knexTransaction;

		const [id] = await knex('stock_count').update({
			..._.pick(args.count, stock_count_fields),
		})
			.returning('id')
			.where({
				id: args.id
			})
			.transacting(tx);
		return id;
	},
	async deleteStockCount(args: gt.MutationDeleteStockCountArgs, context: GraphQLContext, info) {
		context.requireGroups(['rw', 'inventory']);
		const tx = context.knexTransaction;
		const sc = await knex('stock_count').select(['transferred']).where({ id: args.id }).forUpdate().first();
		if (sc.transferred) {
			throw new Error("Cannot delete this stock count, because it has already been transferred");
		}
		await knex('stock_count')
			.delete()
			.where({ id: args.id })
			.transacting(tx);

		return args.id;
	},
	async stockCounts(args: gt.QueryStockCountsArgs, context: GraphQLContext) {
		context.requireGroups(['inventory']);
		const sc = await knex('stock_count')
			.select([...stock_count_fields, "transferred", 'stock_id', 'id'])
			.where({
				stock_id: args.stock_id
			})
			.orderBy('date', 'desc');
		return sc.map(s => new StockCountResolver(s));
	},
	async stockCount(args: gt.QueryStockCountArgs, context: GraphQLContext) {
		context.requireGroups(['inventory']);

		const sc = await knex('stock_count')
			.select([...stock_count_fields, "transferred", 'stock_id', 'id'])
			.where({
				id: args.id
			})
			.first();

		return new StockCountResolver(sc);
	},
	async updateStockCountItems(args: gt.MutationUpdateStockCountItemsArgs, context: GraphQLContext) {
		context.requireGroups(['rw', 'inventory']);
		const sc = await knex('stock_count').select(['id', 'transferred', 'name', 'date']).transacting(context.knexTransaction).where({ id: args.stock_count_id }).forUpdate().first();
		if (sc.transferred) {
			throw new Error("Stock count has already been transferred on " + sc.transferred.toJSON())
		}

		const { stock_id } = await knex('stock_count').select('stock_id').first().where({ id: args.stock_count_id });

		await knex('stock_count_item')
			.transacting(context.knexTransaction)
			.insert(args.counts.map(c => ({
				comment: c.comment,
				stock_item_id: c.stock_item_id,
				stock_count_id: args.stock_count_id,
				quantity: c.quantity,
				stock_item_variant_id: c.stock_item_variant_id,
				updated_at: context.date,
				stock_id
			})))
			.onConflict(['stock_count_id', 'stock_item_id', knex.raw('coalesce(stock_item_variant_id, -1)') as unknown as string]).merge(['comment', 'quantity', 'updated_at', 'stock_id'])
		return true;
	},

	async transferStockCount(args: gt.MutationTransferStockCountArgs, context: GraphQLContext) {
		context.requireGroups(['rw', 'inventory']);
		let changed = false;
		const sc = await knex('stock_count').select(['id', 'stock_id', 'transferred', 'name', 'date']).transacting(context.knexTransaction).where({ id: args.stock_count_id }).forUpdate().first();
		if (sc.transferred) {
			throw new Error("Stock count has already been transferred on " + sc.transferred.toJSON())
		}

		const siqs = await knex('stock_item').transacting(context.knexTransaction)
			.select(['id', 'quantity'])
			.whereIn('id', knex('stock_count_item').select('stock_item_id').where({ stock_count_id: args.stock_count_id }).whereNull('stock_item_variant_id'))
			.forUpdate();
		const siqvs = await knex('stock_item_variant').transacting(context.knexTransaction)
			.select(['id', 'quantity'])
			.whereIn('id', knex('stock_count_item').select('stock_item_variant_id').where({ stock_count_id: args.stock_count_id }).andWhere(knex.raw('stock_item_id=stock_item_variant.stock_item_id')))
			.forUpdate();

		const { rows: updated_items } = await knex.raw(`
			UPDATE stock_item si SET quantity=(SELECT sc.quantity FROM stock_count_item sc WHERE sc.stock_count_id=:count_id AND sc.stock_item_id=si.id AND sc.stock_item_variant_id IS NULL)
			WHERE si.id IN(SELECT sc.stock_item_id FROM stock_count_item sc WHERE sc.stock_count_id=:count_id AND sc.quantity IS NOT NULL AND sc.stock_item_variant_id IS NULL)
			RETURNING si.id, si.quantity, si.stock_id, si.product_id
		`, { count_id: args.stock_count_id })
			.transacting(context.knexTransaction);
		let [operation_id] = await knex('stock_operation')
			.insert({
				type: 'count',
				date: context.date,
				comment: `${sc.name} - ${moment(sc.date).format('DD/MM/YYYY')}`,
				created_by: context.token.sub,
				stock_id: sc.stock_id,
				reference: `stock_count#${sc.id}`
			}).returning('id').transacting(context.knexTransaction);
		if (updated_items.length) {

			for (let u of updated_items) {
				let oldItem = siqs.find(siq => siq.id === u.id);
				u.oldQuantity = oldItem.quantity;
				u.diffQuantity = u.quantity - u.oldQuantity;
			}

			// we insert a stock adjustment even if the diff is 0 to make the count appear
			await knex('stock_adjustment')
				.transacting(context.knexTransaction)
				.insert(updated_items.map(d => ({
					product_id: d.product_id,
					stock_item_id: d.id,
					stock_item_variant_id: null,
					variant_id: null,
					adjustment: d.diffQuantity,
					old_quantity: d.oldQuantity,
					new_quantity: d.quantity,
					operation_id
				})));
		}
		const { rows: updated_item_variants } = await knex.raw(`
			UPDATE stock_item_variant siv SET quantity=(SELECT sc.quantity FROM stock_count_item sc WHERE sc.stock_count_id=:count_id AND sc.stock_item_id=siv.stock_item_id AND sc.stock_item_variant_id=siv.id)
			FROM stock_count_item sc
			LEFT JOIN stock_item si ON si.id=sc.stock_item_id
			WHERE sc.stock_count_id=:count_id AND siv.id=sc.stock_item_variant_id
			RETURNING siv.id, siv.quantity, si.stock_id, siv.variant_id, si.product_id, siv.stock_item_id
		`, { count_id: args.stock_count_id })
			.transacting(context.knexTransaction);
		if (updated_item_variants.length) {
			for (let u of updated_item_variants) {
				let oldItem = siqvs.find(siqv => siqv.id === u.id);
				u.oldQuantity = oldItem.quantity;
				u.diffQuantity = u.quantity - u.oldQuantity;
			}

			// we insert a stock adjustment even if the diff is 0 to make the count appear
			await knex('stock_adjustment')
				.transacting(context.knexTransaction)
				.insert(updated_item_variants.map(d => ({
					product_id: d.product_id,
					stock_item_id: d.stock_item_id,
					stock_item_variant_id: d.id,
					variant_id: d.variant_id,
					adjustment: d.diffQuantity,
					old_quantity: d.oldQuantity,
					new_quantity: d.quantity,
					operation_id
				})));
		}

		await knex('stock_count').update({
			transferred: new Date(),
			operation_id
		}).where({ id: args.stock_count_id }).transacting(context.knexTransaction)
		return true;
	},

}
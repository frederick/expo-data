
import gt from "../../../types/graphql";
import knex from '../../../db/knex';

import { GraphQLResolveInfo } from "graphql";
import { stock_fields } from "../loaders/stock";

import _, { size } from "lodash";
import GraphQLContext from "../../GraphQLContext";
import moment from "moment";
import decorate_adjustment from "../loaders/decorators/stock_item_adjustment"

const stock_movement_fields = ["name", "date", "comment", "destination_stock_id"];
const stock_movement_item_fields = ["stock_item_id", "stock_movement_id", "quantity", "comment", "stock_item_variant_id", 'stock_id'];





class StockMovementResolver {
	id?: number
	stock_id?: number
	destination_stock_id?: number
	operation_id?: number
	destination_operation_id?: number
	constructor(stockMovement) {
		Object.assign(this, stockMovement);
	}

	async items(args, context: GraphQLContext) {
		const query = knex('stock_movement_item')
			.select([...stock_movement_item_fields.map(smif => `stock_movement_item.${smif}`), 'stock_movement_item.id'])
			.leftJoin('stock_item', 'stock_item.id', '=', 'stock_movement_item.stock_item_id')
			.leftJoin('product', 'stock_item.product_id', '=', 'product.id')
			.where({ 'stock_movement_id': this.id! })
			.orderBy(['product.number', 'stock_movement_item.id']);

		const items = await query;

		return items.map(i => new StockMovementItemResolver(i));
	}

	async stock(args, context: GraphQLContext, info) {
		return context.loaderContext.stockLoader.load(this.stock_id!);
	}

	async destination_stock(args, context: GraphQLContext, info) {
		return context.loaderContext.stockLoader.load(this.destination_stock_id!);
	}

	async operation(args, context: GraphQLContext) {
		if (this.operation_id) {
			return context.loaderContext.stockOperationLoader.load(this.operation_id)
		}
	}

	async destination_operation(args, context: GraphQLContext) {
		if (this.destination_operation_id) {
			return context.loaderContext.stockOperationLoader.load(this.destination_operation_id)
		}
	}
}

class StockMovementItemResolver {
	id?: number;
	stock_movement_id?: number;
	stock_item_id?: number;
	stock_item_variant_id?: number;

	constructor(item) {
		Object.assign(this, item);
	}

	async stock_item(args, context, info) {
		return context.loaderContext.stockItemLoader.load(this.stock_item_id);
	}
	async stock_item_variant(args, context: GraphQLContext, info) {
		if (this.stock_item_variant_id) {
			return context.loaderContext.stockItemVariantLoader.load(this.stock_item_variant_id);
		} else {
			return null;
		}
	}

	async destination_reference(args, context, info) {
		if (!this.stock_item_variant_id) {
			return await knex('stock_item')
				.select(['quantity', 'id as stock_item_id', 'stock_id'])
				.where({
					product_id: knex('stock_item').select('product_id').where({ id: this.stock_item_id }),
					"stock_item.stock_id": knex('stock_movement').select('destination_stock_id').where({ id: this.stock_movement_id }),
				})
				.first();
		} else {
			return await knex('stock_item_variant')
				.select(['stock_item_variant.quantity', 'stock_item_variant.id as stock_item_variant_id', 'stock_item.id as stock_item_id', 'stock_item.stock_id'])
				.leftJoin('stock_item', 'stock_item.id', '=', 'stock_item_variant.stock_item_id')
				.where({
					"stock_item.product_id": knex('stock_item').select('product_id').where({ id: this.stock_item_id }),
					"stock_item.stock_id": knex('stock_movement').select('destination_stock_id').where({ id: this.stock_movement_id }),
					"stock_item_variant.variant_id": knex('stock_item_variant').select('variant_id').where({ id: this.stock_item_variant_id })
				})
				.first();
		}
	}
}


export default {

	async createStockMovement(args: gt.MutationCreateStockMovementArgs, context: GraphQLContext, info) {
		context.requireGroups(['rw', 'inventory']);
		const tx = context.knexTransaction;

		const [id] = await knex('stock_movement').insert({
			..._.pick(args.movement, stock_movement_fields),
			stock_id: args.stock_id
		})
			.returning('id')
			.transacting(tx);
		return id;
	},
	async updateStockMovement(args: gt.MutationUpdateStockMovementArgs, context: GraphQLContext, info) {
		context.requireGroups(['rw', 'inventory']);
		const tx = context.knexTransaction;
		const sc = await knex('stock_movement').select(['executed']).where({ id: args.id }).forUpdate().first();
		if (sc.executed) {
			throw new Error("Cannot change this stock movement, because it has already been executed.");
		}


		const [id] = await knex('stock_movement').update({
			..._.pick(args.movement, stock_movement_fields),
		})
			.returning('id')
			.where({
				id: args.id
			})
			.transacting(tx);
		return id;
	},
	async deleteStockMovement(args: gt.MutationDeleteStockMovementArgs, context: GraphQLContext, info) {
		context.requireGroups(['rw', 'inventory']);
		const tx = context.knexTransaction;
		const sc = await knex('stock_movement').select(['executed']).where({ id: args.id }).forUpdate().first();
		if (sc.executed) {
			throw new Error("Cannot delete this stock movement, because it has already been executed.");
		}
		await knex('stock_movement')
			.delete()
			.where({ id: args.id })
			.transacting(tx);

		return args.id;
	},
	async deleteStockMovementItem(args: gt.MutationDeleteStockMovementItemArgs, context: GraphQLContext, info) {
		context.requireGroups(['rw', 'inventory']);
		const tx = context.knexTransaction;
		const sc = await knex('stock_movement').select(['executed']).where({ id: args.stock_movement_id }).forUpdate().first();
		if (!sc) {
			throw new Error("Stock movement not found")
		}
		if (sc.executed) {
			throw new Error("Cannot delete this stock movement item, because it has already been executed.");
		}
		const updates = await knex('stock_movement_item')
			.delete()
			.where({ id: args.stock_movement_item_id, stock_movement_id: args.stock_movement_id })
			.transacting(tx)
			.returning(['id']);

		return updates.length > 0;
	},
	async stockMovements(args: gt.QueryStockMovementsArgs, context: GraphQLContext) {
		context.requireGroups(['inventory']);
		const sc = await knex('stock_movement')
			.select([...stock_movement_fields, "executed", 'stock_id', 'id'])
			.where({
				stock_id: args.stock_id
			})
			.orderBy('date', 'desc');
		return sc.map(s => new StockMovementResolver(s));
	},
	async stockMovement(args: gt.QueryStockMovementArgs, context: GraphQLContext) {
		context.requireGroups(['inventory']);

		const sc = await knex('stock_movement')
			.select([...stock_movement_fields, "operation_id", "destination_operation_id", "executed", 'stock_id', 'id'])
			.where({
				id: args.id
			})
			.first();

		return new StockMovementResolver(sc);
	},
	async updateStockMovementItems(args: gt.MutationUpdateStockMovementItemsArgs, context: GraphQLContext) {
		context.requireGroups(['rw', 'inventory']);
		const tx = context.knexTransaction;
		const sc = await knex('stock_movement').select(['executed']).where({ id: args.stock_movement_id }).forUpdate().first();
		if (sc.executed) {
			throw new Error("Cannot update this stock movement, because it has already been executed.");
		}
		const { stock_id } = await knex('stock_movement').select('stock_id').first().where({ id: args.stock_movement_id });

		await knex('stock_movement_item')
			.transacting(context.knexTransaction)
			.insert(args.items.map(c => ({
				comment: c.comment,
				stock_item_id: c.stock_item_id,
				stock_movement_id: args.stock_movement_id,
				quantity: c.quantity,
				stock_item_variant_id: c.stock_item_variant_id,
				updated_at: context.date,
				stock_id
			})))
			.onConflict(['stock_movement_id', 'stock_item_id', knex.raw('coalesce(stock_item_variant_id, -1)') as unknown as string]).merge(['comment', 'quantity', 'updated_at', 'stock_id'])
		return true;
	},

	async executeStockMovement(args: gt.MutationExecuteStockMovementArgs, context: GraphQLContext) {
		context.requireGroups(['rw', 'inventory']);

		await knex('stock_movement').select(['id']).where({ id: args.stock_movement_id }).forUpdate();
		const sc = await knex('stock_movement')
			.select(['stock_movement.id', 'stock_movement.executed', 'stock_movement.name', 'stock_movement.stock_id', 'stock_movement.destination_stock_id', 'stock_movement.date', 'stock.name as destination', 'source.name as source'])
			.leftJoin('stock', 'stock.id', '=', 'stock_movement.destination_stock_id')
			.leftJoin({ source: 'stock' }, 'source.id', '=', 'stock_movement.stock_id')
			.transacting(context.knexTransaction).where({ "stock_movement.id": args.stock_movement_id }).first();
		if (sc.executed) {
			throw new Error("Stock movement has already been executed on " + sc.executed.toJSON())
		}
		let comment = `Movement ${sc.name}, from ${sc.source} to ${sc.destination}`;
		await knex('stock_movement').select(['id']).where({ id: args.stock_movement_id }).forUpdate();
		// delete empty lines
		await knex('stock_movement_item').transacting(context.knexTransaction)
			.delete()
			.where({
				stock_movement_id: sc.id,
				quantity: 0
			});
		// lock stock_movement_item stock_item/variants and all stock_items in the dest stock
		await knex('stock_item').transacting(context.knexTransaction)
			.select(['stock_item.id']).forUpdate('stock_item')
			.leftJoin('stock_movement_item', knex.raw('stock_movement_item.stock_item_id=stock_item.id'))
			.where('stock_movement_item.stock_movement_id', args.stock_movement_id)
			.orWhere('stock_item.stock_id', sc.destination_stock_id);
		await knex('stock_item_variant').transacting(context.knexTransaction)
			.select(['stock_item_variant.id']).forUpdate('stock_item_variant')
			.leftJoin('stock_item', 'stock_item.id', '=', 'stock_item_variant.stock_item_id')
			.leftJoin('stock_movement_item', knex.raw('stock_movement_item.stock_item_id=stock_item.id'))
			.where('stock_movement_item.stock_movement_id', args.stock_movement_id)
			.orWhere('stock_item.stock_id', sc.destination_stock_id);

		let items = await knex('stock_movement_item').transacting(context.knexTransaction)
			.select(['smi.id', 'smi.quantity as adjustment', 'si.product_id', 'siv.variant_id',
				'smi.stock_item_id', 'smi.stock_item_variant_id', 'sit.id as dest_stock_item_id', 'sivt.id as dest_stock_item_variant_id',
				'si.quantity as si_quantity', 'sit.quantity as sit_quantity', 'siv.quantity as siv_quantity', 'sivt.quantity as sivt_quantity'])
			.from({ smi: 'stock_movement_item' })
			.leftJoin({ m: 'stock_movement' }, 'm.id', '=', 'smi.stock_movement_id')
			.leftJoin({ si: 'stock_item' }, 'si.id', '=', 'smi.stock_item_id')
			.leftJoin({ siv: 'stock_item_variant' }, 'siv.id', '=', 'smi.stock_item_variant_id')
			.leftJoin({ sit: 'stock_item' }, knex.raw('sit.stock_id=m.destination_stock_id AND sit.product_id=si.product_id'))
			.leftJoin({ sivt: 'stock_item_variant' }, knex.raw('sivt.stock_item_id=sit.id AND sivt.variant_id=siv.variant_id'))
			.where('smi.stock_movement_id', args.stock_movement_id);

		if (!items.length) {
			throw new Error("No items found to transfer");
		}

		let [operation_id] = await knex('stock_operation')
			.insert({
				stock_id: sc.stock_id,
				created_by: context.token.sub,
				date: context.date,
				type: 'movement',
				reference: `stock_movement#${sc.id}`,
				comment
			}).returning('id').transacting(context.knexTransaction);

		let [destination_operation_id] = await knex('stock_operation')
			.insert({
				stock_id: sc.destination_stock_id,
				created_by: context.token.sub,
				date: context.date,
				type: 'movement',
				reference: `stock_movement#${sc.id}`,
				comment
			}).returning('id').transacting(context.knexTransaction);

		for (let e of items) {
			if (e.adjustment === 0) {
				continue;
			}
			if (!e.dest_stock_item_id) {
				let [{ id }] = await knex('stock_item').transacting(context.knexTransaction)
					.insert({
						product_id: e.product_id,
						stock_id: sc.destination_stock_id,
						quantity: 0
					}).returning(['id']);
				e.dest_stock_item_id = id;
				e.sit_quantity = 0;
				for (let ex of items) {
					if (ex.product_id === e.product_id && !ex.dest_stock_item_id) {
						ex.dest_stock_item_id = e.dest_stock_item_id;
					}
				}
			}
			if (e.stock_item_variant_id && !e.dest_stock_item_variant_id) {
				let [{ id }] = await knex('stock_item_variant').transacting(context.knexTransaction)
					.insert({
						variant_id: e.variant_id,
						stock_item_id: e.dest_stock_item_id,
						quantity: 0
					}).returning(['id']);
				e.dest_stock_item_variant_id = id;
				e.sivt_quantity = 0;
			}

			if (!e.stock_item_variant_id) {
				let [{ quantity }] = await knex('stock_item').transacting(context.knexTransaction)
					.update({
						quantity: knex.raw('quantity+?', -e.adjustment)
					}).where({ id: e.stock_item_id })
					.returning(['quantity']);
				await knex('stock_adjustment')
					.transacting(context.knexTransaction)
					.insert({
						product_id: e.product_id,
						stock_item_id: e.stock_item_id,
						stock_item_variant_id: null,
						variant_id: null,
						adjustment: -e.adjustment,
						new_quantity: quantity,
						old_quantity: e.si_quantity,
						operation_id
					});
				let [{ quantity: quantity2 }] = await knex('stock_item').transacting(context.knexTransaction)
					.update({
						quantity: knex.raw('quantity+?', e.adjustment)
					}).where({ id: e.dest_stock_item_id })
					.returning(['quantity']);

				await knex('stock_adjustment')
					.transacting(context.knexTransaction)
					.insert({
						product_id: e.product_id,
						stock_item_id: e.dest_stock_item_id,
						stock_item_variant_id: null,
						variant_id: null,
						adjustment: +e.adjustment,
						new_quantity: quantity2,
						old_quantity: e.sit_quantity,
						operation_id: destination_operation_id
					});
			} else { //if (e.stock_item_variant_id) 
				let [{ quantity }] = await knex('stock_item_variant').transacting(context.knexTransaction)
					.update({
						quantity: knex.raw('quantity-?', e.adjustment)
					}).where({ id: e.stock_item_variant_id })
					.returning(['quantity']);
				await knex('stock_adjustment')
					.transacting(context.knexTransaction)
					.insert({
						product_id: e.product_id,
						stock_item_id: e.stock_item_id,
						stock_item_variant_id: e.stock_item_variant_id,
						variant_id: e.variant_id,
						adjustment: -e.adjustment,
						new_quantity: quantity,
						old_quantity: e.siv_quantity,
						operation_id
					});
				let [{ quantity: quantity2 }] = await knex('stock_item_variant').transacting(context.knexTransaction)
					.update({
						quantity: knex.raw('quantity+?', e.adjustment)
					}).where({ id: e.dest_stock_item_variant_id })
					.returning(['quantity']);

				await knex('stock_adjustment')
					.transacting(context.knexTransaction)
					.insert({
						product_id: e.product_id,
						stock_item_id: e.dest_stock_item_id,
						stock_item_variant_id: e.dest_stock_item_variant_id,
						variant_id: e.variant_id,
						adjustment: +e.adjustment,
						new_quantity: quantity2,
						old_quantity: e.sivt_quantity,
						operation_id: destination_operation_id
					});
			}
		}
		await knex('stock_movement').update({
			executed: context.date,
			operation_id,
			destination_operation_id
		}).where({ id: sc.id }).transacting(context.knexTransaction);

		return true;
	}
}

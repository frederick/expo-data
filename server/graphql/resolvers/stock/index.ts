import stock from "./stock"
import stock_movement from "./stock_movement"
import stock_count from "./stock_count"
export default {
  ...stock,
  ...stock_count,
  ...stock_movement
}
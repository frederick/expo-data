export class AccessError extends Error {
  reason: string;

  constructor({ reason, message }) {
    super(message)
    this.reason = reason;
  }
}

export function requireGroup(groups, either = false) {
  if (!Array.isArray(groups)) {
    groups = [groups]
  }
  var fun = groups.every
  if (either) {
    fun = groups.some
  }

  return (gs: Set<string>) => {
    if (fun.call(groups, g => gs && gs.has(g))) {
      return;
    } else {
      throw new AccessError({
        "reason": "forbidden",
        "message": "You do not have access to that feature."
      })
    }
  }
}

export const reqAdmin = requireGroup(['admin', 'super-admin'], true)
export const reqUser = requireGroup(['user', 'admin', 'super-admin'], true)
export const reqRW = requireGroup('rw')
export const factory = reqRW
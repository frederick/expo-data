import { graphqlHTTP } from 'express-graphql';
import { buildSchema, GraphQLScalarType, Kind, execute, OperationDefinitionNode, formatError } from 'graphql';
import fs from 'fs';
import productResolver from './resolvers/product';
import categoryResolver from './resolvers/category';
import ticketResolver from './resolvers/ticket';
import receiptResolver from './resolvers/receipt';
import inventoryResolver from './resolvers/inventory';
import reportResolver from './resolvers/report';
import configurations from './resolvers/configurations';
import salesJournalResolver from './resolvers/salesJournal';
import historyResolver from './resolvers/history';
import stockResolver from './resolvers/stock';
import knex from '../db/knex'
import Context from './resolvers/loaders';
import moment from "moment";
import { handleTokenError } from '../middleware/authware';
import type GraphQLContext from './GraphQLContext';
import type ExtendedRequest from '../types/Request';

const schema = buildSchema(fs.readFileSync(`graphql/schema.graphql`).toString());


Object.assign(schema.getType('Date'), {
	description: 'Date custom scalar type',

	serialize(value) {
		return value?.toJSON(); // Convert outgoing Date to integer for JSON
	},
	parseValue(value) {
		return new Date(value); // Convert incoming integer to Date
	},
	parseLiteral(ast) {
		if (ast.kind === Kind.INT) {
			return new Date(parseInt(ast.value, 10)); // Convert hard-coded AST string to integer and then to Date
		} else if (ast.kind === Kind.STRING) {
			return new Date(ast.value);
		}
		return null; // Invalid hard-coded value (not an integer or string)
	},
});

Object.assign(schema.getType('DateOnly'), {
	description: 'Date custom scalar type without time',

	serialize(value) {
		return moment(value).format('YYYY-MM-DD'); // Convert outgoing Date to integer for JSON
	},
	parseValue(value) {
		return moment(value, "YYYY-MM-DD").format('YYYY-MM-DD');
	},
	parseLiteral(ast) {
		if (ast.kind === Kind.STRING) {
			return moment(ast.value, "YYYY-MM-DD").format('YYYY-MM-DD');
		}
		return null; // Invalid hard-coded value (not an integer or string)
	},
});


Object.assign(schema.getType('JSON'), {
	description: 'JSON custom scalar type',

	serialize(value) {
		return (value); // Convert outgoing Date to integer for JSON
	},
	parseValue(value) {
		return value; // Convert incoming integer to Date
	},
	parseLiteral(ast) {
		if (ast.kind === Kind.OBJECT) {
			return ast.value;
		}
		return null;
	},
});

module.exports = graphqlHTTP((reqs, res) => {
	let req = reqs as unknown as ExtendedRequest;
	return {
		schema,
		context: {
			loaderContext: new Context(),
			groups: req.groups,
			requireGroups: req.requireGroups,
			token: req.token,
			date: new Date()
		} as GraphQLContext,
		async customExecuteFn(args) {

			if (!req.token || req.token.anonymous) {
				const e = new Error('Authentication required');
				e.name = 'AnonymousUserError';
				throw e;
			}

			let hasMutation = args.document?.definitions.find(d => {
				return (d as OperationDefinitionNode).operation === 'mutation'
			})
			if (hasMutation) {
				return await knex.transaction(async (tx) => {
					args.contextValue.knexTransaction = tx;
					// sanitize username (even though the escaping below should work for any string)
					let username = req.token.sub?.replace(/[^A-z-.]/g, '');
					try {
						await tx.raw(`SET LOCAL app.username=${knex.raw('?', [username])};`, )
						return await execute(args);
					} finally {
						// await tx.raw(`SET SESSION app.username=''`,)
					}


				});
			} else {
				return await execute(args);
			}

		},
		customFormatErrorFn(err) {
			if (err.originalError) {
				const oe = err.originalError as any;
				if (oe.code === "28P01") {
					err.message = "Authentication with db failed";
				}
				if (oe.code === "23505") {
					err.message = "Value already taken";
				}
			}
			console.error(err)
			let e: any = formatError(err);
			if (err.name === 'AnonymousUserError') {
				e.login = process.env.WEBAUTHD_PUBLIC_URL;
				e.error = 'TokenExpiredError';
				res.statusCode = e.code = 403;
			}
			if (err.originalError?.name === 'AccessError') {
				e.error = 'AccessError';
				res.statusCode = e.code = 400;
			}
			return e;
		},
		rootValue: {
			...productResolver,
			...categoryResolver,
			...ticketResolver,
			...receiptResolver,
			...inventoryResolver,
			...reportResolver,
			...salesJournalResolver,
			...stockResolver,
			...historyResolver,
			...configurations,
		
		},
		graphiql: {
			headerEditorEnabled: true
		},
	}
});

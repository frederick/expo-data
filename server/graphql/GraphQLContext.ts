import { Knex } from 'knex'
import type LoaderContext from './resolvers/loaders'

export type requireGroupsFn = (groups: string[] | string, either?: boolean ) => void

export default interface GraphQLContext {
  loaderContext: LoaderContext
	groups: Set<string>
	requireGroups: requireGroupsFn
	knexTransaction: Knex.Transaction<any, any[]>,
	token: any
	date: Date
}
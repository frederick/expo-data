const path = require("path");

require("dotenv").config();
var express = require("express");
var app = express();
const auth = require('./middleware/auth');

process.on("unhandledRejection", function (reason, p) {
  console.log(
    "Possibly Unhandled Rejection at: Promise ",
    p,
    " reason: ",
    reason
  );
  process.exit(-1);
});

app.use(
  require("cors")({
    origin: (origin, callback) => {
      if (
        !origin ||
        [
          "http://localhost:8080",
          "http://localhost:3000",
          "http://10.44.1.1",
          ...JSON.parse(process.env.CORS_ORIGINS ?? '[]'),
        ].indexOf(origin) !== -1
      ) {
        callback(null, true);
      } else {
        callback(new Error(`${origin} not allowed by CORS`));
      }
    },
    allowedHeaders: ["authorization", "content-type"]
  })
);

app.use(require("compression")());

app.use('/expo-data/api', auth, require("./routes"));
app.use('/expo-data/graphql', auth, require('./graphql'));

app.use('/expo-data/', express.static('public'))


app.use(function (req, res, next) {
  res.setHeader("Content-Type", "text/html");
  res.sendFile(path.resolve("./public", "index.html"));
});

var server = app.listen(3000);
server.on('listening', () => {
  console.log(`Up in ${process.uptime() * 1000}ms`)
})
process.on('SIGTERM', function () {
  server.close(function () {
    process.exit(0);
  });
});


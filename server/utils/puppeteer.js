var browser;
const puppeteer = require("puppeteer-core");

module.exports = {
  browser: new Promise(async function(resolve, reject) {
    try{
      var browser = await puppeteer.launch({
        executablePath: "/usr/bin/chromium-browser",
        headless: true
      });
      resolve(browser);
    }catch(e) {
      console.error("Chromium not found. Cannot generate PDFs. We will continue launching anyway, don't worry... ", e);
      resolve(null);
    }
    
  })
};

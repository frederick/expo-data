const NOT_FOUND = {
  status: 404,
  message: "Not found"
};

module.exports = {
  NOT_FOUND
};


const {dec,cur, decd} = require('./decimals')
const moment = require('moment')
const nunjucks = require('nunjucks')
var env = new nunjucks.Environment();

nunjucks.configure({
})

env.addFilter('cur', cur);
env.addFilter('dec', dec);
env.addFilter('decd', decd);
env.addFilter('moment', (v, fmt) => moment(v).format(fmt))

module.exports = env;
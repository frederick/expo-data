
const jwt = require('jsonwebtoken')

const mode_algorithms = {
  'shared_secret': 'HS256',
  'public_key': 'RS256'
}

async function signToken({ app, subject, exp, mode = 'shared_secret', payload, secret }) {
  if (!secret)
    throw new Error("No secret provided")
  const algorithm = mode_algorithms[mode];
  if (!algorithm)
    throw new Error(`Unknown mode ${mode}`)
  const options = {
    issuer: "webauthd",
    subject,
    audience: app,
    notBefore: `-${1000 * 60 * 5}s`, // allow up to 5 minutes of time differential
    expiresIn: exp,
    algorithm
  }

  if (payload.exp) {
    // the expiration is defined in the payload
    delete options.expiresIn
  }
  const token = jwt.sign(
    payload,
    secret,
    options
  );

  return token
}


async function createToken({ app, secret, mode = 'shared_secret', user, groups, type, li, exp, payload = {}, token_format = 'default' }) {
  if (!secret)
    throw new Error("No secret provided")
  if (!li) {
    li = Math.floor(new Date().getTime() / 1000);
  }
  // calculate expiration timestamp
  if (type === 'access') {
    if (!exp) exp = '12m'
  } else if (type === 'goto') {
    if (!exp) exp = '3m'
  } else if (type === 'refresh') {
    if (!exp) exp = '1d'
  } else {
    throw new Error("Unknown token type: " + type)
  }

  if (type === 'access') {
    let extra = token_format !== 'couchdb' ? {} : {
      kid: app,
      '_couchdb.roles': groups
    }
    return await signToken({
      app,
      exp,
      subject: user.username,
      payload: {
        type: "access",
        groups,
        applications: [app],
        firstname: user.firstname,
        lastname: user.lastname,
        email: user.email,
        li,
        ...payload
      },
      mode,
      secret,
      ...extra
    })
  } else if (type === 'refresh' || type === 'goto') {
    return await signToken({
      app: 'webauthd-login',
      exp,
      subject: user.username,
      payload: {
        type: "refresh",
        application: app,
        li,
        ...payload
      },
      mode,
      secret
    })
  }

}

module.exports = {
  createToken,
  signToken
}



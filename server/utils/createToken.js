require('dotenv').config();
const readline = require('readline');
const util = require('util');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stderr
});
const question = util.promisify(rl.question).bind(rl);

const { createToken } = require('./tokens');

async function main() {
  const token = await createToken({
    app: 'expo-data',
    user: {
      username: await question('Username:'),
      firstname: '',
      lastname: '',
      email: '',
    },
    type: 'access',
    groups: (await question('Groups (separated by ","):')).split(','),
    exp: await question('Expiration (e.g. 15m, 24h):'),
    secret: process.env.JWT_SECRET
  });
  console.log(token);
  process.exit(0);
}

main()
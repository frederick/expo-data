const cur_formatter = new Intl.NumberFormat("fr-FR", {
  style: "currency",
  currency: "EUR",
  minimumFractionDigits: 2
});

const dec_formatter = new Intl.NumberFormat("fr-FR", {
  style: "decimal",
  minimumFractionDigits: 0
});

function cur(n) {
  return cur_formatter.format(n);
}
function dec(n) {
  return dec_formatter.format(n);
}

function decd(n, d) {
  var formatter = new Intl.NumberFormat("fr-FR", {
    style: "decimal",
    minimumFractionDigits: d
  });
  return formatter.format(n);
}

module.exports = {
  cur_formatter,
  dec_formatter,
  cur,
  dec,
  decd
};


ALTER TABLE public.product ADD sku varchar NULL;
ALTER TABLE public.product_variant ADD sku varchar NULL;

drop view public.vw_product_variants;


CREATE OR REPLACE VIEW public.vw_product_variants
AS SELECT p.id AS product_id,
    v.id AS variant_id,
    concat(p.name, COALESCE(NULLIF(max(v.name::text), ''::text),
        CASE
            WHEN v.id IS NULL THEN ''::text
            ELSE concat(' - ', string_agg(attv.value, ','::text))
        END::character varying::text)) AS full_name,
    COALESCE(v.name::text, string_agg(attv.value, ','::text)::character varying::text) AS variant_name,
	v.name,
  v.sku,
    v.code AS variant_code,
    c.rate AS tax_rate,
    c.name::text AS category,
    p.pasteque_product_id,
    p.version,
    p.number,
    p.category_code,
    p.criteria,
    p.name as product_name,
    p.sku as product_sku,
    p.supplier,
    p.barcode,
    p.price,
    p.import_date,
    p.attributes,
    p.label,
    p.active,
    p.id,
    json_agg(json_build_object('attribute_name', att.name, 'attribute_type', att.type, 'attribute_id', att.id, 'attribute_code', att.code, 'value_id', attv.id, 'value_code', attv.code, 'id', av.id, 'value', attv.value, 'color',
        CASE att.type
            WHEN 'color'::text THEN attv.color
            ELSE NULL::character varying
        END)) FILTER (WHERE attv.id IS NOT NULL) AS attribute_values,
    ( SELECT (EXISTS ( SELECT pp.id
                   FROM pasteque.products pp
                  WHERE pp.id = p.number)) AS "exists") AS has_pasteque
   FROM product p
     LEFT JOIN product_variant v ON v.product_id = p.id
     LEFT JOIN product_variant_attribute_value av ON av.variant_id = v.id
     LEFT JOIN product_attribute_value attv ON av.attribute_value_id = attv.id
     LEFT JOIN product_attribute att ON att.id = av.attribute_id
     LEFT JOIN product_category c ON p.category_code::text = c.code::text
  GROUP BY p.id, v.id, v.name, v.code, c.rate, c.name
  ORDER BY p.number, v.id;


ALTER TABLE public.stock ADD "version" int NOT NULL DEFAULT 0;


----------- WEd
ALTER TABLE public.stock_product RENAME TO stock_item;


-- public.stock_item_variant definition

-- Drop table

-- DROP TABLE public.stock_item_variant;

CREATE TABLE public.stock_item_variant (
	id serial NOT NULL,
	stock_item_id int4 NOT NULL,
	variant_id int4 NULL,
	quantity float8 NOT NULL DEFAULT 0,
	notes varchar NULL,
	CONSTRAINT stock_item_variant_pk PRIMARY KEY (id),
	CONSTRAINT stock_item_variant_un_item_variant UNIQUE (stock_item_id, variant_id)
);


-- public.stock_item_variant foreign keys

ALTER TABLE public.stock_item_variant ADD CONSTRAINT stock_item_variant_fk_product_variant FOREIGN KEY (variant_id) REFERENCES public.product_variant(id) ON DELETE CASCADE;
ALTER TABLE public.stock_item_variant ADD CONSTRAINT stock_item_variant_fk_stock_item FOREIGN KEY (stock_item_id) REFERENCES public.stock_item(id) ON DELETE CASCADE;
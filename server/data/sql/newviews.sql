ALTER TABLE public.product_attribute_value ADD CONSTRAINT product_attribute_value_pk PRIMARY KEY (id);

CREATE VIEW public.vw_product_matches
AS
SELECT p.* FROM (
		SELECT p.id, pp.id AS pasteque_id,
		p.barcode = pp.barcode AS barcode_matches,
		ROUND(p.price::numeric,2) = ROUND((pp.pricesell * (1+t.rate))::numeric,2) as price_matches,
		p.name = pp.label AS name_matches,
		cat.name = pcat.label as category_matches,
		cat.rate / 100 = t.rate as rate_matches,
		p.active = pp.visible as active_matches
		FROM product p
		LEFT JOIN pasteque.products pp ON pp.id=p.number
		LEFT JOIN pasteque.taxes t ON pp.tax_id=t.id
		LEFT JOIN pasteque.categories pcat ON pcat.id=pp.category_id
		LEFT JOIN product_category cat ON cat.code=p.category_code
		) p WHERE p.pasteque_id IS NULL
		OR NOT(p.barcode_matches AND p.price_matches AND p.name_matches AND p.category_matches AND p.rate_matches AND p.active_matches)
		;
-- public.vw_product_attributes source
-- public.vw_product_attributes source

CREATE OR REPLACE VIEW public.vw_product_attributes
AS SELECT pa.id,
    pa.code,
    pa.description,
    pa.name,
    pa.type,
    coalesce (
    json_agg(json_build_object('id', pav.id, 'code', pav.code, 'value', pav.value, 'color', pav.color)) FILTER (WHERE pav.id IS NOT NULL), 
    '[]'::json)
    AS "values"
   FROM product_attribute pa
     LEFT JOIN product_attribute_value pav ON pav.attribute_id = pa.id
  GROUP BY pa.id;

-- public.vw_product_variants source

CREATE OR REPLACE VIEW public.vw_product_variants
AS SELECT p.id AS product_id,
    v.id AS variant_id,
    concat(p.name, COALESCE(NULLIF(max(v.name::text), ''::text),
        CASE
            WHEN v.id IS NULL THEN ''::text
            ELSE concat(' - ', string_agg(attv.value, ','::text))
        END::character varying::text)) AS full_name,
    COALESCE(v.name::text, string_agg(attv.value, ','::text)::character varying::text) AS variant_name,
    v.code AS variant_code,
    c.rate AS tax_rate,
    c.name::text AS category,
    p.pasteque_product_id,
    p.version,
    p.number,
    p.category_code,
    p.criteria,
    p.name,
    p.supplier,
    p.barcode,
    p.price,
    p.import_date,
    p.attributes,
    p.label,
    p.active,
    p.id,
    json_agg(json_build_object('attribute_name', att.name, 'attribute_type', att.type, 'attribute_id', att.id, 'attribute_code', att.code, 'value_id', attv.id, 'value_code', attv.code, 'id', av.id, 'value', attv.value, 'color',
        CASE att.type
            WHEN 'color'::text THEN attv.color
            ELSE NULL::character varying
        END)) FILTER (WHERE attv.id IS NOT NULL) AS attribute_values,
    ( SELECT (EXISTS ( SELECT pp.id
                   FROM pasteque.products pp
                  WHERE pp.id = p.number)) AS "exists") AS has_pasteque
   FROM product p
     LEFT JOIN ( SELECT v_1.id,
            v_1.product_id,
            v_1.name,
            v_1.code
           FROM product_variant v_1) v ON v.product_id = p.id
     LEFT JOIN product_variant_attribute_value av ON av.variant_id = v.id
     LEFT JOIN product_attribute_value attv ON av.attribute_value_id = attv.id
     LEFT JOIN product_attribute att ON att.id = av.attribute_id
     LEFT JOIN product_category c ON p.category_code::text = c.code::text
  GROUP BY p.id, v.id, v.name, v.code, c.rate, c.name
  ORDER BY p.number, v.id;

  alter table product_category add column pasteque_tax integer;
update product_category set pasteque_tax = 5 where rate=5.5;
 update product_category set pasteque_tax = 1 where rate=0;
 update product_category set pasteque_tax = 4 where rate=20;

 ALTER TABLE public.product_attribute_value ADD CONSTRAINT product_attribute_value_un_attr_val UNIQUE (attribute_id,value);
  ALTER TABLE public.product_attribute_value ADD CONSTRAINT product_attribute_value_un_attr_code UNIQUE (attribute_id,code);

ALTER TABLE public.product_attribute_value DROP CONSTRAINT product_attribute_value_product_attribute_id_fkey;
ALTER TABLE public.product_attribute_value ADD CONSTRAINT product_attribute_value_product_attribute_id_fkey FOREIGN KEY (attribute_id) REFERENCES product_attribute(id) on delete CASCADE;
CREATE OR REPLACE VIEW public.vw_product_attributes
AS SELECT pa.id,
    pa.code,
    pa.description,
    pa.name,
    pa.type,
    coalesce (
    json_agg(json_build_object('id', pav.id, 'code', pav.code, 'value', pav.value, 'color', pav.color)) FILTER (WHERE pav.id IS NOT NULL), 
    '[]'::json)
    AS "values"
   FROM product_attribute pa
     LEFT JOIN product_attribute_value pav ON pav.attribute_id = pa.id
  GROUP BY pa.id;

------------------------
create or replace view vw_tickets as
          SELECT distinct on (t.id)
			t.id,t.cashregister_id ,t.user_id ,t.customer_id,t.tariffarea_id,t.discountprofile_id ,t."sequence" ,t.number,t.date,t.custcount,t.discountrate ,t.taxedprice , t.price,t.finalprice ,t.finaltaxedprice , t.custbalance , t.finaltaxedprice as total, t.finalprice as total_ht,
          c.reference as cash_register, SUM(tl.quantity) as items,
          (SELECT json_agg(tt.*) FROM pasteque.tickettaxes  tt WHERE tt.ticket_id=t.id)
          as taxes,
          (SELECT string_agg(pm.reference,',') FROM pasteque.ticketpayments tp LEFT JOIN pasteque.paymentmodes pm ON pm.id=tp.paymentmode_id WHERE tp.ticket_id=t.id)
          as payment_mode,
          json_agg(tl.*) as lines
          FROM pasteque.tickets t 
          LEFT JOIN pasteque.cashregisters c ON c.id=t.cashregister_id
          LEFT JOIN pasteque.ticketlines tl ON tl.ticket_id = t.id
          GROUP BY t.id,t.cashregister_id ,t.user_id ,t.customer_id,t.tariffarea_id,t.discountprofile_id ,t."sequence" ,t.number,t.date,t.custcount,t.discountrate ,t.taxedprice , t.price,t.finalprice ,t.finaltaxedprice , t.custbalance ,cash_register, tl.disporder 
          ORDER BY t.id, tl.disporder;


-- public.tz_vw_inventory_with_start_date source

CREATE OR REPLACE VIEW public.vw_inventory_with_start_date
AS SELECT i.id,
    i.date,
    i.period_start,
    i.inventory_type,
    i.comments,
    i.version,
    (COALESCE(lead(i.date) OVER (ORDER BY i.date DESC), '2018-12-31'::date) + '1 day'::interval)::date AS start_date
   FROM inventory i
  WHERE i.inventory_type = 'continuous'::inventory_type
UNION ALL
 SELECT i.id,
    i.date,
    i.period_start,
    i.inventory_type,
    i.comments,
    i.version,
    i.period_start AS start_date
   FROM inventory i
  WHERE i.inventory_type = 'special'::inventory_type
  ORDER BY 2;





ALTER TABLE public.payment_method 
ADD COLUMN    display_name character varying,
ADD COLUMN barcode character varying,
ADD COLUMN polimod character varying,
ADD COLUMN pasteque_reference character varying,
ADD COLUMN pasteque_id integer;

INSERT INTO public.payment_method VALUES (1, 'Espèces', 'cash', '{"barcode": "9999999900006", "polimod": "RECU", "pasteque_id": 1}', 'Espèces', '9999999900006', 'RECU', 'cash', 1) ON CONFLICT (id) DO UPDATE SET name=EXCLUDED.name, code=EXCLUDED.code,attributes=EXCLUDED.attributes,display_name=EXCLUDED.display_name,barcode=EXCLUDED.barcode,polimod=EXCLUDED.polimod,pasteque_reference=EXCLUDED.pasteque_reference,pasteque_id=EXCLUDED.pasteque_id;
INSERT INTO public.payment_method VALUES (3, 'CB', 'bank_card', '{"barcode": "9999999990038", "polimod": "C-BL", "pasteque_id": 3}', 'Carte bancaire', '9999999990038', 'C-BL', 'magcard', 3) ON CONFLICT (id) DO UPDATE SET name=EXCLUDED.name, code=EXCLUDED.code,attributes=EXCLUDED.attributes,display_name=EXCLUDED.display_name,barcode=EXCLUDED.barcode,polimod=EXCLUDED.polimod,pasteque_reference=EXCLUDED.pasteque_reference,pasteque_id=EXCLUDED.pasteque_id;;
INSERT INTO public.payment_method VALUES (2, 'Chèque', 'cheque', '{"barcode": "9999999990007", "polimod": "CHEQ", "pasteque_id": 2}', 'Chèque', '9999999990007', 'CHEQ', 'cheque', 2) ON CONFLICT (id) DO UPDATE SET name=EXCLUDED.name, code=EXCLUDED.code,attributes=EXCLUDED.attributes,display_name=EXCLUDED.display_name,barcode=EXCLUDED.barcode,polimod=EXCLUDED.polimod,pasteque_reference=EXCLUDED.pasteque_reference,pasteque_id=EXCLUDED.pasteque_id;;






-----------------------------------------------------------------------




drop view public.vw_product_variants;
CREATE OR REPLACE VIEW public.vw_product_variants
AS SELECT p.id AS product_id,
    v.id AS variant_id,
    concat(p.name, COALESCE(NULLIF(max(v.name::text), ''::text),
        CASE
            WHEN v.id IS NULL THEN ''::text
            ELSE concat(' - ', string_agg(attv.value, ','::text))
        END::character varying::text)) AS full_name,
    COALESCE(v.name::text, string_agg(attv.value, ','::text)::character varying::text) AS variant_name,
	v.name,
    v.code AS variant_code,
    c.rate AS tax_rate,
    c.name::text AS category,
    p.pasteque_product_id,
    p.version,
    p.number,
    p.category_code,
    p.criteria,
    p.name as product_name,
    p.supplier,
    p.barcode,
    p.price,
    p.import_date,
    p.attributes,
    p.label,
    p.active,
    p.id,
    json_agg(json_build_object('attribute_name', att.name, 'attribute_type', att.type, 'attribute_id', att.id, 'attribute_code', att.code, 'value_id', attv.id, 'value_code', attv.code, 'id', av.id, 'value', attv.value, 'color',
        CASE att.type
            WHEN 'color'::text THEN attv.color
            ELSE NULL::character varying
        END)) FILTER (WHERE attv.id IS NOT NULL) AS attribute_values,
    ( SELECT (EXISTS ( SELECT pp.id
                   FROM pasteque.products pp
                  WHERE pp.id = p.number)) AS "exists") AS has_pasteque
   FROM product p
     LEFT JOIN ( SELECT v_1.id,
            v_1.product_id,
            v_1.name,
            v_1.code
           FROM product_variant v_1) v ON v.product_id = p.id
     LEFT JOIN product_variant_attribute_value av ON av.variant_id = v.id
     LEFT JOIN product_attribute_value attv ON av.attribute_value_id = attv.id
     LEFT JOIN product_attribute att ON att.id = av.attribute_id
     LEFT JOIN product_category c ON p.category_code::text = c.code::text
  GROUP BY p.id, v.id, v.name, v.code, c.rate, c.name
  ORDER BY p.number, v.id;
  
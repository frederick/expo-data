#!/bin/bash
set -x
podman rm -f expo-postgres
source .env
mkdir -p data/postgres
podman run --rm -d \
    --name expo-postgres \
    -e POSTGRES_PASSWORD=${PGPGPW} \
    -e POSTGRES_USER=${PGUSER} \
    -e POSTGRES_PASSWORD=${PGPASSWORD} \
    -e PGDATA=/var/lib/postgresql/data/pgdata \
    -v ./data/postgres:/var/lib/postgresql/data:Z \
    -p 45432:5432 \
    postgres:alpine

podman logs -f expo-postgres
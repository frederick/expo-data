require('dotenv').config()
const jwt = require('jsonwebtoken')

console.log(jwt.sign({
  type: 'access'
}, process.env.JWT_SECRET, {
    audience: 'expo-data',
    algorithm: 'HS256',
    expiresIn: '1y',
    issuer: 'expo-data',
    subject: 'expo-pc'
  }))
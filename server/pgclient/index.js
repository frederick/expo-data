

const { Client, types, Query } = require('pg')

types.setTypeParser(1114, function(stringValue) {
    return new Date(stringValue + "+0000");
});

const submit = Query.prototype.submit;
Query.prototype.submitx = function() {
  const text = this.text;
  const values = this.values || [];
  const query = values.reduce((q, v, i) => q.replace(`$${i + 1}`, v), text);
  console.log(query);
  submit.apply(this, arguments);
};

module.exports = {
    createClient({ user, password, database } = {}) {
        const client = new Client({
            database
        })

        return { client };

    }
}
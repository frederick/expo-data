

function one({rows}){
  if(rows[0] && rows.length === 1){
    return rows[0]
  }else if(rows.length >1){
    throw new Error('More than one result found')
  }else if(rows.length === 0){
    throw new Error('No result')
  }
}


module.exports = {
  one,
  single
}
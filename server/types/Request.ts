import type {Request} from "express";
import type {requireGroupsFn} from "../graphql/GraphQLContext"
export interface Token {
	anonymous?: boolean;
  groups: string[]
  sub: string
}
export default interface ExtendedRequest extends Request{
  groups: Set<string>
  
  requireGroups: requireGroupsFn

  token:  Token

}
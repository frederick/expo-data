export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Date: Date;
  DateOnly: any;
  JSON: any;
};

export type Category = {
  __typename?: 'Category';
  code: Scalars['String'];
  version: Scalars['Int'];
  name?: Maybe<Scalars['String']>;
  rate?: Maybe<Scalars['Float']>;
  pasteque_tax?: Maybe<Scalars['Int']>;
  comment?: Maybe<Scalars['String']>;
};

export type ConfigurationSetting = {
  __typename?: 'ConfigurationSetting';
  key: Scalars['String'];
  value?: Maybe<Scalars['String']>;
};

export type Customer = {
  __typename?: 'Customer';
  id: Scalars['Int'];
  version: Scalars['Int'];
  firstname?: Maybe<Scalars['String']>;
  lastname?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  phone1?: Maybe<Scalars['String']>;
  phone2?: Maybe<Scalars['String']>;
  fax?: Maybe<Scalars['String']>;
  address?: Maybe<Scalars['String']>;
  zipcode?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  region?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  note?: Maybe<Scalars['String']>;
};

export type CustomerInput = {
  id?: Maybe<Scalars['Int']>;
  version?: Maybe<Scalars['Int']>;
  firstname: Scalars['String'];
  lastname: Scalars['String'];
  email?: Maybe<Scalars['String']>;
  phone1?: Maybe<Scalars['String']>;
  phone2?: Maybe<Scalars['String']>;
  fax?: Maybe<Scalars['String']>;
  address?: Maybe<Scalars['String']>;
  zipcode?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  region?: Maybe<Scalars['String']>;
  country: Scalars['String'];
  note?: Maybe<Scalars['String']>;
};



export type HistoryChange = {
  __typename?: 'HistoryChange';
  date?: Maybe<Scalars['Date']>;
  user?: Maybe<Scalars['String']>;
  changed_fields?: Maybe<Scalars['JSON']>;
  changed_objects?: Maybe<Scalars['JSON']>;
};

export type Inventory = {
  __typename?: 'Inventory';
  id: Scalars['Int'];
  version: Scalars['Int'];
  date?: Maybe<Scalars['DateOnly']>;
  period_start?: Maybe<Scalars['DateOnly']>;
  inventory_type: Scalars['String'];
  comments?: Maybe<Scalars['String']>;
  start_date?: Maybe<Scalars['Date']>;
};

export type InventoryInput = {
  id?: Maybe<Scalars['Int']>;
  version?: Maybe<Scalars['Int']>;
  date?: Maybe<Scalars['DateOnly']>;
  period_start?: Maybe<Scalars['DateOnly']>;
  inventory_type: Scalars['String'];
  comments?: Maybe<Scalars['String']>;
};


export type Mutation = {
  __typename?: 'Mutation';
  createProduct: Scalars['Int'];
  updateProduct: Scalars['Int'];
  deleteProduct: Scalars['Int'];
  createProductVariant: Scalars['Int'];
  updateProductVariant: Scalars['Int'];
  deleteProductVariant: Scalars['Int'];
  updateProductCustomAttributes: Scalars['Int'];
  createProductAttribute: Scalars['Int'];
  updateProductAttribute: Scalars['Int'];
  deleteProductAttribute: Scalars['Int'];
  changeProductAttributeValuePosistion?: Maybe<Scalars['Boolean']>;
  updatePastequeProduct: Scalars['Int'];
  createPastequeProduct: Scalars['Int'];
  createReceipt: Scalars['Int'];
  updateReceipt: Scalars['Int'];
  deleteReceipt: Scalars['Int'];
  createInventory: Scalars['Int'];
  updateInventory: Scalars['Int'];
  deleteInventory: Scalars['Int'];
  createReport: Scalars['Int'];
  updateReport: Scalars['Int'];
  deleteReport: Scalars['Int'];
  createStock: Scalars['Int'];
  updateStock: Scalars['Int'];
  deleteStock: Scalars['Int'];
  createStockItems: Array<Maybe<Scalars['Int']>>;
  updateStockItem: Scalars['Int'];
  removeStockItems: Scalars['Boolean'];
  updateStockItemQuantities: Scalars['Boolean'];
  createStockCount: Scalars['Int'];
  updateStockCount: Scalars['Int'];
  deleteStockCount: Scalars['Int'];
  updateStockCountItems: Scalars['Boolean'];
  transferStockCount: Scalars['Boolean'];
  createStockMovement: Scalars['Int'];
  updateStockMovement: Scalars['Int'];
  deleteStockMovement: Scalars['Int'];
  updateStockMovementItems: Scalars['Boolean'];
  deleteStockMovementItem: Scalars['Boolean'];
  executeStockMovement: Scalars['Boolean'];
  updateSalesJournalEntries?: Maybe<Scalars['Boolean']>;
};


export type MutationCreateProductArgs = {
  product?: Maybe<ProductInput>;
};


export type MutationUpdateProductArgs = {
  id: Scalars['Int'];
  version: Scalars['Int'];
  product?: Maybe<ProductInput>;
};


export type MutationDeleteProductArgs = {
  id: Scalars['Int'];
  version: Scalars['Int'];
};


export type MutationCreateProductVariantArgs = {
  product_id: Scalars['Int'];
  product_version: Scalars['Int'];
  variant: ProductVariantInput;
};


export type MutationUpdateProductVariantArgs = {
  product_id: Scalars['Int'];
  product_version: Scalars['Int'];
  variant_id: Scalars['Int'];
  variant: ProductVariantInput;
};


export type MutationDeleteProductVariantArgs = {
  product_id: Scalars['Int'];
  product_version: Scalars['Int'];
  variant_id: Scalars['Int'];
};


export type MutationUpdateProductCustomAttributesArgs = {
  product_id: Scalars['Int'];
  product_version: Scalars['Int'];
  attributes?: Maybe<Scalars['JSON']>;
};


export type MutationCreateProductAttributeArgs = {
  productAttribute: ProductAttributeInput;
};


export type MutationUpdateProductAttributeArgs = {
  id: Scalars['Int'];
  productAttribute: ProductAttributeInput;
};


export type MutationDeleteProductAttributeArgs = {
  id: Scalars['Int'];
};


export type MutationChangeProductAttributeValuePosistionArgs = {
  product_attribute_id: Scalars['Int'];
  product_attribute_value_id: Scalars['Int'];
  position: Scalars['Int'];
};


export type MutationUpdatePastequeProductArgs = {
  product_id: Scalars['Int'];
  product_version: Scalars['Int'];
  pasteque_product_id: Scalars['Int'];
  pasteque_product: PastequeProductInput;
};


export type MutationCreatePastequeProductArgs = {
  product_id: Scalars['Int'];
  product_version: Scalars['Int'];
  pasteque_product: PastequeProductInput;
};


export type MutationCreateReceiptArgs = {
  receipt: ReceiptInput;
};


export type MutationUpdateReceiptArgs = {
  id: Scalars['Int'];
  version: Scalars['Int'];
  receipt: ReceiptInput;
};


export type MutationDeleteReceiptArgs = {
  id: Scalars['Int'];
  version: Scalars['Int'];
};


export type MutationCreateInventoryArgs = {
  inventory: InventoryInput;
};


export type MutationUpdateInventoryArgs = {
  id: Scalars['Int'];
  version: Scalars['Int'];
  inventory: InventoryInput;
};


export type MutationDeleteInventoryArgs = {
  id: Scalars['Int'];
  version: Scalars['Int'];
};


export type MutationCreateReportArgs = {
  report: ReportInput;
};


export type MutationUpdateReportArgs = {
  id: Scalars['Int'];
  version: Scalars['Int'];
  report: ReportInput;
};


export type MutationDeleteReportArgs = {
  id: Scalars['Int'];
  version: Scalars['Int'];
};


export type MutationCreateStockArgs = {
  stock: StockInput;
};


export type MutationUpdateStockArgs = {
  id: Scalars['Int'];
  version: Scalars['Int'];
  stock: StockInput;
};


export type MutationDeleteStockArgs = {
  id: Scalars['Int'];
  version: Scalars['Int'];
};


export type MutationCreateStockItemsArgs = {
  stock_id: Scalars['Int'];
  product_ids: Array<Scalars['Int']>;
};


export type MutationUpdateStockItemArgs = {
  id: Scalars['Int'];
  item: StockItemInput;
};


export type MutationRemoveStockItemsArgs = {
  ids: Array<Scalars['Int']>;
};


export type MutationUpdateStockItemQuantitiesArgs = {
  stock_item_id: Scalars['Int'];
  stock_item_quantities: Array<StockItemQuantityInput>;
  adjustmentComment?: Maybe<Scalars['String']>;
};


export type MutationCreateStockCountArgs = {
  stock_id: Scalars['Int'];
  count: StockCountInput;
};


export type MutationUpdateStockCountArgs = {
  id: Scalars['Int'];
  count: StockCountInput;
};


export type MutationDeleteStockCountArgs = {
  id: Scalars['Int'];
};


export type MutationUpdateStockCountItemsArgs = {
  stock_count_id: Scalars['Int'];
  counts: Array<StockCountItemInput>;
};


export type MutationTransferStockCountArgs = {
  stock_count_id: Scalars['Int'];
};


export type MutationCreateStockMovementArgs = {
  stock_id: Scalars['Int'];
  movement: StockMovementInput;
};


export type MutationUpdateStockMovementArgs = {
  id: Scalars['Int'];
  movement: StockMovementInput;
};


export type MutationDeleteStockMovementArgs = {
  id: Scalars['Int'];
};


export type MutationUpdateStockMovementItemsArgs = {
  stock_movement_id: Scalars['Int'];
  items: Array<StockMovementItemInput>;
};


export type MutationDeleteStockMovementItemArgs = {
  stock_movement_id: Scalars['Int'];
  stock_movement_item_id: Scalars['Int'];
};


export type MutationExecuteStockMovementArgs = {
  stock_movement_id: Scalars['Int'];
};


export type MutationUpdateSalesJournalEntriesArgs = {
  sales_journal_code: Scalars['String'];
  entries: Array<SalesJournalEntryInput>;
};

export type PastequeProduct = {
  __typename?: 'PastequeProduct';
  id: Scalars['Int'];
  reference?: Maybe<Scalars['String']>;
  label?: Maybe<Scalars['String']>;
  barcode?: Maybe<Scalars['String']>;
  pricesell?: Maybe<Scalars['Float']>;
  visible?: Maybe<Scalars['Boolean']>;
  category?: Maybe<Scalars['String']>;
  tax_rate?: Maybe<Scalars['Float']>;
  tax?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
};

export type PastequeProductInput = {
  id?: Maybe<Scalars['Int']>;
  barcode?: Maybe<Scalars['String']>;
  pricesell?: Maybe<Scalars['Float']>;
  category: Scalars['String'];
  tax_rate: Scalars['Float'];
  tax_id: Scalars['Int'];
  tax?: Maybe<Scalars['String']>;
  price: Scalars['Float'];
  number: Scalars['Int'];
  name: Scalars['String'];
  active: Scalars['Boolean'];
};

export type PaymentMethod = {
  __typename?: 'PaymentMethod';
  id: Scalars['Int'];
  name: Scalars['String'];
  code: Scalars['String'];
  display_name?: Maybe<Scalars['String']>;
  pasteque_id?: Maybe<Scalars['Int']>;
  pasteque_reference?: Maybe<Scalars['String']>;
  barcode?: Maybe<Scalars['String']>;
  polimod?: Maybe<Scalars['String']>;
};

export type Product = {
  __typename?: 'Product';
  id: Scalars['Int'];
  name: Scalars['String'];
  label?: Maybe<Scalars['String']>;
  sku?: Maybe<Scalars['String']>;
  number: Scalars['Int'];
  barcode?: Maybe<Scalars['String']>;
  criteria?: Maybe<Scalars['String']>;
  price: Scalars['Float'];
  category_code?: Maybe<Scalars['ID']>;
  supplier?: Maybe<Scalars['ID']>;
  active?: Maybe<Scalars['Boolean']>;
  variant_count?: Maybe<Scalars['Int']>;
  pasteque_product_id?: Maybe<Scalars['Int']>;
  has_pasteque_product?: Maybe<Scalars['Boolean']>;
  version?: Maybe<Scalars['Int']>;
  import_date?: Maybe<Scalars['Date']>;
  attributes?: Maybe<Scalars['JSON']>;
  category: Category;
  pasteque?: Maybe<PastequeProduct>;
  variants: Array<ProductVariant>;
  syncStats?: Maybe<Scalars['JSON']>;
};

export type ProductAttribute = {
  __typename?: 'ProductAttribute';
  id: Scalars['Int'];
  code?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  type: Scalars['String'];
  position: Scalars['Int'];
  values: Array<ProductAttributeValue>;
};

export type ProductAttributeInput = {
  id?: Maybe<Scalars['Int']>;
  code?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  type: Scalars['String'];
  values: Array<ProductAttributeValueInput>;
};

export type ProductAttributeValue = {
  __typename?: 'ProductAttributeValue';
  id: Scalars['Int'];
  value?: Maybe<Scalars['String']>;
  code: Scalars['String'];
  color?: Maybe<Scalars['String']>;
};

export type ProductAttributeValueInput = {
  id?: Maybe<Scalars['Int']>;
  value?: Maybe<Scalars['String']>;
  code: Scalars['String'];
  color?: Maybe<Scalars['String']>;
};

export type ProductInput = {
  name: Scalars['String'];
  label?: Maybe<Scalars['String']>;
  sku?: Maybe<Scalars['String']>;
  number: Scalars['Int'];
  barcode?: Maybe<Scalars['String']>;
  criteria?: Maybe<Scalars['String']>;
  price: Scalars['Float'];
  category_code?: Maybe<Scalars['ID']>;
  supplier?: Maybe<Scalars['ID']>;
  active?: Maybe<Scalars['Boolean']>;
  pasteque_product_id?: Maybe<Scalars['Int']>;
};

export type ProductVariant = {
  __typename?: 'ProductVariant';
  id: Scalars['Int'];
  name?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
  sku?: Maybe<Scalars['String']>;
  values: Array<ProductVariantAttributeValue>;
};

export type ProductVariantAttributeValue = {
  __typename?: 'ProductVariantAttributeValue';
  id: Scalars['Int'];
  attribute: ProductAttribute;
  value: ProductAttributeValue;
};

export type ProductVariantAttributeValueInput = {
  id?: Maybe<Scalars['Int']>;
  attribute_id: Scalars['Int'];
  value_id: Scalars['Int'];
};

export type ProductVariantInput = {
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
  sku?: Maybe<Scalars['String']>;
  values: Array<ProductVariantAttributeValueInput>;
};

export type Query = {
  __typename?: 'Query';
  product?: Maybe<Product>;
  products: Array<Product>;
  unsynchronizedProducts: Array<Product>;
  missingPastequeProducts: Array<PastequeProduct>;
  productAttributes: Array<ProductAttribute>;
  productAttribute?: Maybe<ProductAttribute>;
  category?: Maybe<Category>;
  categories: Array<Category>;
  ticket?: Maybe<Ticket>;
  tickets: Array<Ticket>;
  ticketStats: Array<TicketStats>;
  receipts: Array<Receipt>;
  receipt?: Maybe<Receipt>;
  inventories: Array<Inventory>;
  inventory?: Maybe<Inventory>;
  reports: Array<Report>;
  report?: Maybe<Report>;
  salesJournal?: Maybe<SalesJournal>;
  historyChanges: Array<HistoryChange>;
  paymentMethods: Array<PaymentMethod>;
  stock?: Maybe<Stock>;
  stocks: Array<Stock>;
  stockItem?: Maybe<StockItem>;
  stockCounts: Array<StockCount>;
  stockCount?: Maybe<StockCount>;
  stockMovements: Array<StockMovement>;
  stockMovement?: Maybe<StockMovement>;
  configurationSettings: Array<ConfigurationSetting>;
  webshopOrdersUrl?: Maybe<Scalars['String']>;
};


export type QueryProductArgs = {
  id: Scalars['Int'];
};


export type QueryProductAttributeArgs = {
  id: Scalars['Int'];
};


export type QueryCategoryArgs = {
  code: Scalars['String'];
};


export type QueryTicketArgs = {
  id: Scalars['Int'];
};


export type QueryTicketsArgs = {
  date: Scalars['Date'];
};


export type QueryTicketStatsArgs = {
  year: Scalars['Int'];
  month: Scalars['Int'];
};


export type QueryReceiptArgs = {
  id: Scalars['Int'];
};


export type QueryInventoryArgs = {
  id: Scalars['Int'];
};


export type QueryReportArgs = {
  id: Scalars['Int'];
};


export type QuerySalesJournalArgs = {
  code: Scalars['String'];
};


export type QueryHistoryChangesArgs = {
  entity: Scalars['String'];
  id: Scalars['String'];
};


export type QueryStockArgs = {
  id: Scalars['Int'];
};


export type QueryStockItemArgs = {
  id: Scalars['Int'];
};


export type QueryStockCountsArgs = {
  stock_id: Scalars['Int'];
};


export type QueryStockCountArgs = {
  id: Scalars['Int'];
};


export type QueryStockMovementsArgs = {
  stock_id: Scalars['Int'];
};


export type QueryStockMovementArgs = {
  id: Scalars['Int'];
};


export type QueryConfigurationSettingsArgs = {
  keys?: Maybe<Array<Scalars['String']>>;
};

export type Receipt = {
  __typename?: 'Receipt';
  id: Scalars['Int'];
  version: Scalars['Int'];
  ticket_id?: Maybe<Scalars['Int']>;
  customer?: Maybe<Customer>;
  date?: Maybe<Scalars['Date']>;
  number?: Maybe<Scalars['String']>;
  payment_details?: Maybe<Scalars['String']>;
  note?: Maybe<Scalars['String']>;
  total?: Maybe<Scalars['Float']>;
  ticket?: Maybe<Scalars['JSON']>;
};

export type ReceiptInput = {
  id?: Maybe<Scalars['Int']>;
  version?: Maybe<Scalars['Int']>;
  ticket_id?: Maybe<Scalars['Int']>;
  customer: CustomerInput;
  date: Scalars['Date'];
  number: Scalars['String'];
  payment_details?: Maybe<Scalars['String']>;
  note?: Maybe<Scalars['String']>;
  ticket?: Maybe<Scalars['JSON']>;
};

export type Report = {
  __typename?: 'Report';
  id: Scalars['Int'];
  version: Scalars['Int'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  datepicker?: Maybe<Scalars['String']>;
  sql?: Maybe<Scalars['String']>;
  template?: Maybe<Scalars['String']>;
};

export type ReportInput = {
  id?: Maybe<Scalars['Int']>;
  version?: Maybe<Scalars['Int']>;
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  datepicker?: Maybe<Scalars['String']>;
  sql?: Maybe<Scalars['String']>;
  template?: Maybe<Scalars['String']>;
};

export type SalesJournal = {
  __typename?: 'SalesJournal';
  id: Scalars['Int'];
  name: Scalars['String'];
  code: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  entries: Array<SalesJournalEntry>;
};


export type SalesJournalEntriesArgs = {
  inventory_id: Scalars['Int'];
};

export type SalesJournalEntry = {
  __typename?: 'SalesJournalEntry';
  date?: Maybe<Scalars['DateOnly']>;
  method?: Maybe<Scalars['String']>;
  closed: Scalars['Boolean'];
  amount: Scalars['Float'];
  pasteque_amount: Scalars['Float'];
  pasteque_session_amount: Scalars['Float'];
  diff: Scalars['Float'];
};

export type SalesJournalEntryInput = {
  date: Scalars['DateOnly'];
  method: Scalars['String'];
  amount?: Maybe<Scalars['Float']>;
};

export type Stock = {
  __typename?: 'Stock';
  id: Scalars['Int'];
  version: Scalars['Int'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  product_count: Scalars['Int'];
  items: Array<StockItem>;
};

export type StockAdjustment = {
  __typename?: 'StockAdjustment';
  id: Scalars['Float'];
  stock_item: StockItem;
  stock_item_id: Scalars['Int'];
  stock_item_variant?: Maybe<StockItemVariant>;
  stock_item_variant_id?: Maybe<Scalars['Int']>;
  variant_id?: Maybe<Scalars['Int']>;
  variant?: Maybe<ProductVariant>;
  product_id?: Maybe<Scalars['Int']>;
  product?: Maybe<Product>;
  comment?: Maybe<Scalars['String']>;
  adjustment: Scalars['Float'];
  old_quantity: Scalars['Float'];
  new_quantity: Scalars['Float'];
  reference?: Maybe<Scalars['String']>;
  operation: StockOperation;
};

export type StockCount = {
  __typename?: 'StockCount';
  id: Scalars['Int'];
  name: Scalars['String'];
  date: Scalars['DateOnly'];
  inventory?: Maybe<Inventory>;
  inventory_id?: Maybe<Scalars['Int']>;
  stock: Stock;
  stock_id: Scalars['Int'];
  comment?: Maybe<Scalars['String']>;
  items: Array<StockCountItem>;
  validated: Scalars['Boolean'];
  transferred?: Maybe<Scalars['Date']>;
};

export type StockCountInput = {
  id?: Maybe<Scalars['Int']>;
  name: Scalars['String'];
  date: Scalars['DateOnly'];
  inventory_id?: Maybe<Scalars['Int']>;
  comment?: Maybe<Scalars['String']>;
  validated: Scalars['Boolean'];
};

export type StockCountItem = {
  __typename?: 'StockCountItem';
  id?: Maybe<Scalars['Int']>;
  comment?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['Date']>;
  stock: Stock;
  stock_id: Scalars['Int'];
  count?: Maybe<StockCount>;
  stock_item: StockItem;
  stock_item_id: Scalars['Int'];
  stock_item_variant?: Maybe<StockItemVariant>;
  stock_item_variant_id?: Maybe<Scalars['Int']>;
  quantity?: Maybe<Scalars['Float']>;
};

export type StockCountItemInput = {
  id?: Maybe<Scalars['Int']>;
  stock_item_id: Scalars['Int'];
  stock_item_variant_id?: Maybe<Scalars['Int']>;
  comment?: Maybe<Scalars['String']>;
  quantity: Scalars['Float'];
};

export type StockInput = {
  id?: Maybe<Scalars['Int']>;
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
};

export type StockItem = {
  __typename?: 'StockItem';
  id: Scalars['Int'];
  product_id?: Maybe<Scalars['Int']>;
  quantity: Scalars['Float'];
  notes?: Maybe<Scalars['String']>;
  product?: Maybe<Product>;
  variant_count: Scalars['Int'];
  variants: Array<StockItemVariant>;
  adjustments: Array<StockAdjustment>;
};

export type StockItemInput = {
  id?: Maybe<Scalars['Int']>;
  notes?: Maybe<Scalars['String']>;
  variants: Array<StockItemVariantInput>;
};

export type StockItemQuantityInput = {
  variant_id?: Maybe<Scalars['Int']>;
  quantity: Scalars['Float'];
};

export type StockItemVariant = {
  __typename?: 'StockItemVariant';
  id?: Maybe<Scalars['Int']>;
  variant_id: Scalars['Int'];
  variant: ProductVariant;
  quantity: Scalars['Float'];
  notes?: Maybe<Scalars['String']>;
};

export type StockItemVariantInput = {
  variant_id: Scalars['Int'];
  notes?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
};

export type StockMovement = {
  __typename?: 'StockMovement';
  id: Scalars['Int'];
  name: Scalars['String'];
  date: Scalars['Date'];
  stock: Stock;
  stock_id: Scalars['Int'];
  destination_stock: Stock;
  destination_stock_id: Scalars['Int'];
  comment?: Maybe<Scalars['String']>;
  items: Array<StockMovementItem>;
  operation?: Maybe<StockOperation>;
  destination_operation?: Maybe<StockOperation>;
  executed?: Maybe<Scalars['Date']>;
};

export type StockMovementInput = {
  id?: Maybe<Scalars['Int']>;
  name: Scalars['String'];
  date: Scalars['Date'];
  comment?: Maybe<Scalars['String']>;
  destination_stock_id: Scalars['Int'];
};

export type StockMovementItem = {
  __typename?: 'StockMovementItem';
  id?: Maybe<Scalars['Int']>;
  comment?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['Date']>;
  stock: Stock;
  stock_id: Scalars['Int'];
  count?: Maybe<StockMovement>;
  stock_item: StockItem;
  stock_item_id: Scalars['Int'];
  stock_item_variant?: Maybe<StockItemVariant>;
  stock_item_variant_id?: Maybe<Scalars['Int']>;
  quantity: Scalars['Float'];
  destination_reference?: Maybe<StockReference>;
};

export type StockMovementItemInput = {
  id?: Maybe<Scalars['Int']>;
  stock_item_id: Scalars['Int'];
  stock_item_variant_id?: Maybe<Scalars['Int']>;
  comment?: Maybe<Scalars['String']>;
  quantity: Scalars['Float'];
};

export type StockOperation = {
  __typename?: 'StockOperation';
  id: Scalars['Int'];
  stock_id: Scalars['Int'];
  stock: Stock;
  type: Scalars['String'];
  name?: Maybe<Scalars['String']>;
  date: Scalars['Date'];
  comment?: Maybe<Scalars['String']>;
  adjustments: Array<StockAdjustment>;
  created_by: Scalars['String'];
  reference?: Maybe<Scalars['String']>;
};

export type StockReference = {
  __typename?: 'StockReference';
  stock_id: Scalars['Int'];
  stock_item_id: Scalars['Int'];
  stock_item_variant_id?: Maybe<Scalars['Int']>;
  quantity?: Maybe<Scalars['Float']>;
};

export type Ticket = {
  __typename?: 'Ticket';
  id?: Maybe<Scalars['Int']>;
  number?: Maybe<Scalars['Int']>;
  date?: Maybe<Scalars['Date']>;
  total?: Maybe<Scalars['Float']>;
  total_ht?: Maybe<Scalars['Float']>;
  finalprice?: Maybe<Scalars['Float']>;
  cash_register?: Maybe<Scalars['String']>;
  items?: Maybe<Scalars['Float']>;
  taxes: Array<TicketTaxes>;
  payment_mode: Scalars['String'];
  lines?: Maybe<Array<Maybe<TicketLine>>>;
  rawTicket?: Maybe<Scalars['JSON']>;
};

export type TicketLine = {
  __typename?: 'TicketLine';
  product_id?: Maybe<Scalars['Int']>;
  tax_id?: Maybe<Scalars['Int']>;
  disporder?: Maybe<Scalars['Int']>;
  productlabel?: Maybe<Scalars['String']>;
  unitprice?: Maybe<Scalars['Float']>;
  taxedunitprice?: Maybe<Scalars['Float']>;
  quantity?: Maybe<Scalars['Float']>;
  price?: Maybe<Scalars['Float']>;
  taxedprice?: Maybe<Scalars['Float']>;
  taxrate?: Maybe<Scalars['Float']>;
  discountrate?: Maybe<Scalars['Float']>;
  finalprice?: Maybe<Scalars['Float']>;
  finaltaxedprice?: Maybe<Scalars['Float']>;
};

export type TicketStats = {
  __typename?: 'TicketStats';
  date: Scalars['String'];
  count: Scalars['Int'];
  cash_registers: Scalars['Int'];
};

export type TicketTaxes = {
  __typename?: 'TicketTaxes';
  tax_id: Scalars['Int'];
  tax_label?: Maybe<Scalars['String']>;
  taxrate?: Maybe<Scalars['Float']>;
  base?: Maybe<Scalars['Float']>;
  amount?: Maybe<Scalars['Float']>;
};

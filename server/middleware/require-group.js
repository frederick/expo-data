function requireGroup(groups, either) {
  if (!Array.isArray(groups)) {
    groups = [groups]
  }
  var fun = groups.every
  if (either) {
    fun = groups.some
  }

  return (req, res, next) => {
    if (fun.call(groups, g => req.groups && req.groups.has(g))) {
      return next()
    } else {

      res.status(400).json({
        "reason": "forbidden",
        "message": "You do not have access to that feature."
      })
    }
  }
}

let reqAdmin = requireGroup(['admin', 'super-admin'], true)
let reqUser = requireGroup(['user', 'admin', 'super-admin'], true)
let reqRW = requireGroup('rw')

module.exports = { reqAdmin, reqUser, requireGroup, factory: requireGroup, reqRW }
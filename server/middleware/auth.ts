import type Request from "../types/Request";

const { factory } = require('./authware')
const auth = factory({
  secret: process.env.JWT_SECRET,
  app: 'expo-data',
  allowAnonymous: true
})
export class AccessError extends Error {
  reason = null;
  name = 'AccessError'
  constructor({ reason, message }) {
    super(message)
    this.reason = reason;
  }
}

module.exports = async (req: Request, res, next) => {
  return await auth(req, res, () => {
    const groups = Array.from(req.token.groups || [])
    if (groups.includes('admin') || groups.includes('super-admin')) {
      // do role mapping for admin
      groups.push(...['rw','user', 'reports', 'pasteque', 'tickets', 'receipts', 'inventory', 'accounting', 'products'])
    } else if (groups.includes('user')) {
      // read only access
      // groups.push(...['reports', 'tickets', 'receipts', 'inventory', 'accounting', 'products'])
    } else if (groups.includes('receipts')) {
      // receipts also needs tickets access
      groups.push('tickets')
    }
    req.groups = new Set(groups);

    req.requireGroups = function (groups, either = false) {
      if (!Array.isArray(groups)) {
        groups = [groups]
      }
      var fun = groups.every
      if (either) {
        fun = groups.some
      }
      if (fun.call(groups, g => req.groups && req.groups.has(g))) {
        return;
      } else {
        throw new AccessError({
          "reason": "forbidden",
          "message": "You do not have access to that feature."
        })
      }
    };

    return next();

  })
}
const Joi = require('@hapi/joi')
function validate({ schema, querySchema }) {
  return function (req, res, next) {
    try {
      if (schema) {
        req.body = Joi.attempt(req.body || {}, schema)
      } else if (querySchema) {
        req.query = Joi.attempt(req.query || {}, querySchema)
      }
      next()
    } catch (err) {
      if (err.isJoi) {
        res.status(400).json({
          message: err.details && err.details.map(e => e.message).join(', '),
          title: 'Validation error'
        })
      } else {
        throw err
      }
    }
  }
}

module.exports = validate
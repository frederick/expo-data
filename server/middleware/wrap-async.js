function wrapAsync(f) {
  return function (req, res, next) {
    var result = f(req, res, next);
    if (result && typeof result.then === "function") {
      return result.then(undefined, function (err) {
        return next(
          err || new Error("Promise was rejected with a falsy value")
        );
      });
    } else {
      return result;
    }
  };
}

var { createClient } = require("./../pgclient");

function wrapAsyncWithDb(f, { db } = {}) {
  return async function (req, res, next) {

    try {
      let { client } = createClient({
        database: db
      });
      await client.connect();
      var username = req.token && req.token.sub || '?unknown'
      await client.query(`SET SESSION app.username=${client.escapeLiteral(username)};`)

      client._end = client.end
      client.ended = false
      client.end = async function (cb) {
        if (!client.ended) {
          client.ended = true
          await client._end()
        }
        if (cb) {
          cb()
        }
      }
      req.pg = client;


      var result = await f(req, res, next, req.pg);

      return result;
    } catch (e) {
      console.error(e);
      if (e.code === "28P01") {
        next({
          status: 401,
          message: "Authentication failed",
          cause: e
        });
        return;
      }
      if (e.code === "23505") {
        // unique constraint failed
        next({
          status: 409,
          message: "Value already taken",
          cause: e
        });
      } else
        return next(e || new Error("Promise was rejected with a falsy value"));
    } finally {
      if (req.pg) {
        try {
          if (Array.isArray(req.pg)) {
            await Promise.all(req.pg.map(p => p.end()))
          } else {
            await req.pg.end();
          }

        } catch (e) {
        }
      }
    }
  };
}

module.exports = { wrapAsync, wrapAsyncWithDb };

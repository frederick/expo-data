const { verify, decode, sign } = require("jsonwebtoken");
const fetch = require('node-fetch')
var crypto = require('crypto');

const algorithms = ["HS256", "RS256"];
const headerRE = /^Bearer\s+(.+)$/;

const public_url = process.env.WEBAUTHD_PUBLIC_URL;
const api_url = process.env.WEBAUTHD_API_URL;

const mode_algorithms = {
  'shared_secret': 'HS256',
  'public_key': 'RS256'
}
class BlacklistedTokenError extends Error {
  constructor(message) {
    super(message)
    this.name = this.constructor.name
  }
}
class WrongTokenTypeError extends Error {
  constructor(message) {
    super(message)
    this.name = this.constructor.name
  }
}

class NoJWTAuthError extends Error {
  constructor(message) {
    super(message)
    this.name = this.constructor.name
  }
}

// ⚠️ global state
const syncApps = new Map()

async function sync() {
  for (let appConfig of syncApps.values()) {
    try {
      var token = sign({
        'type': 'access'
      }, appConfig.secret, {
        audience: appConfig.app,
        expiresIn: '3m',
        issuer: 'webauthd',
        subject: appConfig.app,
        // here we use the secret (even if a public key) as secret for communication between webauthd and this app
        algorithm: mode_algorithms['shared_secret']
      })
      const headers = {
        authorization: `Bearer ${token}`,
      }

      if (appConfig.config && appConfig.config.url)
        headers.origin = appConfig.config.url

      var result = await fetch(`${api_url}/application/${appConfig.app}/sync`, {
        method: 'GET',
        headers
      })
      if (result.status !== 200) {
        throw new Error(result.status)
      }
      result = await result.json()
      appConfig.config = result
      console.log(`Synced ${appConfig.app}`)
    } catch (e) {
      console.log(`Syncing of ${appConfig.app} failed (${e})`)
    }
  }
}
setInterval(sync, 60 * 1000)
//initial after 5s after startup
setTimeout(sync, 1000)
function handleTokenError(res, e) {
  console.log(e)
  return res.status(403).json({
    status: 403,
    message: "Invalid token",
    login: public_url,
    error: "TokenExpiredError"
  });
}
function factory({ secret, secretPromise, mode = 'shared_secret', app, allowRefresh, allowAnonymous, sync }) {
  if (secretPromise && !secret) {
    secretPromise.then(s => {
      secret = s
    });
  } else {
    secretPromise = Promise.resolve(secret)
  }

  async function authware(req, res, next) {
    await secretPromise
    var e = req.url.split("/").pop();
    if (e === "refresh-token") {
      return res.json({
        refresh: api_url && `${api_url}/${app}`,
        login: public_url,
      });
    }


    try {
      let header = req.get("Authorization");
      if (!header && allowAnonymous) {
        req.token = { anonymous: true };
        return next();
      }
      const { decoded, token } = decodeToken(header, app, secret, mode);
      var sa = syncApps.get(app)
      if (sa && sa.config) {
        req.authwareConfig = sa.config
      }


      if (decoded.type !== "access" && (!allowRefresh || decoded.type !== "refresh")) {
        // only decode access tokens in MW
        throw new WrongTokenTypeError(`Wrong token type, ${decoded.type}`);
      }

      req.token = decoded;
      return next();
    } catch (e) {
      return handleTokenError(res, e)
    }
  };
  if (sync) {

    secretPromise.then(s => {
      syncApps.set(app, { app, secret: s, mode, config: null })
    })
  }
  return authware;
}

function readTokenHeader(req) {
  const reres = headerRE.exec(req.get('authorization'));
  if (reres) {
    const token = reres[1];
    return token
  }
}

function decodeToken(header, app, secret, mode = 'shared_secret') {
  if (!secret || secret.length < 127) {
    throw new NoJWTAuthError("NO_JWT_AUTH");
  }
  const alg = mode_algorithms[mode]
  if (!alg) {
    throw new Error("No algorithm for mode " + mode)
  }
  const reres = headerRE.exec(header);
  if (reres) {
    const token = reres[1];
    var sa = syncApps.get(app)
    if (sa && sa.config) {
      const { blacklisted } = sa.config
      if (blacklisted) {
        var tokenHash = crypto.createHash('md5').update(token).digest("hex");
        if (blacklisted.includes(tokenHash)) {
          throw new BlacklistedTokenError('Blacklisted')
        }
      }
    }
    const decoded = verify(token, secret, {
      algorithms: alg,
      audience: app,
      issuer: "webauthd"
    });
    return { decoded, token };
  } else {
    throw new Error("Invalid token header.");
  }
}

module.exports = { handleTokenError, decodeToken, factory, algorithms, readTokenHeader };

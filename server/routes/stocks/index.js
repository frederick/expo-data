var { wrap, wrapAsyncWithDb } = require("../../middleware/wrap-async");
const { NOT_FOUND } = require("../../utils/responses");
const { reqAdmin, reqRW, factory } = require('../../middleware/require-group')
const validate = require('../../middleware/joi')

const Router = require("express").Router;

const r = Router({});
r.get(
  "/",
  wrapAsyncWithDb(async function (req, res, next) {
    var stocks = await req.pg.query(
      `
        SELECT 
        s.id,s.name,s.description, count(sp.id)::int AS product_count
        FROM stock s LEFT JOIN stock_product sp ON sp.stock_id=s.id
        GROUP BY s.id
        ORDER BY s.name ASC;
    `
    );
    stocks = stocks.rows;

    res.json(stocks);
  })
);

r.use(factory('inventory'))

r.get(
  "/:stock",
  wrapAsyncWithDb(async function (req, res, next) {
    var data = await req.pg.query(
      `
          SELECT 
          s.id,s.name,s.description
          FROM stock s
          WHERE id=$1
          ;
        `,
      [req.params.stock]
    );
    if (data.rows.length === 1) {
      const stock = data.rows[0];

      res.json(stock);
    } else {
      res.status(404).json(NOT_FOUND);
    }
  })
);


r.post(
  "/",
  validate({ schema: require('../../schemas/stocks').createStock }),
  reqAdmin,
  wrapAsyncWithDb(async function (req, res, next) {
    var stock = req.body;
    var data = await req.pg.query(
      `
          INSERT INTO stock(name,description)
          VALUES ($1,$2)
          RETURNING id
          ;
        `,
      [stock.name, stock.description]
    );
    res.json(data.rows[0].id);
  })
);

r.put(
  "/:stock",
  reqAdmin,
  validate({ schema: require('../../schemas/stocks').updateStock }),
  wrapAsyncWithDb(async function (req, res, next) {
    var stock = req.body;
    await req.pg.query('BEGIN')
    await req.pg.query(
      `UPDATE stock
          SET name=$1,
          description=$2
          WHERE id=$3
          ;
        `,
      [stock.name, stock.description, req.params.stock]
    );
    await req.pg.query('COMMIT;')
    res.json(req.params.stock);
  })
);

r.delete(
  "/:stock",
  reqAdmin,
  wrapAsyncWithDb(async function (req, res, next) {
    var stock = req.body;
    await req.pg.query(
      `DELETE FROM stock WHERE id=$1;`,
      [req.params.stock]
    );
    res.json(req.params.stock);
  })
);

r.get(
  "/:stock/products",
  wrapAsyncWithDb(async function (req, res, next) {
    var data = await req.pg.query(`
      SELECT sp.id,sp.quantity,sp.notes,sp.product_id,p.number,p.name,p.barcode,p.criteria,p.price,p.category_code, p.active, p.tax_rate, p.category,
      json_agg(json_build_object('id',pc.id,'variant_id',pv.variant_id,'name',pv.variant_name,'av',pv.attribute_values->0,'quantity',COALESCE(pc.quantity,0)) ORDER BY COALESCE(pv.variant_name,'')) as variants
      FROM stock_product sp 
      LEFT JOIN vw_products p ON sp.product_id=p.id
      LEFT JOIN vw_product_variants pv ON pv.product_id=p.id
      LEFT JOIN product_count pc ON pc.counting_list_id IS NULL AND pc.product_id=p.id AND pc.variant_id=pv.variant_id AND pc.stock_id=$1
      WHERE sp.stock_id=$1
      GROUP BY sp.id,p.number,p.name,p.barcode,p.criteria,p.price,p.category_code, p.active, p.tax_rate, p.category
      ORDER BY p.number,p.name
      `, [req.params.stock])
    res.json(data.rows);

  })
);


r.get(
  "/:stock/products/:stockProduct",
  wrapAsyncWithDb(async function (req, res, next) {
    var data = await req.pg.query(`
      SELECT sp.id,sp.quantity,sp.notes,sp.product_id,p.number,p.name,p.barcode,p.criteria,p.price,p.category_code, p.active, p.tax_rate, p.category,
      json_agg(json_build_object('id',pc.id,'variant_id',pv.variant_id,'name',pv.variant_name,'av',pv.attribute_values->0,'quantity',COALESCE(pc.quantity,0)) ORDER BY COALESCE(pv.variant_name,'')) as variants
      FROM stock_product sp 
      LEFT JOIN vw_products p ON sp.product_id=p.id
      LEFT JOIN vw_product_variants pv ON pv.product_id=p.id
      LEFT JOIN product_count pc ON pc.counting_list_id IS NULL AND pc.product_id=p.id AND pc.variant_id=pv.variant_id AND pc.stock_id=$1
      WHERE sp.stock_id=$1 AND sp.id=$2
      GROUP BY sp.id,p.number,p.name,p.barcode,p.criteria,p.price,p.category_code, p.active, p.tax_rate, p.category
      `, [req.params.stock, req.params.stockProduct])
    res.json(data.rows[0]);

  })
);

r.put(
  "/:stock/products",
  validate({ schema: require('../../schemas/stocks').products }),
  wrapAsyncWithDb(async function (req, res, next) {
    var stockId = parseInt(req.params.stock)
    await req.pg.query(
      `WITH source AS (SELECT * FROM json_to_recordset($2) AS j(id int4, quantity numeric,notes text))
        UPDATE stock_product sp SET quantity=j.quantity, notes=j.notes
        FROM source as j
        WHERE sp.stock_id=$1 AND sp.id=j.id`,
      [stockId, JSON.stringify(req.body)])
    res.json({ ok: true })
  })
)


r.post(
  "/:stock/products",
  validate({ schema: require('../../schemas/stocks').product_ids }),
  reqAdmin,
  wrapAsyncWithDb(async function (req, res, next) {
    var productIds = req.body;
    var stockId = parseInt(req.params.stock)
    var data = await req.pg.query(
      `
          INSERT INTO stock_product(stock_id, product_id)
          SELECT $1,pid
          FROM UNNEST($2::int[]) pid
          ON CONFLICT DO NOTHING
          RETURNING id
          ;
        `,
      [stockId, productIds]
    );
    res.json(data.rows);
  })
);

r.delete(
  "/:stock/products",
  validate({ schema: require('../../schemas/stocks').product_ids }),
  reqAdmin,
  wrapAsyncWithDb(async function (req, res, next) {
    var productIds = req.body;
    var stockId = parseInt(req.params.stock)
    var data = await req.pg.query(
      `
          DELETE FROM stock_product
          WHERE stock_id=$1 AND id = ANY($2::int[])
          ;
        `,
      [stockId, productIds]
    );
    res.json(data.rows);
  })
);

r.use(require('./countingLists'))

module.exports = r

const Router = require("express").Router;
const validate = require('../../middleware/joi')
var { wrap, wrapAsyncWithDb } = require("../../middleware/wrap-async");
const { factory, reqRW } = require('../../middleware/require-group')

const reqCL = factory('inventory')

const r = Router({});

r.get(
    "/:stock/counting-lists",
    reqCL, reqRW,
    wrapAsyncWithDb(async function (req, res, next) {
        const { rows: lists } = await req.pg.query(`
        SELECT id,title,date,inventory_id,stock_id,comment,validated FROM counting_list WHERE stock_id=$1 ORDER BY date DESC
        `, [req.params.stock])
        res.json(lists)
    })
);
r.get(
    "/:stock/counting-lists/:list",
    reqCL, reqRW,
    wrapAsyncWithDb(async function (req, res, next) {
        const { rows: lists } = await req.pg.query(`
        SELECT c.id,c.title,c.date,c.inventory_id,c.stock_id,c.comment,c.validated,
        COALESCE(json_agg(pc)  FILTER (WHERE pc.id IS NOT NULL), '[]') as items
        FROM counting_list c
        LEFT JOIN product_count pc ON pc.counting_list_id=c.id
        WHERE c.stock_id=$1 AND c.id=$2
        GROUP BY c.id
        `, [req.params.stock, req.params.list]
        )
        if (lists[0])
            res.json(lists[0])
        else
            res.status(404).json({reason: 'Not found'})
    })
);

r.put(
    "/:stock/counting-lists/:list",
    validate({ schema: require('../../schemas/stocks').updateCountingList }),
    reqCL, reqRW,
    wrapAsyncWithDb(async function (req, res, next) {
        var list = req.body;
        var data = await req.pg.query(
            `UPDATE counting_list as nr
             SET title=j.title, date=j.date,inventory_id=j.inventory_id,comment=j.comment,validated=j.validated
             FROM  (SELECT * FROM json_populate_record(null::counting_list, $2)) j
             WHERE nr.stock_id=$1 AND nr.id=$3;
          `,
            [req.params.stock, list,req.params.list]
        );
        res.json(data.rows);
    })
);

r.post(
    "/:stock/counting-lists",
    validate({ schema: require('../../schemas/stocks').createCountingList }),
    reqCL, reqRW,
    wrapAsyncWithDb(async function (req, res, next) {
        var list = req.body;
        var data = await req.pg.query(
        `
        INSERT INTO counting_list
            (title, "date", inventory_id, stock_id, "comment", validated)
        VALUES($1, $2, $3, $4, $5, $6)
            RETURNING id;
          `,
            [list.title, list.date, list.inventory_id, req.params.stock, list.comment, list.validated]
        );
        res.json(data.rows[0].id);
    })
);

module.exports = r
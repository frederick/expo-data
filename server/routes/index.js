
const Router = require('express').Router;
const { reqAdmin, reqUser } = require('../middleware/require-group')
const { handleTokenError } = require('../middleware/authware')

var bodyparser = require("body-parser");

var r = Router()



r.use(bodyparser.json());
r.use(bodyparser.urlencoded({ extended: false }));


r.use(async (req, res, next) => {
  res.setHeader("Content-Type", "application/json");
  res.setHeader("Access-Control-Allow-Origin", "http://localhost:8080");
  res.notFound = function () {
    this.status(404).json({ reason: 'Object not found' })
  }
  next();
});

r.use('/reports', require('./run-report'))
r.use('/groups', (req, res) => {
  res.json(Array.from(req.groups))
})

r.use((req, res, next) => {
  if (!req.token || req.token.anonymous) {
    return handleTokenError(res, new Error('Authentication required'));
  }
  return next()
})


r.use('/accounting', reqUser, require('./accounting'))
r.use('/stocks', reqUser, require('./stocks'))
r.use('/webshop', reqUser, require('./webshop'))


r.use(require("../middleware/error/api-error-handler")());

r.use(function (req, res, next) {
  res.status(404);
  res.json({
    status: 404,
    message: "API not found"
  });
});

module.exports = r
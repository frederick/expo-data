var { wrap, wrapAsyncWithDb } = require("../middleware/wrap-async");
const { factory, reqRW } = require('../middleware/require-group')
const validate = require('../middleware/joi')


const Router = require('express').Router;

const r = Router({})


r.use(factory('accounting'))

r.post(
  "/analyze-transactions",
  reqRW,
  wrapAsyncWithDb(async function (req, res, next) {
    const { transactions } = req.body;
    const { combineTAListWithWebshopData } = require('../webshop/transactionParser')
    res.json(await combineTAListWithWebshopData(transactions))
  })
);

module.exports = r;
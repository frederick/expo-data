
const fs = require("fs");
const Router = require("express").Router;
const jwt = require('jsonwebtoken')
var { wrapAsyncWithDb } = require("../middleware/wrap-async");
var { handleTokenError } = require('../middleware/authware')

const bundleCss = fs.readFileSync(`data/bundle.css`).toString();
const reportTemplate = fs
  .readFileSync(`data/report-template.html`)
  .toString()
  .replace("$head", `<style>${bundleCss}</style>`);

const nunjucks = require("../utils/nunjucks");
const puppeteer = require("../utils/puppeteer");
const { stringify } = require("csv");

var r = Router();

r.all(
  "/:report/run",
  wrapAsyncWithDb(async function (req, res) {
    if (req.method !== "GET" && req.method !== "POST") {
      throw Error("Wrong method");
    }
    var encodedToken = req.get('authorization')
    if (encodedToken) {
      encodedToken = encodedToken.split(' ')[1].trim()
    } else {
      encodedToken = req.query.token || req.body && req.body.Authorization
    }
    try {

      jwt.verify(encodedToken, process.env.JWT_SECRET, {
        algorithms: 'HS256',
        audience: 'expo-data'
      })
    } catch (e) {
      return handleTokenError(res, e)
    }

    var mode = req.query.mode;
    var today = req.query.date && new Date(req.query.date) || new Date();
    if (req.query.inventory) {
      var invdates = await req.pg.query(
        `SELECT start_date::varchar as start_date, date::varchar as end_date, comments FROM vw_inventory_with_start_date
          WHERE id = $1
          ORDER BY date
          LIMIT 1;
        `,
        [req.query.inventory]
      );
    } else {
      var invdates = await req.pg.query(
        `SELECT start_date::varchar as start_date, date::varchar as end_date, comments FROM vw_inventory_with_start_date
          WHERE date >= $1
          ORDER BY date
          LIMIT 1;
        `,
        [today]
      );
    }

    invdates = invdates.rows[0];
    var report = await req.pg.query(`SELECT * FROM report WHERE id=$1;`, [
      req.params.report
    ]);

    if (report.rows.length > 0) {
      var rows;
      try {
        report = report.rows[0];
        var dateParams = {
          date: today.toISOString().substring(0, 10),
          inventoryStart: invdates.start_date,
          inventoryEnd: invdates.end_date,
          inventoryName: invdates.comments,
          reportName: report.name
        };
        const dependsOnInventory = /inventory((Start)|(End)|(Name))|/.test(report.sql)
        const title = `${report.name}${dependsOnInventory ? ` - ${invdates.comments}` : ''}`
        var sql = nunjucks.renderString(report.sql, dateParams);

        var data = await req.pg.query(sql);
        rows = data.rows;
        if (mode === "csv") {
          stringify(
            rows,
            {
              header: true,
              columns: data.fields.map(f => f.name)
            },
            (err, csv) => {
              if (err) {
                res.status(500);
                res.send(err);
                return
              }
              res.type("text/csv");
              res.set(
                "Content-disposition",
                `attachment; filename="${title}.csv"`
              );

              res.send(csv);
            }
          );
          return;
        }
        var output = nunjucks.renderString(report.template, {
          rows,
          ...dateParams
        });
        if (!mode || mode === "json") {
          res.json({
            output,
            report,
            rows
          });
        } else if (mode === "html" || mode === "pdf") {
          var out = reportTemplate
            .replace("$output", output)
            .replace("$title", title);

          if (mode === "html") {
            res.type("text/html");
            res.send(out);
          } else if (mode === "pdf") {
            const browser = await puppeteer.browser;
            const page = await browser.newPage();
            const fullUrl = 'http://127.0.0.1:3000/expo-data/api' + req.originalUrl;

            await page.setRequestInterception(true);
            page.on('request', (request) => {
              if (request.url().indexOf(req.fullUrl) === 0) {
                request.respond({
                  body: ''
                });
              } else {
                request.continue();
              }
              return;
            });
            await page.goto(fullUrl);
            //await page.goto(`data:text/html,${out}`, { waitUntil: 'networkidle0' });
            await page.setContent(out);
            var pdfData = await page.pdf({
              displayHeaderFooter: true,
              //xheaderTemplate: `<span style="font-size: 10px"><span class="date"></span> ${report.name} (${invdates.comments})</span>`,
              // xfooterTemplate: `<span style="font-size: 10px"> <span class="pageNumber"></span>/<span class="totalPages"></span></span>`,

            });

            res.type("application/pdf");
            res.set(
              "Content-disposition",
              `attachment; filename=${encodeURIComponent(title)}.pdf`
            );

            res.send(pdfData);
            page.close();

            //await browser.close();
          }
        }
      } catch (error) {
        console.error(error)
        res.json({
          output,
          report,
          rows: rows,
          output: `<h4>An error occurred</h4><b class="text-danger">${
            error.message
            }</b>`
        });
      }
    } else {
      res.status(404).json(NOT_FOUND);
    }
  })
);

module.exports = r
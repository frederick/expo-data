var { wrap, wrapAsyncWithDb } = require("../middleware/wrap-async");
const { factory, reqRW } = require('../middleware/require-group');
const validate = require('../middleware/joi');
const phpu = require('../utils/phpu');
const puppeteer = require('../utils/puppeteer');
const Router = require('express').Router;
const { exec } = require('child_process');
const { promisify } = require('util');
const execP = promisify(exec);

const r = Router({})


r.use(factory('webshop'));

r.get(
  "/orders",
  wrapAsyncWithDb(async function (req, res, next) {
    let status = req.query.status?.split(',') ?? ['processing'];

    const client = require('../webshop/client');
    const data = await client.query(`SELECT 
    o.entity_id, 
    o.increment_id as order_no,
    oa.firstname,
    oa.lastname,
    o.created_at,
    o.status,
    o.state,
    o.grand_total, 
    o.shipping_incl_tax  as shipping_amount,
    oa.firstname,
    oa.lastname,
    oa.telephone as customer_phone,
    o.customer_email,
    oa.street,
    oa.postcode, 
    oa.city, 
    GROUP_CONCAT(cpev.value separator '@@@' ) as product_names,
    GROUP_CONCAT(sfoi.sku separator '@@@' ) as product_skus,
    oa.country_id
    FROM sales_flat_order o 
    LEFT JOIN sales_flat_order_address oa ON oa.parent_id=o.entity_id AND oa.address_type='shipping'
    LEFT JOIN sales_flat_order_item sfoi ON sfoi.order_id=o.entity_id
    LEFT JOIN  catalog_product_entity cp  ON sfoi.product_id = cp.entity_id
    LEFT JOIN eav_entity_type eet ON eet.entity_type_code = 'catalog_product'
    LEFT JOIN eav_attribute ea ON ea.entity_type_id = eet.entity_type_id  AND ea.attribute_code = 'name'
    LEFT JOIN catalog_product_entity_varchar cpev ON cpev.attribute_id =ea.attribute_id  AND cpev.entity_type_id = eet.entity_type_id
        AND cpev.entity_id = cp.entity_id AND cpev.store_id =0
    
    WHERE o.status IN(?)
    GROUP BY o.entity_id
    ORDER BY o.created_at DESC LIMIT 1000`,[status])
    const orders = data.result;

    const sugar = await req.pg.query(`SELECT o.order_id,o.print_count,o.comment,og.name as order_group, og.color as order_color FROM webshop_order_meta o LEFT JOIN webshop_order_group og ON og.id=o.group_id WHERE o.order_id = ANY($1)`, [orders.map(d => d.entity_id)]);
    for (let s of sugar.rows) {
      const o = orders.find(o => o.entity_id == s.order_id);
      if (o) {
        Object.assign(o, s);
      }
    }

    res.json(orders);
  })
);
r.get(
  "/orders-groups",
  wrapAsyncWithDb(async function (req, res, next) {
    const { rows: groups } = await req.pg.query(`SELECT og.* FROM webshop_order_group og ORDER by og.name`, []);
    
    res.json(groups);
  })
);

r.post(
  "/orders/assign_group/:group",
  wrapAsyncWithDb(async function (req, res, next) {
    const group = req.params.group;
    const order_ids = (req.body.orders).map(oid => parseInt(oid));
    await req.pg.query(`
    INSERT INTO webshop_order_meta(order_id,group_id) 
      SELECT *,(SELECT id FROM webshop_order_group WHERE name=$2)
    FROM UNNEST ($1::varchar[]) ON CONFLICT (order_id) DO UPDATE SET group_id=(SELECT id FROM webshop_order_group WHERE name=$2);`, [order_ids,group]);
    res.json({ok:true});
  }
));
r.get(
  "/orders/:order/items",
  wrapAsyncWithDb(async function (req, res, next) {
    const order_id = req.params.order;
    const client = require('../webshop/client');
    const data = await client.query(`SELECT oi.name,oi.sku,oi.item_id,oi.qty_ordered,oi.price_incl_tax,oi.tax_percent,oi.tax_amount,oi.row_weight,oi.row_total_incl_tax as total FROM sales_flat_order_item oi WHERE oi.order_id=? LIMIT 1000`, [order_id])
    const orders = data.result;
    res.json(orders);
  })
);


r.get(
  "/boxes",
  wrapAsyncWithDb(async function (req, res, next) {
    const client = require('../webshop/client');
    const data = await client.query(`
    SELECT 
      sfo.increment_id  as order_id,
			sfo.entity_id as _order_entity_id,
			sfo.entity_id,
      sfo.created_at,
			sfo.increment_id as order_no,

      sfo.status,
      sfo.customer_firstname,
      sfo.customer_lastname,
			sfo.grand_total, 
			sfo.shipping_incl_tax  as shipping_amount,
      oa.firstname,
      oa.lastname,
			oa.telephone as customer_phone,
			oa.country_id,
			sfo.customer_email,
			oa.street,
			oa.postcode, 
			oa.city, 
      cpev.value as product,
      sfoi.product_options  as options
      FROM sales_flat_order_item sfoi
      LEFT JOIN  catalog_product_entity cp  ON sfoi.product_id = cp.entity_id
      LEFT JOIN eav_entity_type eet ON eet.entity_type_code = 'catalog_product'
      LEFT JOIN eav_attribute ea ON ea.entity_type_id = eet.entity_type_id  AND ea.attribute_code = 'name'
      LEFT JOIN catalog_product_entity_varchar cpev ON cpev.attribute_id =ea.attribute_id  AND cpev.entity_type_id = eet.entity_type_id
        AND cpev.entity_id = cp.entity_id AND cpev.store_id =0
      LEFT JOIN sales_flat_order sfo ON sfo.entity_id = sfoi.order_id 
      LEFT JOIN sales_flat_order_address oa ON oa.parent_id=sfo.entity_id AND oa.address_type='shipping'
      LEFT JOIN catalog_product_option cpo ON cpo.option_id = 11
      LEFT JOIN catalog_product_option_type_value cpotv ON cpotv.option_id = cpo.option_id 
      LEFT JOIN catalog_product_option_type_title cpott ON cpott.option_type_id =cpotv.option_type_id 
      WHERE sfoi.product_id IN (1019,1023,1024,1025,1026) AND sfo.status IN ('processing', 'holded', 'complete')
      GROUP BY sfoi.item_id 
      ORDER BY sfoi.product_id, sfo.created_at
    `)
    const boxes = data.result;
    boxes.forEach(b => {
      b.options = phpu(b.options);
      b.options = b?.options.options?.map(o => o?.value);
    });
    const counts = {};
    for (let b of boxes) {
			const key = `${b.product}`;
			const options = `${(b.options ?? []).join(';')}`;
			if(!counts[key]) {
				counts[key] = {};
			}
			counts[key][options] = (counts[key][options] ?? 0) + 1 
		}
		const sugar = await req.pg.query(`SELECT o.order_id as entity_id,o.print_count,o.comment,og.name as order_group, og.color as order_color FROM webshop_order_meta o LEFT JOIN webshop_order_group og ON og.id=o.group_id WHERE o.order_id = ANY($1)`, [boxes.map(d => d.entity_id)]);
    for (let s of sugar.rows) {
      const o = boxes.find(o => o.entity_id == s.entity_id);
      if (o) {
        Object.assign(o, s);
      }
    }
    res.json({ boxes, counts });
  })
);
async function requestWebshopPage(url, page) {
  const b = await puppeteer.browser;
  if (!page) {
    page = await b.newPage();
  }
  await page.goto(url);
  const title = await page.title();
  if (/^Log into Magento Admin/.test(title)) {
    //login
    await page.type('#username', process.env.WEBSHOP_USER);
    await page.type('#login', process.env.WEBSHOP_PASSWORD);
    await page.click('#loginForm input[type=submit]');
    await page.waitForNavigation({ waitUntil: 'networkidle2' });
    await page.goto(url);
  }

  return page;
}

r.put(
  "/orders/:order/comment",
  wrapAsyncWithDb(async function (req, res, next) {
    await req.pg.query(`INSERT INTO webshop_order_meta(order_id,comment) VALUES($1,$2) ON CONFLICT (order_id) DO UPDATE SET comment=$2;`,
      [req.params.order, req.body.comment]);

    res.status(200).json({ ok: true });
  }
  ));
r.post(
  "/orders/pdf",
  wrapAsyncWithDb(async function (req, res, next) {
    const order_ids = (req.body.orders).map(oid => parseInt(oid));
    let page = await requestWebshopPage(process.env.WEBSHOP_URL_ORDERS + '/');
    const pdfs = []
    orders: for (let o of order_ids) {
      for (let it = 0; it < 5; it++) {
        await page.goto(`${process.env.WEBSHOP_URL_INVOICE}/${o}/`);
        const title = await page.title();

        if (/^(Ticket|Order)/.test(title)) {
          const pdf = await page.pdf({
            format: 'A4',
						margin: {top: '1.5cm', left: '1.5cm', right: '1.5cm', bottom: 70},
            displayHeaderFooter: true,
            footerTemplate: '<span class="pageNumber"></span>/<span class="totalPages"></span>'
          });
          pdfs.push(pdf);
          pdfs.push(pdf);
          continue orders;
        }
        return res.status(500).json({ message: 'Invalid title ' + title })
      }
    }

    const pdf = require('pdfjs');
    const doc = new pdf.Document();

    for(let p of pdfs){
      var ext = new pdf.ExternalDocument(p);
      doc.addPagesOf(ext)
    }
		const buffer = await doc.asBuffer();
		
		await req.pg.query(`INSERT INTO webshop_order_meta(order_id,print_count) SELECT *,1 FROM UNNEST ($1::varchar[]) ON CONFLICT (order_id) DO UPDATE SET print_count=webshop_order_meta.print_count+1;`, [order_ids]);
          
    res.set('Content-Type', 'application/pdf').status(200).send(buffer);
  }
));
r.post(
  "/orders/:order/print",
  wrapAsyncWithDb(async function (req, res, next) {
    const order_id = parseInt(req.params.order);
    const page = await requestWebshopPage(`${process.env.WEBSHOP_URL_INVOICE}/${order_id}/`);
    const title = await page.title();
    if (/^Ticket/.test(title)) {
      await page.pdf({
        format: 'A4',
        path: './out.pdf'
      });
      return new Promise((resolve) => {
        exec(`lp -d iR-ADV-4225-4235-UFR-II -o media=A4 out.pdf`, async (error, stdout, stderr) => {
          if (error) {
            res.status(500).json({ message: `print error: ${error}` });
            return resolve();
          }
          await req.pg.query(`INSERT INTO webshop_order_meta(order_id,print_count) VALUES($1,1) ON CONFLICT (order_id) DO UPDATE SET print_count=webshop_order_meta.print_count+1;`, [order_id]);
          res.status(200).json({
            ok: true
          })
          resolve();
        });
      })


    } else {
      res.status(500).json({ message: 'Invalid title ' + title })
    }
  }
  ));


module.exports = r;
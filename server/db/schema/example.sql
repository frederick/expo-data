insert into product_category values('1000',1,'No tax (0,0%)',0,null,1);
insert into product_category values('2000',1,'Reduced tax (5,5%)',5.5,null,5);
insert into product_category values('0',1,'Normal tax (20%)',20,null,4);
insert into product_category values ('0',1,'Main (20%)',20,null,4);

insert into pasteque.categories (id,reference,label,hasimage,disp_order) select ROW_NUMBER() OVER (ORDER BY code)+1, code,name,false,0  from product_category;

INSERT INTO public.sales_journal (name,code,description) VALUES	 ('Daily pasteque sales','pasteque','Records of daily transactions by payment method.');

INSERT INTO public.configuration_setting
("key", value)
VALUES('ticket.header', 'My awesome company

4 mystreet, 99999 Fake city
Tel. +12345679 / Fax. +123456789

');
INSERT INTO public.configuration_setting
("key", value)
VALUES('ticket.location', 'My place');

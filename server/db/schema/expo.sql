--
-- PostgreSQL database dump
--

-- Dumped from database version 10.8
-- Dumped by pg_dump version 13.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: audit; Type: SCHEMA; Schema: -; Owner: expo
--

CREATE SCHEMA audit;


ALTER SCHEMA audit OWNER TO expo;

--
-- Name: pasteque; Type: SCHEMA; Schema: -; Owner: expo
--

CREATE SCHEMA pasteque;


ALTER SCHEMA pasteque OWNER TO expo;

--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


--
-- Name: mysql_fdw; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS mysql_fdw WITH SCHEMA public;


--
-- Name: EXTENSION mysql_fdw; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION mysql_fdw IS 'Foreign data wrapper for querying a MySQL server';


--
-- Name: postgres_fdw; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgres_fdw WITH SCHEMA public;


--
-- Name: EXTENSION postgres_fdw; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgres_fdw IS 'foreign-data wrapper for remote PostgreSQL servers';


--
-- Name: inventory_type; Type: TYPE; Schema: public; Owner: expo
--

CREATE TYPE public.inventory_type AS ENUM (
    'continuous',
    'special'
);


ALTER TYPE public.inventory_type OWNER TO expo;

--
-- Name: stock_adjustment_type; Type: TYPE; Schema: public; Owner: expo
--

CREATE TYPE public.stock_adjustment_type AS ENUM (
    'withdrawl',
    'production',
    'movement',
    'correction',
    'sale',
    'distribution',
    'count',
    'damaged'
);


ALTER TYPE public.stock_adjustment_type OWNER TO expo;

--
-- Name: stock_operation_type; Type: TYPE; Schema: public; Owner: expo
--

CREATE TYPE public.stock_operation_type AS ENUM (
    'withdrawl',
    'production',
    'movement',
    'correction',
    'sale',
    'distribution',
    'count',
    'damaged'
);


ALTER TYPE public.stock_operation_type OWNER TO expo;

--
-- Name: audit_table(regclass); Type: FUNCTION; Schema: audit; Owner: expo
--

CREATE FUNCTION audit.audit_table(target_table regclass) RETURNS void
    LANGUAGE sql
    AS $_$
SELECT audit.audit_table($1, BOOLEAN 't', BOOLEAN 't');
$_$;


ALTER FUNCTION audit.audit_table(target_table regclass) OWNER TO expo;

--
-- Name: FUNCTION audit_table(target_table regclass); Type: COMMENT; Schema: audit; Owner: expo
--

COMMENT ON FUNCTION audit.audit_table(target_table regclass) IS '
Add auditing support to the given table. Row-level changes will be logged with full client query text. No cols are ignored.
';


--
-- Name: audit_table(regclass, boolean, boolean); Type: FUNCTION; Schema: audit; Owner: expo
--

CREATE FUNCTION audit.audit_table(target_table regclass, audit_rows boolean, audit_query_text boolean) RETURNS void
    LANGUAGE sql
    AS $_$
SELECT audit.audit_table($1, $2, $3, ARRAY[]::text[]);
$_$;


ALTER FUNCTION audit.audit_table(target_table regclass, audit_rows boolean, audit_query_text boolean) OWNER TO expo;

--
-- Name: audit_table(regclass, boolean, boolean, text[]); Type: FUNCTION; Schema: audit; Owner: expo
--

CREATE FUNCTION audit.audit_table(target_table regclass, audit_rows boolean, audit_query_text boolean, ignored_cols text[]) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
  stm_targets text = 'INSERT OR UPDATE OR DELETE OR TRUNCATE';
  _q_txt text;
  _ignored_cols_snip text = '';
BEGIN
    EXECUTE 'DROP TRIGGER IF EXISTS audit_trigger_row ON ' || target_table;
    EXECUTE 'DROP TRIGGER IF EXISTS audit_trigger_stm ON ' || target_table;

    IF audit_rows THEN
        IF array_length(ignored_cols,1) > 0 THEN
            _ignored_cols_snip = ', ' || quote_literal(ignored_cols);
        END IF;
        _q_txt = 'CREATE TRIGGER audit_trigger_row AFTER INSERT OR UPDATE OR DELETE ON ' || 
                 target_table || 
                 ' FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func(' ||
                 quote_literal(audit_query_text) || _ignored_cols_snip || ');';
        RAISE NOTICE '%',_q_txt;
        EXECUTE _q_txt;
        stm_targets = 'TRUNCATE';
    ELSE
    END IF;

    _q_txt = 'CREATE TRIGGER audit_trigger_stm AFTER ' || stm_targets || ' ON ' ||
             target_table ||
             ' FOR EACH STATEMENT EXECUTE PROCEDURE audit.if_modified_func('||
             quote_literal(audit_query_text) || ');';
    RAISE NOTICE '%',_q_txt;
    EXECUTE _q_txt;

END;
$$;


ALTER FUNCTION audit.audit_table(target_table regclass, audit_rows boolean, audit_query_text boolean, ignored_cols text[]) OWNER TO expo;

--
-- Name: FUNCTION audit_table(target_table regclass, audit_rows boolean, audit_query_text boolean, ignored_cols text[]); Type: COMMENT; Schema: audit; Owner: expo
--

COMMENT ON FUNCTION audit.audit_table(target_table regclass, audit_rows boolean, audit_query_text boolean, ignored_cols text[]) IS '
Add auditing support to a table.

Arguments:
   target_table:     Table name, schema qualified if not on search_path
   audit_rows:       Record each row change, or only audit at a statement level
   audit_query_text: Record the text of the client query that triggered the audit event?
   ignored_cols:     Columns to exclude from update diffs, ignore updates that change only ignored cols.
';


--
-- Name: if_modified_func(); Type: FUNCTION; Schema: audit; Owner: expo
--

CREATE FUNCTION audit.if_modified_func() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    SET search_path TO 'pg_catalog', 'public'
    AS $$
DECLARE
    audit_row audit.logged_actions;
    include_values boolean;
    log_diffs boolean;
    h_old hstore;
    h_new hstore;
    excluded_cols text[] = ARRAY[]::text[];
BEGIN
    IF TG_WHEN <> 'AFTER' THEN
        RAISE EXCEPTION 'audit.if_modified_func() may only run as an AFTER trigger';
    END IF;

    audit_row = ROW(
        nextval('audit.logged_actions_event_id_seq'), -- event_id
        TG_TABLE_SCHEMA::text,                        -- schema_name
        TG_TABLE_NAME::text,                          -- table_name
        TG_RELID,                                     -- relation OID for much quicker searches
        session_user::text,                           -- session_user_name
        current_timestamp,                            -- action_tstamp_tx
        statement_timestamp(),                        -- action_tstamp_stm
        clock_timestamp(),                            -- action_tstamp_clk
        txid_current(),                               -- transaction ID
        current_setting('application_name'),          -- client application
        inet_client_addr(),                           -- client_addr
        inet_client_port(),                           -- client_port
        current_query(),                              -- top-level query or queries (if multistatement) from client
        substring(TG_OP,1,1),                         -- action
        NULL, NULL,                                   -- row_data, changed_fields
        'f',                                           -- statement_only
        current_setting('app.username','T')         -- app var
        );

    IF NOT TG_ARGV[0]::boolean IS DISTINCT FROM 'f'::boolean THEN
        audit_row.client_query = NULL;
    END IF;

    IF TG_ARGV[1] IS NOT NULL THEN
        excluded_cols = TG_ARGV[1]::text[];
    END IF;
    
    IF (TG_OP = 'UPDATE' AND TG_LEVEL = 'ROW') THEN
        audit_row.row_data = hstore(OLD.*) - excluded_cols;
        audit_row.changed_fields =  (hstore(NEW.*) - audit_row.row_data) - excluded_cols;
        IF audit_row.changed_fields = hstore('') THEN
            -- All changed fields are ignored. Skip this update.
            RETURN NULL;
        END IF;
    ELSIF (TG_OP = 'DELETE' AND TG_LEVEL = 'ROW') THEN
        audit_row.row_data = hstore(OLD.*) - excluded_cols;
    ELSIF (TG_OP = 'INSERT' AND TG_LEVEL = 'ROW') THEN
        audit_row.row_data = hstore(NEW.*) - excluded_cols;
    ELSIF (TG_LEVEL = 'STATEMENT' AND TG_OP IN ('INSERT','UPDATE','DELETE','TRUNCATE')) THEN
        audit_row.statement_only = 't';
    ELSE
        RAISE EXCEPTION '[audit.if_modified_func] - Trigger func added as trigger for unhandled case: %, %',TG_OP, TG_LEVEL;
        RETURN NULL;
    END IF;
    INSERT INTO audit.logged_actions VALUES (audit_row.*);
    RETURN NULL;
END;
$$;


ALTER FUNCTION audit.if_modified_func() OWNER TO expo;

--
-- Name: FUNCTION if_modified_func(); Type: COMMENT; Schema: audit; Owner: expo
--

COMMENT ON FUNCTION audit.if_modified_func() IS '
Track changes to a table at the statement and/or row level.

Optional parameters to trigger in CREATE TRIGGER call:

param 0: boolean, whether to log the query text. Default ''t''.

param 1: text[], columns to ignore in updates. Default [].

         Updates to ignored cols are omitted from changed_fields.

         Updates with only ignored cols changed are not inserted
         into the audit log.

         Almost all the processing work is still done for updates
         that ignored. If you need to save the load, you need to use
         WHEN clause on the trigger instead.

         No warning or error is issued if ignored_cols contains columns
         that do not exist in the target table. This lets you specify
         a standard set of ignored columns.

There is no parameter to disable logging of values. Add this trigger as
a ''FOR EACH STATEMENT'' rather than ''FOR EACH ROW'' trigger if you do not
want to log row values.

Note that the user name logged is the login role for the session. The audit trigger
cannot obtain the active role because it is reset by the SECURITY DEFINER invocation
of the audit trigger its self.
';


--
-- Name: test_slow(); Type: FUNCTION; Schema: public; Owner: expo
--

CREATE FUNCTION public.test_slow() RETURNS integer
    LANGUAGE plpgsql IMMUTABLE
    AS $$
begin
perform pg_sleep(1);
return 1;
end
$$;


ALTER FUNCTION public.test_slow() OWNER TO expo;

--
-- Name: local_pasteque; Type: SERVER; Schema: -; Owner: postgres
--

CREATE SERVER local_pasteque FOREIGN DATA WRAPPER postgres_fdw OPTIONS (
    dbname 'pasteque',
    host 'localhost',
    port '5432'
);


ALTER SERVER local_pasteque OWNER TO postgres;

--
-- Name: USER MAPPING expo SERVER local_pasteque; Type: USER MAPPING; Schema: -; Owner: postgres
--

CREATE USER MAPPING FOR expo SERVER local_pasteque OPTIONS (
    password_required 'false',
    "user" 'expo'
);


--
-- Name: USER MAPPING postgres SERVER local_pasteque; Type: USER MAPPING; Schema: -; Owner: postgres
--

CREATE USER MAPPING FOR postgres SERVER local_pasteque;


SET default_tablespace = '';

--
-- Name: logged_actions; Type: TABLE; Schema: audit; Owner: expo
--

CREATE TABLE audit.logged_actions (
    event_id bigint NOT NULL,
    schema_name text NOT NULL,
    table_name text NOT NULL,
    relid oid NOT NULL,
    session_user_name text,
    action_tstamp_tx timestamp with time zone NOT NULL,
    action_tstamp_stm timestamp with time zone NOT NULL,
    action_tstamp_clk timestamp with time zone NOT NULL,
    transaction_id bigint,
    application_name text,
    client_addr inet,
    client_port integer,
    client_query text,
    action text NOT NULL,
    row_data public.hstore,
    changed_fields public.hstore,
    statement_only boolean NOT NULL,
    app_user_name text,
    CONSTRAINT logged_actions_action_check CHECK ((action = ANY (ARRAY['I'::text, 'D'::text, 'U'::text, 'T'::text])))
);


ALTER TABLE audit.logged_actions OWNER TO expo;

--
-- Name: TABLE logged_actions; Type: COMMENT; Schema: audit; Owner: expo
--

COMMENT ON TABLE audit.logged_actions IS 'History of auditable actions on audited tables, from audit.if_modified_func()';


--
-- Name: COLUMN logged_actions.event_id; Type: COMMENT; Schema: audit; Owner: expo
--

COMMENT ON COLUMN audit.logged_actions.event_id IS 'Unique identifier for each auditable event';


--
-- Name: COLUMN logged_actions.schema_name; Type: COMMENT; Schema: audit; Owner: expo
--

COMMENT ON COLUMN audit.logged_actions.schema_name IS 'Database schema audited table for this event is in';


--
-- Name: COLUMN logged_actions.table_name; Type: COMMENT; Schema: audit; Owner: expo
--

COMMENT ON COLUMN audit.logged_actions.table_name IS 'Non-schema-qualified table name of table event occured in';


--
-- Name: COLUMN logged_actions.relid; Type: COMMENT; Schema: audit; Owner: expo
--

COMMENT ON COLUMN audit.logged_actions.relid IS 'Table OID. Changes with drop/create. Get with ''tablename''::regclass';


--
-- Name: COLUMN logged_actions.session_user_name; Type: COMMENT; Schema: audit; Owner: expo
--

COMMENT ON COLUMN audit.logged_actions.session_user_name IS 'Login / session user whose statement caused the audited event';


--
-- Name: COLUMN logged_actions.action_tstamp_tx; Type: COMMENT; Schema: audit; Owner: expo
--

COMMENT ON COLUMN audit.logged_actions.action_tstamp_tx IS 'Transaction start timestamp for tx in which audited event occurred';


--
-- Name: COLUMN logged_actions.action_tstamp_stm; Type: COMMENT; Schema: audit; Owner: expo
--

COMMENT ON COLUMN audit.logged_actions.action_tstamp_stm IS 'Statement start timestamp for tx in which audited event occurred';


--
-- Name: COLUMN logged_actions.action_tstamp_clk; Type: COMMENT; Schema: audit; Owner: expo
--

COMMENT ON COLUMN audit.logged_actions.action_tstamp_clk IS 'Wall clock time at which audited event''s trigger call occurred';


--
-- Name: COLUMN logged_actions.transaction_id; Type: COMMENT; Schema: audit; Owner: expo
--

COMMENT ON COLUMN audit.logged_actions.transaction_id IS 'Identifier of transaction that made the change. May wrap, but unique paired with action_tstamp_tx.';


--
-- Name: COLUMN logged_actions.application_name; Type: COMMENT; Schema: audit; Owner: expo
--

COMMENT ON COLUMN audit.logged_actions.application_name IS 'Application name set when this audit event occurred. Can be changed in-session by client.';


--
-- Name: COLUMN logged_actions.client_addr; Type: COMMENT; Schema: audit; Owner: expo
--

COMMENT ON COLUMN audit.logged_actions.client_addr IS 'IP address of client that issued query. Null for unix domain socket.';


--
-- Name: COLUMN logged_actions.client_port; Type: COMMENT; Schema: audit; Owner: expo
--

COMMENT ON COLUMN audit.logged_actions.client_port IS 'Remote peer IP port address of client that issued query. Undefined for unix socket.';


--
-- Name: COLUMN logged_actions.client_query; Type: COMMENT; Schema: audit; Owner: expo
--

COMMENT ON COLUMN audit.logged_actions.client_query IS 'Top-level query that caused this auditable event. May be more than one statement.';


--
-- Name: COLUMN logged_actions.action; Type: COMMENT; Schema: audit; Owner: expo
--

COMMENT ON COLUMN audit.logged_actions.action IS 'Action type; I = insert, D = delete, U = update, T = truncate';


--
-- Name: COLUMN logged_actions.row_data; Type: COMMENT; Schema: audit; Owner: expo
--

COMMENT ON COLUMN audit.logged_actions.row_data IS 'Record value. Null for statement-level trigger. For INSERT this is the new tuple. For DELETE and UPDATE it is the old tuple.';


--
-- Name: COLUMN logged_actions.changed_fields; Type: COMMENT; Schema: audit; Owner: expo
--

COMMENT ON COLUMN audit.logged_actions.changed_fields IS 'New values of fields changed by UPDATE. Null except for row-level UPDATE events.';


--
-- Name: COLUMN logged_actions.statement_only; Type: COMMENT; Schema: audit; Owner: expo
--

COMMENT ON COLUMN audit.logged_actions.statement_only IS '''t'' if audit event is from an FOR EACH STATEMENT trigger, ''f'' for FOR EACH ROW';


--
-- Name: logged_actions_event_id_seq; Type: SEQUENCE; Schema: audit; Owner: expo
--

CREATE SEQUENCE audit.logged_actions_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE audit.logged_actions_event_id_seq OWNER TO expo;

--
-- Name: logged_actions_event_id_seq; Type: SEQUENCE OWNED BY; Schema: audit; Owner: expo
--

ALTER SEQUENCE audit.logged_actions_event_id_seq OWNED BY audit.logged_actions.event_id;


--
-- Name: tableslist; Type: VIEW; Schema: audit; Owner: expo
--

CREATE VIEW audit.tableslist AS
 SELECT DISTINCT triggers.trigger_schema AS schema,
    triggers.event_object_table AS auditedtable
   FROM information_schema.triggers
  WHERE ((triggers.trigger_name)::text = ANY (ARRAY['audit_trigger_row'::text, 'audit_trigger_stm'::text]))
  ORDER BY triggers.trigger_schema, triggers.event_object_table;


ALTER TABLE audit.tableslist OWNER TO expo;

--
-- Name: VIEW tableslist; Type: COMMENT; Schema: audit; Owner: expo
--

COMMENT ON VIEW audit.tableslist IS '
View showing all tables with auditing set up. Ordered by schema, then table.
';


--
-- Name: cashregisters; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.cashregisters (
    id integer NOT NULL,
    reference character varying(255) NOT NULL,
    label character varying(255) NOT NULL,
    nextticketid integer NOT NULL,
    nextsessionid integer NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'cashregisters'
);
ALTER FOREIGN TABLE pasteque.cashregisters ALTER COLUMN id OPTIONS (
    column_name 'id'
);
ALTER FOREIGN TABLE pasteque.cashregisters ALTER COLUMN reference OPTIONS (
    column_name 'reference'
);
ALTER FOREIGN TABLE pasteque.cashregisters ALTER COLUMN label OPTIONS (
    column_name 'label'
);
ALTER FOREIGN TABLE pasteque.cashregisters ALTER COLUMN nextticketid OPTIONS (
    column_name 'nextticketid'
);
ALTER FOREIGN TABLE pasteque.cashregisters ALTER COLUMN nextsessionid OPTIONS (
    column_name 'nextsessionid'
);


ALTER FOREIGN TABLE pasteque.cashregisters OWNER TO postgres;

--
-- Name: categories; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.categories (
    id integer NOT NULL,
    parent_id integer,
    reference character varying(255) NOT NULL,
    label character varying(255) NOT NULL,
    hasimage boolean NOT NULL,
    disp_order integer NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'categories'
);
ALTER FOREIGN TABLE pasteque.categories ALTER COLUMN id OPTIONS (
    column_name 'id'
);
ALTER FOREIGN TABLE pasteque.categories ALTER COLUMN parent_id OPTIONS (
    column_name 'parent_id'
);
ALTER FOREIGN TABLE pasteque.categories ALTER COLUMN reference OPTIONS (
    column_name 'reference'
);
ALTER FOREIGN TABLE pasteque.categories ALTER COLUMN label OPTIONS (
    column_name 'label'
);
ALTER FOREIGN TABLE pasteque.categories ALTER COLUMN hasimage OPTIONS (
    column_name 'hasimage'
);
ALTER FOREIGN TABLE pasteque.categories ALTER COLUMN disp_order OPTIONS (
    column_name 'disp_order'
);


ALTER FOREIGN TABLE pasteque.categories OWNER TO postgres;

--
-- Name: compositiongroups; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.compositiongroups (
    id integer NOT NULL,
    product_id integer NOT NULL,
    label character varying(255) NOT NULL,
    disp_order integer NOT NULL,
    image bytea
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'compositiongroups'
);
ALTER FOREIGN TABLE pasteque.compositiongroups ALTER COLUMN id OPTIONS (
    column_name 'id'
);
ALTER FOREIGN TABLE pasteque.compositiongroups ALTER COLUMN product_id OPTIONS (
    column_name 'product_id'
);
ALTER FOREIGN TABLE pasteque.compositiongroups ALTER COLUMN label OPTIONS (
    column_name 'label'
);
ALTER FOREIGN TABLE pasteque.compositiongroups ALTER COLUMN disp_order OPTIONS (
    column_name 'disp_order'
);
ALTER FOREIGN TABLE pasteque.compositiongroups ALTER COLUMN image OPTIONS (
    column_name 'image'
);


ALTER FOREIGN TABLE pasteque.compositiongroups OWNER TO postgres;

--
-- Name: compositionproducts; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.compositionproducts (
    compositiongroup_id integer NOT NULL,
    product_id integer NOT NULL,
    disp_order integer NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'compositionproducts'
);
ALTER FOREIGN TABLE pasteque.compositionproducts ALTER COLUMN compositiongroup_id OPTIONS (
    column_name 'compositiongroup_id'
);
ALTER FOREIGN TABLE pasteque.compositionproducts ALTER COLUMN product_id OPTIONS (
    column_name 'product_id'
);
ALTER FOREIGN TABLE pasteque.compositionproducts ALTER COLUMN disp_order OPTIONS (
    column_name 'disp_order'
);


ALTER FOREIGN TABLE pasteque.compositionproducts OWNER TO postgres;

--
-- Name: currencies; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.currencies (
    id integer NOT NULL,
    reference character varying(255) NOT NULL,
    label character varying(255) NOT NULL,
    symbol character varying(255) NOT NULL,
    decimalseparator character varying(255) NOT NULL,
    thousandsseparator character varying(255) NOT NULL,
    format character varying(255) NOT NULL,
    rate double precision NOT NULL,
    main boolean NOT NULL,
    visible boolean NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'currencies'
);
ALTER FOREIGN TABLE pasteque.currencies ALTER COLUMN id OPTIONS (
    column_name 'id'
);
ALTER FOREIGN TABLE pasteque.currencies ALTER COLUMN reference OPTIONS (
    column_name 'reference'
);
ALTER FOREIGN TABLE pasteque.currencies ALTER COLUMN label OPTIONS (
    column_name 'label'
);
ALTER FOREIGN TABLE pasteque.currencies ALTER COLUMN symbol OPTIONS (
    column_name 'symbol'
);
ALTER FOREIGN TABLE pasteque.currencies ALTER COLUMN decimalseparator OPTIONS (
    column_name 'decimalseparator'
);
ALTER FOREIGN TABLE pasteque.currencies ALTER COLUMN thousandsseparator OPTIONS (
    column_name 'thousandsseparator'
);
ALTER FOREIGN TABLE pasteque.currencies ALTER COLUMN format OPTIONS (
    column_name 'format'
);
ALTER FOREIGN TABLE pasteque.currencies ALTER COLUMN rate OPTIONS (
    column_name 'rate'
);
ALTER FOREIGN TABLE pasteque.currencies ALTER COLUMN main OPTIONS (
    column_name 'main'
);
ALTER FOREIGN TABLE pasteque.currencies ALTER COLUMN visible OPTIONS (
    column_name 'visible'
);


ALTER FOREIGN TABLE pasteque.currencies OWNER TO postgres;

--
-- Name: customers; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.customers (
    id integer NOT NULL,
    tax_id integer,
    discountprofile_id integer,
    tariffarea_id integer,
    dispname character varying(255) NOT NULL,
    card character varying(255) NOT NULL,
    firstname character varying(255) NOT NULL,
    lastname character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    phone1 character varying(255) NOT NULL,
    phone2 character varying(255) NOT NULL,
    fax character varying(255) NOT NULL,
    addr1 character varying(255) NOT NULL,
    addr2 character varying(255) NOT NULL,
    zipcode character varying(255) NOT NULL,
    city character varying(255) NOT NULL,
    region character varying(255) NOT NULL,
    country character varying(255) NOT NULL,
    balance double precision NOT NULL,
    maxdebt double precision NOT NULL,
    note text NOT NULL,
    visible boolean NOT NULL,
    expiredate timestamp(0) without time zone,
    hasimage boolean NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'customers'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN id OPTIONS (
    column_name 'id'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN tax_id OPTIONS (
    column_name 'tax_id'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN discountprofile_id OPTIONS (
    column_name 'discountprofile_id'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN tariffarea_id OPTIONS (
    column_name 'tariffarea_id'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN dispname OPTIONS (
    column_name 'dispname'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN card OPTIONS (
    column_name 'card'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN firstname OPTIONS (
    column_name 'firstname'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN lastname OPTIONS (
    column_name 'lastname'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN email OPTIONS (
    column_name 'email'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN phone1 OPTIONS (
    column_name 'phone1'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN phone2 OPTIONS (
    column_name 'phone2'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN fax OPTIONS (
    column_name 'fax'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN addr1 OPTIONS (
    column_name 'addr1'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN addr2 OPTIONS (
    column_name 'addr2'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN zipcode OPTIONS (
    column_name 'zipcode'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN city OPTIONS (
    column_name 'city'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN region OPTIONS (
    column_name 'region'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN country OPTIONS (
    column_name 'country'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN balance OPTIONS (
    column_name 'balance'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN maxdebt OPTIONS (
    column_name 'maxdebt'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN note OPTIONS (
    column_name 'note'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN visible OPTIONS (
    column_name 'visible'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN expiredate OPTIONS (
    column_name 'expiredate'
);
ALTER FOREIGN TABLE pasteque.customers ALTER COLUMN hasimage OPTIONS (
    column_name 'hasimage'
);


ALTER FOREIGN TABLE pasteque.customers OWNER TO postgres;

--
-- Name: discountprofiles; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.discountprofiles (
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    rate double precision NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'discountprofiles'
);
ALTER FOREIGN TABLE pasteque.discountprofiles ALTER COLUMN id OPTIONS (
    column_name 'id'
);
ALTER FOREIGN TABLE pasteque.discountprofiles ALTER COLUMN label OPTIONS (
    column_name 'label'
);
ALTER FOREIGN TABLE pasteque.discountprofiles ALTER COLUMN rate OPTIONS (
    column_name 'rate'
);


ALTER FOREIGN TABLE pasteque.discountprofiles OWNER TO postgres;

--
-- Name: discounts; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.discounts (
    id integer NOT NULL,
    startdate timestamp(0) without time zone,
    enddate timestamp(0) without time zone,
    rate double precision NOT NULL,
    barcode character varying(255),
    barcodetype integer NOT NULL,
    disp_order integer NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'discounts'
);
ALTER FOREIGN TABLE pasteque.discounts ALTER COLUMN id OPTIONS (
    column_name 'id'
);
ALTER FOREIGN TABLE pasteque.discounts ALTER COLUMN startdate OPTIONS (
    column_name 'startdate'
);
ALTER FOREIGN TABLE pasteque.discounts ALTER COLUMN enddate OPTIONS (
    column_name 'enddate'
);
ALTER FOREIGN TABLE pasteque.discounts ALTER COLUMN rate OPTIONS (
    column_name 'rate'
);
ALTER FOREIGN TABLE pasteque.discounts ALTER COLUMN barcode OPTIONS (
    column_name 'barcode'
);
ALTER FOREIGN TABLE pasteque.discounts ALTER COLUMN barcodetype OPTIONS (
    column_name 'barcodetype'
);
ALTER FOREIGN TABLE pasteque.discounts ALTER COLUMN disp_order OPTIONS (
    column_name 'disp_order'
);


ALTER FOREIGN TABLE pasteque.discounts OWNER TO postgres;

--
-- Name: fiscaltickets; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.fiscaltickets (
    type character varying(255) NOT NULL,
    sequence character varying(255) NOT NULL,
    number integer NOT NULL,
    date timestamp(0) without time zone NOT NULL,
    content text NOT NULL,
    signature character varying(255) NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'fiscaltickets'
);
ALTER FOREIGN TABLE pasteque.fiscaltickets ALTER COLUMN type OPTIONS (
    column_name 'type'
);
ALTER FOREIGN TABLE pasteque.fiscaltickets ALTER COLUMN sequence OPTIONS (
    column_name 'sequence'
);
ALTER FOREIGN TABLE pasteque.fiscaltickets ALTER COLUMN number OPTIONS (
    column_name 'number'
);
ALTER FOREIGN TABLE pasteque.fiscaltickets ALTER COLUMN date OPTIONS (
    column_name 'date'
);
ALTER FOREIGN TABLE pasteque.fiscaltickets ALTER COLUMN content OPTIONS (
    column_name 'content'
);
ALTER FOREIGN TABLE pasteque.fiscaltickets ALTER COLUMN signature OPTIONS (
    column_name 'signature'
);


ALTER FOREIGN TABLE pasteque.fiscaltickets OWNER TO postgres;

--
-- Name: floors; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.floors (
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    image bytea,
    disp_order integer NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'floors'
);
ALTER FOREIGN TABLE pasteque.floors ALTER COLUMN id OPTIONS (
    column_name 'id'
);
ALTER FOREIGN TABLE pasteque.floors ALTER COLUMN label OPTIONS (
    column_name 'label'
);
ALTER FOREIGN TABLE pasteque.floors ALTER COLUMN image OPTIONS (
    column_name 'image'
);
ALTER FOREIGN TABLE pasteque.floors ALTER COLUMN disp_order OPTIONS (
    column_name 'disp_order'
);


ALTER FOREIGN TABLE pasteque.floors OWNER TO postgres;

--
-- Name: images; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.images (
    model character varying(255) NOT NULL,
    modelid character varying(255) NOT NULL,
    mimetype character varying(255) NOT NULL,
    image bytea NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'images'
);
ALTER FOREIGN TABLE pasteque.images ALTER COLUMN model OPTIONS (
    column_name 'model'
);
ALTER FOREIGN TABLE pasteque.images ALTER COLUMN modelid OPTIONS (
    column_name 'modelid'
);
ALTER FOREIGN TABLE pasteque.images ALTER COLUMN mimetype OPTIONS (
    column_name 'mimetype'
);
ALTER FOREIGN TABLE pasteque.images ALTER COLUMN image OPTIONS (
    column_name 'image'
);


ALTER FOREIGN TABLE pasteque.images OWNER TO postgres;

--
-- Name: options; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.options (
    name character varying(255) NOT NULL,
    system boolean NOT NULL,
    content text NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'options'
);
ALTER FOREIGN TABLE pasteque.options ALTER COLUMN name OPTIONS (
    column_name 'name'
);
ALTER FOREIGN TABLE pasteque.options ALTER COLUMN system OPTIONS (
    column_name 'system'
);
ALTER FOREIGN TABLE pasteque.options ALTER COLUMN content OPTIONS (
    column_name 'content'
);


ALTER FOREIGN TABLE pasteque.options OWNER TO postgres;

--
-- Name: paymentmodereturns; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.paymentmodereturns (
    paymentmode_id integer NOT NULL,
    returnmode_id integer NOT NULL,
    minamount double precision NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'paymentmodereturns'
);
ALTER FOREIGN TABLE pasteque.paymentmodereturns ALTER COLUMN paymentmode_id OPTIONS (
    column_name 'paymentmode_id'
);
ALTER FOREIGN TABLE pasteque.paymentmodereturns ALTER COLUMN returnmode_id OPTIONS (
    column_name 'returnmode_id'
);
ALTER FOREIGN TABLE pasteque.paymentmodereturns ALTER COLUMN minamount OPTIONS (
    column_name 'minamount'
);


ALTER FOREIGN TABLE pasteque.paymentmodereturns OWNER TO postgres;

--
-- Name: paymentmodes; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.paymentmodes (
    id integer NOT NULL,
    reference character varying(255) NOT NULL,
    label character varying(255) NOT NULL,
    backlabel character varying(255) NOT NULL,
    type integer NOT NULL,
    hasimage boolean NOT NULL,
    disp_order integer NOT NULL,
    visible boolean NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'paymentmodes'
);
ALTER FOREIGN TABLE pasteque.paymentmodes ALTER COLUMN id OPTIONS (
    column_name 'id'
);
ALTER FOREIGN TABLE pasteque.paymentmodes ALTER COLUMN reference OPTIONS (
    column_name 'reference'
);
ALTER FOREIGN TABLE pasteque.paymentmodes ALTER COLUMN label OPTIONS (
    column_name 'label'
);
ALTER FOREIGN TABLE pasteque.paymentmodes ALTER COLUMN backlabel OPTIONS (
    column_name 'backlabel'
);
ALTER FOREIGN TABLE pasteque.paymentmodes ALTER COLUMN type OPTIONS (
    column_name 'type'
);
ALTER FOREIGN TABLE pasteque.paymentmodes ALTER COLUMN hasimage OPTIONS (
    column_name 'hasimage'
);
ALTER FOREIGN TABLE pasteque.paymentmodes ALTER COLUMN disp_order OPTIONS (
    column_name 'disp_order'
);
ALTER FOREIGN TABLE pasteque.paymentmodes ALTER COLUMN visible OPTIONS (
    column_name 'visible'
);


ALTER FOREIGN TABLE pasteque.paymentmodes OWNER TO postgres;

--
-- Name: paymentmodevalues; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.paymentmodevalues (
    paymentmode_id integer NOT NULL,
    value double precision NOT NULL,
    hasimage boolean NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'paymentmodevalues'
);
ALTER FOREIGN TABLE pasteque.paymentmodevalues ALTER COLUMN paymentmode_id OPTIONS (
    column_name 'paymentmode_id'
);
ALTER FOREIGN TABLE pasteque.paymentmodevalues ALTER COLUMN value OPTIONS (
    column_name 'value'
);
ALTER FOREIGN TABLE pasteque.paymentmodevalues ALTER COLUMN hasimage OPTIONS (
    column_name 'hasimage'
);


ALTER FOREIGN TABLE pasteque.paymentmodevalues OWNER TO postgres;

--
-- Name: places; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.places (
    id integer NOT NULL,
    floor_id integer NOT NULL,
    label character varying(255) NOT NULL,
    x integer NOT NULL,
    y integer NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'places'
);
ALTER FOREIGN TABLE pasteque.places ALTER COLUMN id OPTIONS (
    column_name 'id'
);
ALTER FOREIGN TABLE pasteque.places ALTER COLUMN floor_id OPTIONS (
    column_name 'floor_id'
);
ALTER FOREIGN TABLE pasteque.places ALTER COLUMN label OPTIONS (
    column_name 'label'
);
ALTER FOREIGN TABLE pasteque.places ALTER COLUMN x OPTIONS (
    column_name 'x'
);
ALTER FOREIGN TABLE pasteque.places ALTER COLUMN y OPTIONS (
    column_name 'y'
);


ALTER FOREIGN TABLE pasteque.places OWNER TO postgres;

--
-- Name: products; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.products (
    id integer NOT NULL,
    category_id integer NOT NULL,
    tax_id integer NOT NULL,
    reference character varying(255) NOT NULL,
    barcode character varying(255) NOT NULL,
    label character varying(255) NOT NULL,
    pricebuy double precision,
    pricesell double precision NOT NULL,
    visible boolean NOT NULL,
    scaled boolean NOT NULL,
    scaletype smallint NOT NULL,
    scalevalue double precision NOT NULL,
    disp_order integer NOT NULL,
    hasimage boolean NOT NULL,
    discountenabled boolean NOT NULL,
    discountrate double precision NOT NULL,
    prepay boolean NOT NULL,
    composition boolean NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'products'
);
ALTER FOREIGN TABLE pasteque.products ALTER COLUMN id OPTIONS (
    column_name 'id'
);
ALTER FOREIGN TABLE pasteque.products ALTER COLUMN category_id OPTIONS (
    column_name 'category_id'
);
ALTER FOREIGN TABLE pasteque.products ALTER COLUMN tax_id OPTIONS (
    column_name 'tax_id'
);
ALTER FOREIGN TABLE pasteque.products ALTER COLUMN reference OPTIONS (
    column_name 'reference'
);
ALTER FOREIGN TABLE pasteque.products ALTER COLUMN barcode OPTIONS (
    column_name 'barcode'
);
ALTER FOREIGN TABLE pasteque.products ALTER COLUMN label OPTIONS (
    column_name 'label'
);
ALTER FOREIGN TABLE pasteque.products ALTER COLUMN pricebuy OPTIONS (
    column_name 'pricebuy'
);
ALTER FOREIGN TABLE pasteque.products ALTER COLUMN pricesell OPTIONS (
    column_name 'pricesell'
);
ALTER FOREIGN TABLE pasteque.products ALTER COLUMN visible OPTIONS (
    column_name 'visible'
);
ALTER FOREIGN TABLE pasteque.products ALTER COLUMN scaled OPTIONS (
    column_name 'scaled'
);
ALTER FOREIGN TABLE pasteque.products ALTER COLUMN scaletype OPTIONS (
    column_name 'scaletype'
);
ALTER FOREIGN TABLE pasteque.products ALTER COLUMN scalevalue OPTIONS (
    column_name 'scalevalue'
);
ALTER FOREIGN TABLE pasteque.products ALTER COLUMN disp_order OPTIONS (
    column_name 'disp_order'
);
ALTER FOREIGN TABLE pasteque.products ALTER COLUMN hasimage OPTIONS (
    column_name 'hasimage'
);
ALTER FOREIGN TABLE pasteque.products ALTER COLUMN discountenabled OPTIONS (
    column_name 'discountenabled'
);
ALTER FOREIGN TABLE pasteque.products ALTER COLUMN discountrate OPTIONS (
    column_name 'discountrate'
);
ALTER FOREIGN TABLE pasteque.products ALTER COLUMN prepay OPTIONS (
    column_name 'prepay'
);
ALTER FOREIGN TABLE pasteque.products ALTER COLUMN composition OPTIONS (
    column_name 'composition'
);


ALTER FOREIGN TABLE pasteque.products OWNER TO postgres;

--
-- Name: resources; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.resources (
    label character varying(255) NOT NULL,
    type integer NOT NULL,
    content bytea NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'resources'
);
ALTER FOREIGN TABLE pasteque.resources ALTER COLUMN label OPTIONS (
    column_name 'label'
);
ALTER FOREIGN TABLE pasteque.resources ALTER COLUMN type OPTIONS (
    column_name 'type'
);
ALTER FOREIGN TABLE pasteque.resources ALTER COLUMN content OPTIONS (
    column_name 'content'
);


ALTER FOREIGN TABLE pasteque.resources OWNER TO postgres;

--
-- Name: roles; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.roles (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    permissions text NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'roles'
);
ALTER FOREIGN TABLE pasteque.roles ALTER COLUMN id OPTIONS (
    column_name 'id'
);
ALTER FOREIGN TABLE pasteque.roles ALTER COLUMN name OPTIONS (
    column_name 'name'
);
ALTER FOREIGN TABLE pasteque.roles ALTER COLUMN permissions OPTIONS (
    column_name 'permissions'
);


ALTER FOREIGN TABLE pasteque.roles OWNER TO postgres;

--
-- Name: sessioncats; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.sessioncats (
    cashsession_id integer NOT NULL,
    reference character varying(255) NOT NULL,
    label character varying(255) NOT NULL,
    amount double precision NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'sessioncats'
);
ALTER FOREIGN TABLE pasteque.sessioncats ALTER COLUMN cashsession_id OPTIONS (
    column_name 'cashsession_id'
);
ALTER FOREIGN TABLE pasteque.sessioncats ALTER COLUMN reference OPTIONS (
    column_name 'reference'
);
ALTER FOREIGN TABLE pasteque.sessioncats ALTER COLUMN label OPTIONS (
    column_name 'label'
);
ALTER FOREIGN TABLE pasteque.sessioncats ALTER COLUMN amount OPTIONS (
    column_name 'amount'
);


ALTER FOREIGN TABLE pasteque.sessioncats OWNER TO postgres;

--
-- Name: sessioncattaxes; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.sessioncattaxes (
    cashsession_id integer NOT NULL,
    reference character varying(255) NOT NULL,
    tax_id integer NOT NULL,
    label character varying(255) NOT NULL,
    base double precision NOT NULL,
    amount double precision NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'sessioncattaxes'
);
ALTER FOREIGN TABLE pasteque.sessioncattaxes ALTER COLUMN cashsession_id OPTIONS (
    column_name 'cashsession_id'
);
ALTER FOREIGN TABLE pasteque.sessioncattaxes ALTER COLUMN reference OPTIONS (
    column_name 'reference'
);
ALTER FOREIGN TABLE pasteque.sessioncattaxes ALTER COLUMN tax_id OPTIONS (
    column_name 'tax_id'
);
ALTER FOREIGN TABLE pasteque.sessioncattaxes ALTER COLUMN label OPTIONS (
    column_name 'label'
);
ALTER FOREIGN TABLE pasteque.sessioncattaxes ALTER COLUMN base OPTIONS (
    column_name 'base'
);
ALTER FOREIGN TABLE pasteque.sessioncattaxes ALTER COLUMN amount OPTIONS (
    column_name 'amount'
);


ALTER FOREIGN TABLE pasteque.sessioncattaxes OWNER TO postgres;

--
-- Name: sessioncustbalances; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.sessioncustbalances (
    cashsession_id integer NOT NULL,
    customer_id integer NOT NULL,
    balance double precision NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'sessioncustbalances'
);
ALTER FOREIGN TABLE pasteque.sessioncustbalances ALTER COLUMN cashsession_id OPTIONS (
    column_name 'cashsession_id'
);
ALTER FOREIGN TABLE pasteque.sessioncustbalances ALTER COLUMN customer_id OPTIONS (
    column_name 'customer_id'
);
ALTER FOREIGN TABLE pasteque.sessioncustbalances ALTER COLUMN balance OPTIONS (
    column_name 'balance'
);


ALTER FOREIGN TABLE pasteque.sessioncustbalances OWNER TO postgres;

--
-- Name: sessionpayments; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.sessionpayments (
    cashsession_id integer NOT NULL,
    paymentmode_id integer NOT NULL,
    currency_id integer NOT NULL,
    amount double precision NOT NULL,
    currencyamount double precision NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'sessionpayments'
);
ALTER FOREIGN TABLE pasteque.sessionpayments ALTER COLUMN cashsession_id OPTIONS (
    column_name 'cashsession_id'
);
ALTER FOREIGN TABLE pasteque.sessionpayments ALTER COLUMN paymentmode_id OPTIONS (
    column_name 'paymentmode_id'
);
ALTER FOREIGN TABLE pasteque.sessionpayments ALTER COLUMN currency_id OPTIONS (
    column_name 'currency_id'
);
ALTER FOREIGN TABLE pasteque.sessionpayments ALTER COLUMN amount OPTIONS (
    column_name 'amount'
);
ALTER FOREIGN TABLE pasteque.sessionpayments ALTER COLUMN currencyamount OPTIONS (
    column_name 'currencyamount'
);


ALTER FOREIGN TABLE pasteque.sessionpayments OWNER TO postgres;

--
-- Name: sessions; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.sessions (
    id integer NOT NULL,
    cashregister_id integer NOT NULL,
    sequence integer NOT NULL,
    continuous boolean NOT NULL,
    opendate timestamp(0) without time zone,
    closedate timestamp(0) without time zone,
    opencash double precision,
    closecash double precision,
    expectedcash double precision,
    ticketcount integer,
    custcount integer,
    cs double precision,
    csperiod double precision NOT NULL,
    csfyear double precision NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'sessions'
);
ALTER FOREIGN TABLE pasteque.sessions ALTER COLUMN id OPTIONS (
    column_name 'id'
);
ALTER FOREIGN TABLE pasteque.sessions ALTER COLUMN cashregister_id OPTIONS (
    column_name 'cashregister_id'
);
ALTER FOREIGN TABLE pasteque.sessions ALTER COLUMN sequence OPTIONS (
    column_name 'sequence'
);
ALTER FOREIGN TABLE pasteque.sessions ALTER COLUMN continuous OPTIONS (
    column_name 'continuous'
);
ALTER FOREIGN TABLE pasteque.sessions ALTER COLUMN opendate OPTIONS (
    column_name 'opendate'
);
ALTER FOREIGN TABLE pasteque.sessions ALTER COLUMN closedate OPTIONS (
    column_name 'closedate'
);
ALTER FOREIGN TABLE pasteque.sessions ALTER COLUMN opencash OPTIONS (
    column_name 'opencash'
);
ALTER FOREIGN TABLE pasteque.sessions ALTER COLUMN closecash OPTIONS (
    column_name 'closecash'
);
ALTER FOREIGN TABLE pasteque.sessions ALTER COLUMN expectedcash OPTIONS (
    column_name 'expectedcash'
);
ALTER FOREIGN TABLE pasteque.sessions ALTER COLUMN ticketcount OPTIONS (
    column_name 'ticketcount'
);
ALTER FOREIGN TABLE pasteque.sessions ALTER COLUMN custcount OPTIONS (
    column_name 'custcount'
);
ALTER FOREIGN TABLE pasteque.sessions ALTER COLUMN cs OPTIONS (
    column_name 'cs'
);
ALTER FOREIGN TABLE pasteque.sessions ALTER COLUMN csperiod OPTIONS (
    column_name 'csperiod'
);
ALTER FOREIGN TABLE pasteque.sessions ALTER COLUMN csfyear OPTIONS (
    column_name 'csfyear'
);


ALTER FOREIGN TABLE pasteque.sessions OWNER TO postgres;

--
-- Name: sessiontaxes; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.sessiontaxes (
    cashsession_id integer NOT NULL,
    tax_id integer NOT NULL,
    taxrate double precision NOT NULL,
    base double precision NOT NULL,
    baseperiod double precision NOT NULL,
    basefyear double precision NOT NULL,
    amount double precision NOT NULL,
    amountperiod double precision NOT NULL,
    amountfyear double precision NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'sessiontaxes'
);
ALTER FOREIGN TABLE pasteque.sessiontaxes ALTER COLUMN cashsession_id OPTIONS (
    column_name 'cashsession_id'
);
ALTER FOREIGN TABLE pasteque.sessiontaxes ALTER COLUMN tax_id OPTIONS (
    column_name 'tax_id'
);
ALTER FOREIGN TABLE pasteque.sessiontaxes ALTER COLUMN taxrate OPTIONS (
    column_name 'taxrate'
);
ALTER FOREIGN TABLE pasteque.sessiontaxes ALTER COLUMN base OPTIONS (
    column_name 'base'
);
ALTER FOREIGN TABLE pasteque.sessiontaxes ALTER COLUMN baseperiod OPTIONS (
    column_name 'baseperiod'
);
ALTER FOREIGN TABLE pasteque.sessiontaxes ALTER COLUMN basefyear OPTIONS (
    column_name 'basefyear'
);
ALTER FOREIGN TABLE pasteque.sessiontaxes ALTER COLUMN amount OPTIONS (
    column_name 'amount'
);
ALTER FOREIGN TABLE pasteque.sessiontaxes ALTER COLUMN amountperiod OPTIONS (
    column_name 'amountperiod'
);
ALTER FOREIGN TABLE pasteque.sessiontaxes ALTER COLUMN amountfyear OPTIONS (
    column_name 'amountfyear'
);


ALTER FOREIGN TABLE pasteque.sessiontaxes OWNER TO postgres;

--
-- Name: tariffareaprices; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.tariffareaprices (
    tariffarea_id integer NOT NULL,
    product_id integer NOT NULL,
    tax_id integer,
    price double precision
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'tariffareaprices'
);
ALTER FOREIGN TABLE pasteque.tariffareaprices ALTER COLUMN tariffarea_id OPTIONS (
    column_name 'tariffarea_id'
);
ALTER FOREIGN TABLE pasteque.tariffareaprices ALTER COLUMN product_id OPTIONS (
    column_name 'product_id'
);
ALTER FOREIGN TABLE pasteque.tariffareaprices ALTER COLUMN tax_id OPTIONS (
    column_name 'tax_id'
);
ALTER FOREIGN TABLE pasteque.tariffareaprices ALTER COLUMN price OPTIONS (
    column_name 'price'
);


ALTER FOREIGN TABLE pasteque.tariffareaprices OWNER TO postgres;

--
-- Name: tariffareas; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.tariffareas (
    id integer NOT NULL,
    reference character varying(255) NOT NULL,
    label character varying(255) NOT NULL,
    disp_order integer NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'tariffareas'
);
ALTER FOREIGN TABLE pasteque.tariffareas ALTER COLUMN id OPTIONS (
    column_name 'id'
);
ALTER FOREIGN TABLE pasteque.tariffareas ALTER COLUMN reference OPTIONS (
    column_name 'reference'
);
ALTER FOREIGN TABLE pasteque.tariffareas ALTER COLUMN label OPTIONS (
    column_name 'label'
);
ALTER FOREIGN TABLE pasteque.tariffareas ALTER COLUMN disp_order OPTIONS (
    column_name 'disp_order'
);


ALTER FOREIGN TABLE pasteque.tariffareas OWNER TO postgres;

--
-- Name: taxes; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.taxes (
    id integer NOT NULL,
    label character varying(255) NOT NULL,
    rate double precision NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'taxes'
);
ALTER FOREIGN TABLE pasteque.taxes ALTER COLUMN id OPTIONS (
    column_name 'id'
);
ALTER FOREIGN TABLE pasteque.taxes ALTER COLUMN label OPTIONS (
    column_name 'label'
);
ALTER FOREIGN TABLE pasteque.taxes ALTER COLUMN rate OPTIONS (
    column_name 'rate'
);


ALTER FOREIGN TABLE pasteque.taxes OWNER TO postgres;

--
-- Name: ticketlines; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.ticketlines (
    ticket_id integer NOT NULL,
    product_id integer,
    tax_id integer NOT NULL,
    disporder integer NOT NULL,
    productlabel character varying(255) NOT NULL,
    unitprice double precision,
    taxedunitprice double precision,
    quantity double precision NOT NULL,
    price double precision,
    taxedprice double precision NOT NULL,
    taxrate double precision NOT NULL,
    discountrate double precision NOT NULL,
    finalprice double precision,
    finaltaxedprice double precision
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'ticketlines'
);
ALTER FOREIGN TABLE pasteque.ticketlines ALTER COLUMN ticket_id OPTIONS (
    column_name 'ticket_id'
);
ALTER FOREIGN TABLE pasteque.ticketlines ALTER COLUMN product_id OPTIONS (
    column_name 'product_id'
);
ALTER FOREIGN TABLE pasteque.ticketlines ALTER COLUMN tax_id OPTIONS (
    column_name 'tax_id'
);
ALTER FOREIGN TABLE pasteque.ticketlines ALTER COLUMN disporder OPTIONS (
    column_name 'disporder'
);
ALTER FOREIGN TABLE pasteque.ticketlines ALTER COLUMN productlabel OPTIONS (
    column_name 'productlabel'
);
ALTER FOREIGN TABLE pasteque.ticketlines ALTER COLUMN unitprice OPTIONS (
    column_name 'unitprice'
);
ALTER FOREIGN TABLE pasteque.ticketlines ALTER COLUMN taxedunitprice OPTIONS (
    column_name 'taxedunitprice'
);
ALTER FOREIGN TABLE pasteque.ticketlines ALTER COLUMN quantity OPTIONS (
    column_name 'quantity'
);
ALTER FOREIGN TABLE pasteque.ticketlines ALTER COLUMN price OPTIONS (
    column_name 'price'
);
ALTER FOREIGN TABLE pasteque.ticketlines ALTER COLUMN taxedprice OPTIONS (
    column_name 'taxedprice'
);
ALTER FOREIGN TABLE pasteque.ticketlines ALTER COLUMN taxrate OPTIONS (
    column_name 'taxrate'
);
ALTER FOREIGN TABLE pasteque.ticketlines ALTER COLUMN discountrate OPTIONS (
    column_name 'discountrate'
);
ALTER FOREIGN TABLE pasteque.ticketlines ALTER COLUMN finalprice OPTIONS (
    column_name 'finalprice'
);
ALTER FOREIGN TABLE pasteque.ticketlines ALTER COLUMN finaltaxedprice OPTIONS (
    column_name 'finaltaxedprice'
);


ALTER FOREIGN TABLE pasteque.ticketlines OWNER TO postgres;

--
-- Name: ticketpayments; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.ticketpayments (
    ticket_id integer NOT NULL,
    paymentmode_id integer NOT NULL,
    currency_id integer NOT NULL,
    disporder integer NOT NULL,
    amount double precision NOT NULL,
    currencyamount double precision NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'ticketpayments'
);
ALTER FOREIGN TABLE pasteque.ticketpayments ALTER COLUMN ticket_id OPTIONS (
    column_name 'ticket_id'
);
ALTER FOREIGN TABLE pasteque.ticketpayments ALTER COLUMN paymentmode_id OPTIONS (
    column_name 'paymentmode_id'
);
ALTER FOREIGN TABLE pasteque.ticketpayments ALTER COLUMN currency_id OPTIONS (
    column_name 'currency_id'
);
ALTER FOREIGN TABLE pasteque.ticketpayments ALTER COLUMN disporder OPTIONS (
    column_name 'disporder'
);
ALTER FOREIGN TABLE pasteque.ticketpayments ALTER COLUMN amount OPTIONS (
    column_name 'amount'
);
ALTER FOREIGN TABLE pasteque.ticketpayments ALTER COLUMN currencyamount OPTIONS (
    column_name 'currencyamount'
);


ALTER FOREIGN TABLE pasteque.ticketpayments OWNER TO postgres;

--
-- Name: tickets; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.tickets (
    id integer NOT NULL,
    cashregister_id integer NOT NULL,
    user_id integer NOT NULL,
    customer_id integer,
    tariffarea_id integer,
    discountprofile_id integer,
    sequence integer NOT NULL,
    number integer NOT NULL,
    date timestamp(0) without time zone NOT NULL,
    custcount integer,
    discountrate double precision NOT NULL,
    price double precision,
    taxedprice double precision,
    finalprice double precision NOT NULL,
    finaltaxedprice double precision NOT NULL,
    custbalance double precision NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'tickets'
);
ALTER FOREIGN TABLE pasteque.tickets ALTER COLUMN id OPTIONS (
    column_name 'id'
);
ALTER FOREIGN TABLE pasteque.tickets ALTER COLUMN cashregister_id OPTIONS (
    column_name 'cashregister_id'
);
ALTER FOREIGN TABLE pasteque.tickets ALTER COLUMN user_id OPTIONS (
    column_name 'user_id'
);
ALTER FOREIGN TABLE pasteque.tickets ALTER COLUMN customer_id OPTIONS (
    column_name 'customer_id'
);
ALTER FOREIGN TABLE pasteque.tickets ALTER COLUMN tariffarea_id OPTIONS (
    column_name 'tariffarea_id'
);
ALTER FOREIGN TABLE pasteque.tickets ALTER COLUMN discountprofile_id OPTIONS (
    column_name 'discountprofile_id'
);
ALTER FOREIGN TABLE pasteque.tickets ALTER COLUMN sequence OPTIONS (
    column_name 'sequence'
);
ALTER FOREIGN TABLE pasteque.tickets ALTER COLUMN number OPTIONS (
    column_name 'number'
);
ALTER FOREIGN TABLE pasteque.tickets ALTER COLUMN date OPTIONS (
    column_name 'date'
);
ALTER FOREIGN TABLE pasteque.tickets ALTER COLUMN custcount OPTIONS (
    column_name 'custcount'
);
ALTER FOREIGN TABLE pasteque.tickets ALTER COLUMN discountrate OPTIONS (
    column_name 'discountrate'
);
ALTER FOREIGN TABLE pasteque.tickets ALTER COLUMN price OPTIONS (
    column_name 'price'
);
ALTER FOREIGN TABLE pasteque.tickets ALTER COLUMN taxedprice OPTIONS (
    column_name 'taxedprice'
);
ALTER FOREIGN TABLE pasteque.tickets ALTER COLUMN finalprice OPTIONS (
    column_name 'finalprice'
);
ALTER FOREIGN TABLE pasteque.tickets ALTER COLUMN finaltaxedprice OPTIONS (
    column_name 'finaltaxedprice'
);
ALTER FOREIGN TABLE pasteque.tickets ALTER COLUMN custbalance OPTIONS (
    column_name 'custbalance'
);


ALTER FOREIGN TABLE pasteque.tickets OWNER TO postgres;

--
-- Name: tickettaxes; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.tickettaxes (
    ticket_id integer NOT NULL,
    tax_id integer NOT NULL,
    taxrate double precision NOT NULL,
    base double precision NOT NULL,
    amount double precision NOT NULL
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'tickettaxes'
);
ALTER FOREIGN TABLE pasteque.tickettaxes ALTER COLUMN ticket_id OPTIONS (
    column_name 'ticket_id'
);
ALTER FOREIGN TABLE pasteque.tickettaxes ALTER COLUMN tax_id OPTIONS (
    column_name 'tax_id'
);
ALTER FOREIGN TABLE pasteque.tickettaxes ALTER COLUMN taxrate OPTIONS (
    column_name 'taxrate'
);
ALTER FOREIGN TABLE pasteque.tickettaxes ALTER COLUMN base OPTIONS (
    column_name 'base'
);
ALTER FOREIGN TABLE pasteque.tickettaxes ALTER COLUMN amount OPTIONS (
    column_name 'amount'
);


ALTER FOREIGN TABLE pasteque.tickettaxes OWNER TO postgres;

--
-- Name: users; Type: FOREIGN TABLE; Schema: pasteque; Owner: postgres
--

CREATE FOREIGN TABLE pasteque.users (
    id integer NOT NULL,
    role_id integer NOT NULL,
    name character varying(255) NOT NULL,
    password character varying(255),
    active boolean NOT NULL,
    hasimage boolean NOT NULL,
    card character varying(255)
)
SERVER local_pasteque
OPTIONS (
    schema_name 'public',
    table_name 'users'
);
ALTER FOREIGN TABLE pasteque.users ALTER COLUMN id OPTIONS (
    column_name 'id'
);
ALTER FOREIGN TABLE pasteque.users ALTER COLUMN role_id OPTIONS (
    column_name 'role_id'
);
ALTER FOREIGN TABLE pasteque.users ALTER COLUMN name OPTIONS (
    column_name 'name'
);
ALTER FOREIGN TABLE pasteque.users ALTER COLUMN password OPTIONS (
    column_name 'password'
);
ALTER FOREIGN TABLE pasteque.users ALTER COLUMN active OPTIONS (
    column_name 'active'
);
ALTER FOREIGN TABLE pasteque.users ALTER COLUMN hasimage OPTIONS (
    column_name 'hasimage'
);
ALTER FOREIGN TABLE pasteque.users ALTER COLUMN card OPTIONS (
    column_name 'card'
);


ALTER FOREIGN TABLE pasteque.users OWNER TO postgres;

--
-- Name: configuration_setting; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.configuration_setting (
    key character varying NOT NULL,
    value character varying
);


ALTER TABLE public.configuration_setting OWNER TO expo;

--
-- Name: customer; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.customer (
    id integer NOT NULL,
    version integer DEFAULT 1 NOT NULL,
    firstname character varying(255),
    lastname character varying(255),
    email character varying(255),
    phone1 character varying(255),
    phone2 character varying(255),
    fax character varying(255),
    address character varying(512),
    zipcode character varying(255),
    city character varying(255),
    region character varying(255),
    country character varying(255),
    note text
);


ALTER TABLE public.customer OWNER TO expo;

--
-- Name: inventory; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.inventory (
    id integer NOT NULL,
    date date,
    comments text,
    version integer DEFAULT 1 NOT NULL,
    period_start date,
    inventory_type public.inventory_type DEFAULT 'continuous'::public.inventory_type NOT NULL
);


ALTER TABLE public.inventory OWNER TO expo;

--
-- Name: webshop_order_meta; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.webshop_order_meta (
    id integer NOT NULL,
    order_id character varying NOT NULL,
    print_count integer DEFAULT 0 NOT NULL,
    comment character varying,
    group_id integer
);


ALTER TABLE public.webshop_order_meta OWNER TO expo;

--
-- Name: newtable_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.newtable_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.newtable_id_seq OWNER TO expo;

--
-- Name: newtable_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.newtable_id_seq OWNED BY public.webshop_order_meta.id;


--
-- Name: payment_method; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.payment_method (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    code character varying(32) NOT NULL,
    attributes jsonb,
    display_name character varying,
    barcode character varying,
    polimod character varying,
    pasteque_reference character varying,
    pasteque_id integer
);


ALTER TABLE public.payment_method OWNER TO expo;

--
-- Name: payment_method_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.payment_method_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payment_method_id_seq OWNER TO expo;

--
-- Name: payment_method_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.payment_method_id_seq OWNED BY public.payment_method.id;


--
-- Name: product; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.product (
    pasteque_product_id integer,
    version integer DEFAULT 1 NOT NULL,
    number integer NOT NULL,
    category_code character varying(16),
    criteria character varying(16),
    name character varying(127),
    supplier character varying(100),
    barcode character varying(32),
    price double precision,
    import_date timestamp without time zone,
    attributes jsonb,
    label character varying(255),
    active boolean DEFAULT true,
    id integer NOT NULL,
    sku character varying
);


ALTER TABLE public.product OWNER TO expo;

--
-- Name: product_attribute; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.product_attribute (
    id integer NOT NULL,
    code character varying(128),
    name text,
    description text,
    type character varying NOT NULL
);


ALTER TABLE public.product_attribute OWNER TO expo;

--
-- Name: product_attribute_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.product_attribute_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_attribute_id_seq OWNER TO expo;

--
-- Name: product_attribute_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.product_attribute_id_seq OWNED BY public.product_attribute.id;


--
-- Name: product_attribute_value; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.product_attribute_value (
    id integer NOT NULL,
    attribute_id integer NOT NULL,
    value text NOT NULL,
    code character varying NOT NULL,
    color character varying,
    "position" integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.product_attribute_value OWNER TO expo;

--
-- Name: product_attribute_value_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.product_attribute_value_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_attribute_value_id_seq OWNER TO expo;

--
-- Name: product_attribute_value_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.product_attribute_value_id_seq OWNED BY public.product_attribute_value.id;


--
-- Name: product_category; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.product_category (
    code character varying(16) NOT NULL,
    version integer DEFAULT 1 NOT NULL,
    name character varying(127),
    rate numeric,
    comment character varying(255),
    pasteque_tax integer
);


ALTER TABLE public.product_category OWNER TO expo;

--
-- Name: product_variant; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.product_variant (
    id integer NOT NULL,
    product_id integer NOT NULL,
    name character varying(255) DEFAULT ''::character varying,
    code character varying,
    sku character varying
);


ALTER TABLE public.product_variant OWNER TO expo;

--
-- Name: product_variant_attribute_value; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.product_variant_attribute_value (
    id integer NOT NULL,
    variant_id integer NOT NULL,
    attribute_id integer NOT NULL,
    attribute_value_id integer NOT NULL
);


ALTER TABLE public.product_variant_attribute_value OWNER TO expo;

--
-- Name: product_variant_attribute_value_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.product_variant_attribute_value_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_variant_attribute_value_id_seq OWNER TO expo;

--
-- Name: product_variant_attribute_value_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.product_variant_attribute_value_id_seq OWNED BY public.product_variant_attribute_value.id;


--
-- Name: product_variant_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.product_variant_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_variant_id_seq OWNER TO expo;

--
-- Name: product_variant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.product_variant_id_seq OWNED BY public.product_variant.id;


--
-- Name: receipt; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.receipt (
    id integer NOT NULL,
    version integer DEFAULT 1 NOT NULL,
    ticket_id integer,
    customer_id integer,
    date date,
    number character varying(32),
    payment_details character varying(255),
    note text,
    ticket jsonb
);


ALTER TABLE public.receipt OWNER TO expo;

--
-- Name: report; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.report (
    id integer NOT NULL,
    name character varying(255),
    description text,
    version integer DEFAULT 1 NOT NULL,
    type character varying(32),
    datepicker character varying(32),
    sql text,
    template text
);


ALTER TABLE public.report OWNER TO expo;

--
-- Name: sales_journal; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.sales_journal (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    code character varying(32) NOT NULL,
    description text
);


ALTER TABLE public.sales_journal OWNER TO expo;

--
-- Name: sales_journal_entry; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.sales_journal_entry (
    id integer NOT NULL,
    date date NOT NULL,
    title text NOT NULL,
    sales_journal_id integer NOT NULL,
    payment_method_id integer NOT NULL,
    amount numeric
);


ALTER TABLE public.sales_journal_entry OWNER TO expo;

--
-- Name: sales_journal_entry_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.sales_journal_entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sales_journal_entry_id_seq OWNER TO expo;

--
-- Name: sales_journal_entry_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.sales_journal_entry_id_seq OWNED BY public.sales_journal_entry.id;


--
-- Name: sales_journal_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.sales_journal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sales_journal_id_seq OWNER TO expo;

--
-- Name: sales_journal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.sales_journal_id_seq OWNED BY public.sales_journal.id;


--
-- Name: sent_tickets; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.sent_tickets (
    id integer NOT NULL,
    date timestamp without time zone DEFAULT now() NOT NULL,
    data bytea
);


ALTER TABLE public.sent_tickets OWNER TO expo;

--
-- Name: sent_tickets_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.sent_tickets_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sent_tickets_id_seq OWNER TO expo;

--
-- Name: sent_tickets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.sent_tickets_id_seq OWNED BY public.sent_tickets.id;


--
-- Name: stock; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.stock (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    description text,
    version integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.stock OWNER TO expo;

--
-- Name: stock_adjustment; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.stock_adjustment (
    id bigint NOT NULL,
    operation_id integer NOT NULL,
    stock_item_id integer NOT NULL,
    product_id integer NOT NULL,
    variant_id integer,
    stock_item_variant_id integer,
    adjustment double precision NOT NULL,
    old_quantity double precision NOT NULL,
    new_quantity double precision NOT NULL,
    comment character varying,
    reference character varying
);


ALTER TABLE public.stock_adjustment OWNER TO expo;

--
-- Name: stock_adjustment_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.stock_adjustment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stock_adjustment_id_seq OWNER TO expo;

--
-- Name: stock_adjustment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.stock_adjustment_id_seq OWNED BY public.stock_adjustment.id;


--
-- Name: stock_count; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.stock_count (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    date timestamp with time zone DEFAULT CURRENT_DATE NOT NULL,
    inventory_id integer,
    stock_id integer NOT NULL,
    comment text,
    transferred timestamp with time zone,
    validated boolean DEFAULT false NOT NULL,
    operation_id integer
);


ALTER TABLE public.stock_count OWNER TO expo;

--
-- Name: stock_count_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.stock_count_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stock_count_id_seq OWNER TO expo;

--
-- Name: stock_count_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.stock_count_id_seq OWNED BY public.stock_count.id;


--
-- Name: stock_count_item; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.stock_count_item (
    id integer NOT NULL,
    stock_item_id integer NOT NULL,
    stock_count_id integer NOT NULL,
    quantity double precision DEFAULT 0 NOT NULL,
    comment text,
    stock_item_variant_id integer,
    stock_id integer NOT NULL,
    updated_at timestamp with time zone DEFAULT CURRENT_DATE NOT NULL
);


ALTER TABLE public.stock_count_item OWNER TO expo;

--
-- Name: stock_count_item_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.stock_count_item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stock_count_item_id_seq OWNER TO expo;

--
-- Name: stock_count_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.stock_count_item_id_seq OWNED BY public.stock_count_item.id;


--
-- Name: stock_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.stock_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stock_id_seq OWNER TO expo;

--
-- Name: stock_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.stock_id_seq OWNED BY public.stock.id;


--
-- Name: stock_item; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.stock_item (
    id integer NOT NULL,
    stock_id integer NOT NULL,
    product_id integer NOT NULL,
    quantity double precision DEFAULT 0 NOT NULL,
    notes text
);


ALTER TABLE public.stock_item OWNER TO expo;

--
-- Name: stock_item_variant; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.stock_item_variant (
    id integer NOT NULL,
    stock_item_id integer NOT NULL,
    variant_id integer,
    quantity double precision DEFAULT 0 NOT NULL,
    notes character varying
);


ALTER TABLE public.stock_item_variant OWNER TO expo;

--
-- Name: stock_item_variant_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.stock_item_variant_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stock_item_variant_id_seq OWNER TO expo;

--
-- Name: stock_item_variant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.stock_item_variant_id_seq OWNED BY public.stock_item_variant.id;


--
-- Name: stock_movement; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.stock_movement (
    id integer NOT NULL,
    stock_id integer NOT NULL,
    name character varying(255) NOT NULL,
    date timestamp with time zone DEFAULT CURRENT_DATE NOT NULL,
    comment text,
    executed timestamp with time zone,
    destination_stock_id integer NOT NULL,
    operation_id integer,
    destination_operation_id integer
);


ALTER TABLE public.stock_movement OWNER TO expo;

--
-- Name: stock_movement_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.stock_movement_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stock_movement_id_seq OWNER TO expo;

--
-- Name: stock_movement_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.stock_movement_id_seq OWNED BY public.stock_movement.id;


--
-- Name: stock_movement_item; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.stock_movement_item (
    id integer NOT NULL,
    stock_id integer NOT NULL,
    stock_movement_id integer NOT NULL,
    stock_item_id integer NOT NULL,
    stock_item_variant_id integer,
    quantity double precision DEFAULT 0 NOT NULL,
    updated_at timestamp with time zone DEFAULT CURRENT_DATE NOT NULL,
    comment text
);


ALTER TABLE public.stock_movement_item OWNER TO expo;

--
-- Name: stock_movement_item_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.stock_movement_item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stock_movement_item_id_seq OWNER TO expo;

--
-- Name: stock_movement_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.stock_movement_item_id_seq OWNED BY public.stock_movement_item.id;


--
-- Name: stock_operation; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.stock_operation (
    id integer NOT NULL,
    stock_id integer NOT NULL,
    type public.stock_operation_type NOT NULL,
    name character varying,
    date timestamp(0) with time zone,
    comment character varying,
    reference character varying,
    created_by character varying NOT NULL
);


ALTER TABLE public.stock_operation OWNER TO expo;

--
-- Name: stock_operation_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.stock_operation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stock_operation_id_seq OWNER TO expo;

--
-- Name: stock_operation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.stock_operation_id_seq OWNED BY public.stock_operation.id;


--
-- Name: stock_product_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.stock_product_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stock_product_id_seq OWNER TO expo;

--
-- Name: stock_product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.stock_product_id_seq OWNED BY public.stock_item.id;


--
-- Name: tz_customer; Type: VIEW; Schema: public; Owner: expo
--

CREATE VIEW public.tz_customer AS
 SELECT customer.id,
    customer.version,
    customer.firstname,
    customer.lastname,
    customer.email,
    customer.phone1,
    customer.phone2,
    customer.fax,
    customer.address,
    customer.zipcode,
    customer.city,
    customer.region,
    customer.country,
    customer.note
   FROM public.customer;


ALTER TABLE public.tz_customer OWNER TO expo;

--
-- Name: tz_customer_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.tz_customer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tz_customer_id_seq OWNER TO expo;

--
-- Name: tz_customer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.tz_customer_id_seq OWNED BY public.customer.id;


--
-- Name: tz_inventory; Type: VIEW; Schema: public; Owner: expo
--

CREATE VIEW public.tz_inventory AS
 SELECT inventory.id,
    inventory.date,
    inventory.comments,
    inventory.version,
    inventory.period_start,
    inventory.inventory_type
   FROM public.inventory;


ALTER TABLE public.tz_inventory OWNER TO expo;

--
-- Name: tz_inventory_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.tz_inventory_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tz_inventory_id_seq OWNER TO expo;

--
-- Name: tz_inventory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.tz_inventory_id_seq OWNED BY public.inventory.id;


--
-- Name: tz_product; Type: VIEW; Schema: public; Owner: expo
--

CREATE VIEW public.tz_product AS
 SELECT product.pasteque_product_id,
    product.version,
    product.number,
    product.category_code,
    product.criteria,
    product.name,
    product.supplier,
    product.barcode,
    product.price,
    product.import_date,
    product.attributes,
    product.label,
    product.active,
    product.id
   FROM public.product;


ALTER TABLE public.tz_product OWNER TO expo;

--
-- Name: tz_product_bkp; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.tz_product_bkp (
    pasteque_product_id integer,
    version integer,
    number integer,
    category_code character varying(16),
    criteria character varying(16),
    name character varying(127),
    supplier character varying(100),
    barcode character varying(32),
    price double precision,
    import_date timestamp without time zone,
    attributes jsonb,
    label character varying(255),
    active boolean,
    id bigint
);


ALTER TABLE public.tz_product_bkp OWNER TO expo;

--
-- Name: tz_product_category; Type: VIEW; Schema: public; Owner: expo
--

CREATE VIEW public.tz_product_category AS
 SELECT product_category.code,
    product_category.version,
    product_category.name,
    product_category.rate,
    product_category.comment
   FROM public.product_category;


ALTER TABLE public.tz_product_category OWNER TO expo;

--
-- Name: tz_product_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.tz_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tz_product_id_seq OWNER TO expo;

--
-- Name: tz_product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.tz_product_id_seq OWNED BY public.product.id;


--
-- Name: tz_receipt; Type: VIEW; Schema: public; Owner: expo
--

CREATE VIEW public.tz_receipt AS
 SELECT receipt.id,
    receipt.version,
    receipt.ticket_id,
    receipt.customer_id,
    receipt.date,
    receipt.number,
    receipt.payment_details,
    receipt.note,
    receipt.ticket
   FROM public.receipt;


ALTER TABLE public.tz_receipt OWNER TO expo;

--
-- Name: tz_receipt_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.tz_receipt_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tz_receipt_id_seq OWNER TO expo;

--
-- Name: tz_receipt_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.tz_receipt_id_seq OWNED BY public.receipt.id;


--
-- Name: tz_report; Type: VIEW; Schema: public; Owner: expo
--

CREATE VIEW public.tz_report AS
 SELECT report.id,
    report.name,
    report.description,
    report.version,
    report.type,
    report.datepicker,
    report.sql,
    report.template
   FROM public.report;


ALTER TABLE public.tz_report OWNER TO expo;

--
-- Name: tz_report_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.tz_report_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tz_report_id_seq OWNER TO expo;

--
-- Name: tz_report_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.tz_report_id_seq OWNED BY public.report.id;


--
-- Name: tz_vw_inventory_with_start_date; Type: VIEW; Schema: public; Owner: expo
--

CREATE VIEW public.tz_vw_inventory_with_start_date AS
 SELECT i.id,
    i.date,
    i.period_start,
    i.inventory_type,
    i.comments,
    i.version,
    ((COALESCE(lead(i.date) OVER (ORDER BY i.date DESC), '2018-12-31'::date) + '1 day'::interval))::date AS start_date
   FROM public.inventory i
  WHERE (i.inventory_type = 'continuous'::public.inventory_type)
UNION ALL
 SELECT i.id,
    i.date,
    i.period_start,
    i.inventory_type,
    i.comments,
    i.version,
    i.period_start AS start_date
   FROM public.inventory i
  WHERE (i.inventory_type = 'special'::public.inventory_type)
  ORDER BY 2;


ALTER TABLE public.tz_vw_inventory_with_start_date OWNER TO expo;

--
-- Name: tz_vw_product_variants; Type: VIEW; Schema: public; Owner: expo
--

CREATE VIEW public.tz_vw_product_variants AS
SELECT
    NULL::integer AS product_id,
    NULL::integer AS variant_id,
    NULL::text AS full_name,
    NULL::text AS variant_name,
    NULL::numeric AS tax_rate,
    NULL::text AS category,
    NULL::integer AS pasteque_product_id,
    NULL::integer AS version,
    NULL::integer AS number,
    NULL::character varying(16) AS category_code,
    NULL::character varying(16) AS criteria,
    NULL::character varying(127) AS name,
    NULL::character varying(100) AS supplier,
    NULL::character varying(32) AS barcode,
    NULL::double precision AS price,
    NULL::timestamp without time zone AS import_date,
    NULL::jsonb AS attributes,
    NULL::character varying(255) AS label,
    NULL::boolean AS active,
    NULL::integer AS id,
    NULL::json AS attribute_values,
    NULL::boolean AS has_pasteque;


ALTER TABLE public.tz_vw_product_variants OWNER TO expo;

--
-- Name: tz_vw_products; Type: VIEW; Schema: public; Owner: expo
--

CREATE VIEW public.tz_vw_products AS
SELECT
    NULL::integer AS id,
    NULL::integer AS pasteque_product_id,
    NULL::integer AS version,
    NULL::integer AS number,
    NULL::character varying(16) AS category_code,
    NULL::character varying(16) AS criteria,
    NULL::character varying(127) AS name,
    NULL::character varying(100) AS supplier,
    NULL::character varying(32) AS barcode,
    NULL::double precision AS price,
    NULL::timestamp without time zone AS import_date,
    NULL::jsonb AS attributes,
    NULL::character varying(255) AS label,
    NULL::boolean AS active,
    NULL::numeric AS tax_rate,
    NULL::text AS category,
    NULL::boolean AS has_pasteque,
    NULL::json AS variants;


ALTER TABLE public.tz_vw_products OWNER TO expo;

--
-- Name: tz_vw_tickets; Type: VIEW; Schema: public; Owner: expo
--

CREATE VIEW public.tz_vw_tickets AS
 SELECT tickets.id,
    tickets.cashregister_id,
    tickets.user_id,
    tickets.customer_id,
    tickets.tariffarea_id,
    tickets.discountprofile_id,
    tickets.sequence,
    tickets.number,
    tickets.date,
    tickets.custcount,
    tickets.discountrate,
    tickets.price,
    tickets.taxedprice,
    tickets.finalprice,
    tickets.finaltaxedprice,
    tickets.custbalance,
    timezone('Europe/Paris'::text, timezone('utc'::text, tickets.date)) AS local_date
   FROM pasteque.tickets;


ALTER TABLE public.tz_vw_tickets OWNER TO expo;

--
-- Name: vw_inventory_with_start_date; Type: VIEW; Schema: public; Owner: expo
--

CREATE VIEW public.vw_inventory_with_start_date AS
 SELECT i.id,
    i.date,
    i.period_start,
    i.inventory_type,
    i.comments,
    i.version,
    ((COALESCE(lead(i.date) OVER (ORDER BY i.date DESC), '2018-12-31'::date) + '1 day'::interval))::date AS start_date
   FROM public.inventory i
  WHERE (i.inventory_type = 'continuous'::public.inventory_type)
UNION ALL
 SELECT i.id,
    i.date,
    i.period_start,
    i.inventory_type,
    i.comments,
    i.version,
    i.period_start AS start_date
   FROM public.inventory i
  WHERE (i.inventory_type = 'special'::public.inventory_type)
  ORDER BY 2;


ALTER TABLE public.vw_inventory_with_start_date OWNER TO expo;

--
-- Name: vw_product; Type: VIEW; Schema: public; Owner: expo
--

CREATE VIEW public.vw_product AS
 SELECT product.id,
    public.test_slow() AS test_slow
   FROM public.product;


ALTER TABLE public.vw_product OWNER TO expo;

--
-- Name: vw_product_attributes; Type: VIEW; Schema: public; Owner: expo
--

CREATE VIEW public.vw_product_attributes AS
SELECT
    NULL::integer AS id,
    NULL::character varying(128) AS code,
    NULL::text AS description,
    NULL::text AS name,
    NULL::character varying AS type,
    NULL::json AS "values";


ALTER TABLE public.vw_product_attributes OWNER TO expo;

--
-- Name: vw_product_variants; Type: VIEW; Schema: public; Owner: expo
--

CREATE VIEW public.vw_product_variants AS
SELECT
    NULL::integer AS product_id,
    NULL::integer AS variant_id,
    NULL::text AS full_name,
    NULL::text AS variant_name,
    NULL::character varying(255) AS name,
    NULL::character varying AS sku,
    NULL::character varying AS variant_code,
    NULL::numeric AS tax_rate,
    NULL::text AS category,
    NULL::integer AS pasteque_product_id,
    NULL::integer AS version,
    NULL::integer AS number,
    NULL::character varying(16) AS category_code,
    NULL::character varying(16) AS criteria,
    NULL::character varying(127) AS product_name,
    NULL::character varying AS product_sku,
    NULL::character varying(100) AS supplier,
    NULL::character varying(32) AS barcode,
    NULL::double precision AS price,
    NULL::timestamp without time zone AS import_date,
    NULL::jsonb AS attributes,
    NULL::character varying(255) AS label,
    NULL::boolean AS active,
    NULL::integer AS id,
    NULL::integer AS "position",
    NULL::json AS attribute_values,
    NULL::boolean AS has_pasteque;


ALTER TABLE public.vw_product_variants OWNER TO expo;

--
-- Name: vw_tickets; Type: VIEW; Schema: public; Owner: expo
--

CREATE VIEW public.vw_tickets AS
 SELECT DISTINCT ON (t.id) t.id,
    t.cashregister_id,
    t.user_id,
    t.customer_id,
    t.tariffarea_id,
    t.discountprofile_id,
    t.sequence,
    t.number,
    t.date,
    t.custcount,
    t.discountrate,
    t.taxedprice,
    t.price,
    t.finalprice,
    t.finaltaxedprice,
    t.custbalance,
    t.finaltaxedprice AS total,
    t.finalprice AS total_ht,
    c.reference AS cash_register,
    sum(tl.quantity) AS items,
    ( SELECT json_agg(tt.*) AS json_agg
           FROM pasteque.tickettaxes tt
          WHERE (tt.ticket_id = t.id)) AS taxes,
    ( SELECT string_agg((pm.reference)::text, ','::text) AS string_agg
           FROM (pasteque.ticketpayments tp
             LEFT JOIN pasteque.paymentmodes pm ON ((pm.id = tp.paymentmode_id)))
          WHERE (tp.ticket_id = t.id)) AS payment_mode,
    json_agg(tl.*) AS lines
   FROM ((pasteque.tickets t
     LEFT JOIN pasteque.cashregisters c ON ((c.id = t.cashregister_id)))
     LEFT JOIN pasteque.ticketlines tl ON ((tl.ticket_id = t.id)))
  GROUP BY t.id, t.cashregister_id, t.user_id, t.customer_id, t.tariffarea_id, t.discountprofile_id, t.sequence, t.number, t.date, t.custcount, t.discountrate, t.taxedprice, t.price, t.finalprice, t.finaltaxedprice, t.custbalance, c.reference, tl.disporder
  ORDER BY t.id, tl.disporder;


ALTER TABLE public.vw_tickets OWNER TO expo;

--
-- Name: webshop_order_group; Type: TABLE; Schema: public; Owner: expo
--

CREATE TABLE public.webshop_order_group (
    id integer NOT NULL,
    name character varying NOT NULL,
    color character varying NOT NULL
);


ALTER TABLE public.webshop_order_group OWNER TO expo;

--
-- Name: webshop_order_group_id_seq; Type: SEQUENCE; Schema: public; Owner: expo
--

CREATE SEQUENCE public.webshop_order_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.webshop_order_group_id_seq OWNER TO expo;

--
-- Name: webshop_order_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: expo
--

ALTER SEQUENCE public.webshop_order_group_id_seq OWNED BY public.webshop_order_group.id;


--
-- Name: logged_actions event_id; Type: DEFAULT; Schema: audit; Owner: expo
--

ALTER TABLE ONLY audit.logged_actions ALTER COLUMN event_id SET DEFAULT nextval('audit.logged_actions_event_id_seq'::regclass);


--
-- Name: customer id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.customer ALTER COLUMN id SET DEFAULT nextval('public.tz_customer_id_seq'::regclass);


--
-- Name: inventory id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.inventory ALTER COLUMN id SET DEFAULT nextval('public.tz_inventory_id_seq'::regclass);


--
-- Name: payment_method id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.payment_method ALTER COLUMN id SET DEFAULT nextval('public.payment_method_id_seq'::regclass);


--
-- Name: product id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.product ALTER COLUMN id SET DEFAULT nextval('public.tz_product_id_seq'::regclass);


--
-- Name: product_attribute id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.product_attribute ALTER COLUMN id SET DEFAULT nextval('public.product_attribute_id_seq'::regclass);


--
-- Name: product_attribute_value id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.product_attribute_value ALTER COLUMN id SET DEFAULT nextval('public.product_attribute_value_id_seq'::regclass);


--
-- Name: product_variant id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.product_variant ALTER COLUMN id SET DEFAULT nextval('public.product_variant_id_seq'::regclass);


--
-- Name: product_variant_attribute_value id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.product_variant_attribute_value ALTER COLUMN id SET DEFAULT nextval('public.product_variant_attribute_value_id_seq'::regclass);


--
-- Name: receipt id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.receipt ALTER COLUMN id SET DEFAULT nextval('public.tz_receipt_id_seq'::regclass);


--
-- Name: report id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.report ALTER COLUMN id SET DEFAULT nextval('public.tz_report_id_seq'::regclass);


--
-- Name: sales_journal id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.sales_journal ALTER COLUMN id SET DEFAULT nextval('public.sales_journal_id_seq'::regclass);


--
-- Name: sales_journal_entry id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.sales_journal_entry ALTER COLUMN id SET DEFAULT nextval('public.sales_journal_entry_id_seq'::regclass);


--
-- Name: sent_tickets id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.sent_tickets ALTER COLUMN id SET DEFAULT nextval('public.sent_tickets_id_seq'::regclass);


--
-- Name: stock id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock ALTER COLUMN id SET DEFAULT nextval('public.stock_id_seq'::regclass);


--
-- Name: stock_adjustment id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_adjustment ALTER COLUMN id SET DEFAULT nextval('public.stock_adjustment_id_seq'::regclass);


--
-- Name: stock_count id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_count ALTER COLUMN id SET DEFAULT nextval('public.stock_count_id_seq'::regclass);


--
-- Name: stock_count_item id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_count_item ALTER COLUMN id SET DEFAULT nextval('public.stock_count_item_id_seq'::regclass);


--
-- Name: stock_item id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_item ALTER COLUMN id SET DEFAULT nextval('public.stock_product_id_seq'::regclass);


--
-- Name: stock_item_variant id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_item_variant ALTER COLUMN id SET DEFAULT nextval('public.stock_item_variant_id_seq'::regclass);


--
-- Name: stock_movement id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_movement ALTER COLUMN id SET DEFAULT nextval('public.stock_movement_id_seq'::regclass);


--
-- Name: stock_movement_item id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_movement_item ALTER COLUMN id SET DEFAULT nextval('public.stock_movement_item_id_seq'::regclass);


--
-- Name: stock_operation id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_operation ALTER COLUMN id SET DEFAULT nextval('public.stock_operation_id_seq'::regclass);


--
-- Name: webshop_order_group id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.webshop_order_group ALTER COLUMN id SET DEFAULT nextval('public.webshop_order_group_id_seq'::regclass);


--
-- Name: webshop_order_meta id; Type: DEFAULT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.webshop_order_meta ALTER COLUMN id SET DEFAULT nextval('public.newtable_id_seq'::regclass);


--
-- Name: logged_actions logged_actions_pkey; Type: CONSTRAINT; Schema: audit; Owner: expo
--

ALTER TABLE ONLY audit.logged_actions
    ADD CONSTRAINT logged_actions_pkey PRIMARY KEY (event_id);


--
-- Name: customer customer_pkey; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (id);


--
-- Name: inventory inventory_pkey; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.inventory
    ADD CONSTRAINT inventory_pkey PRIMARY KEY (id);


--
-- Name: payment_method payment_method_pkey; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.payment_method
    ADD CONSTRAINT payment_method_pkey PRIMARY KEY (id);


--
-- Name: product_attribute product_attribute_pkey; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.product_attribute
    ADD CONSTRAINT product_attribute_pkey PRIMARY KEY (id);


--
-- Name: product_attribute_value product_attribute_value_pkey; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.product_attribute_value
    ADD CONSTRAINT product_attribute_value_pkey PRIMARY KEY (id);


--
-- Name: product_attribute_value product_attribute_value_un_attr_code; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.product_attribute_value
    ADD CONSTRAINT product_attribute_value_un_attr_code UNIQUE (attribute_id, code);


--
-- Name: product_attribute_value product_attribute_value_un_attr_val; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.product_attribute_value
    ADD CONSTRAINT product_attribute_value_un_attr_val UNIQUE (attribute_id, value);


--
-- Name: product_attribute_value product_attribute_value_un_id_attr; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.product_attribute_value
    ADD CONSTRAINT product_attribute_value_un_id_attr UNIQUE (id, attribute_id);


--
-- Name: product_category product_category_pkey; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.product_category
    ADD CONSTRAINT product_category_pkey PRIMARY KEY (code);


--
-- Name: product_category product_category_un; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.product_category
    ADD CONSTRAINT product_category_un UNIQUE (code);


--
-- Name: product product_pk; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_pk PRIMARY KEY (id);


--
-- Name: product product_un_number; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_un_number UNIQUE (number);


--
-- Name: product_variant_attribute_value product_variant_attribute_val_product_variant_id_attribute__key; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.product_variant_attribute_value
    ADD CONSTRAINT product_variant_attribute_val_product_variant_id_attribute__key UNIQUE (variant_id, attribute_id);


--
-- Name: product_variant_attribute_value product_variant_attribute_value_pkey; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.product_variant_attribute_value
    ADD CONSTRAINT product_variant_attribute_value_pkey PRIMARY KEY (id);


--
-- Name: product_variant product_variant_pkey; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.product_variant
    ADD CONSTRAINT product_variant_pkey PRIMARY KEY (id);


--
-- Name: product_variant product_variant_un_id_product_id; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.product_variant
    ADD CONSTRAINT product_variant_un_id_product_id UNIQUE (product_id, id);


--
-- Name: receipt receipt_number_key; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.receipt
    ADD CONSTRAINT receipt_number_key UNIQUE (number);


--
-- Name: receipt receipt_pkey; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.receipt
    ADD CONSTRAINT receipt_pkey PRIMARY KEY (id);


--
-- Name: report report_pkey; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.report
    ADD CONSTRAINT report_pkey PRIMARY KEY (id);


--
-- Name: sales_journal_entry sales_journal_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.sales_journal_entry
    ADD CONSTRAINT sales_journal_entry_pkey PRIMARY KEY (id);


--
-- Name: sales_journal_entry sales_journal_entry_un; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.sales_journal_entry
    ADD CONSTRAINT sales_journal_entry_un UNIQUE (sales_journal_id, payment_method_id, date);


--
-- Name: sales_journal sales_journal_pkey; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.sales_journal
    ADD CONSTRAINT sales_journal_pkey PRIMARY KEY (id);


--
-- Name: sent_tickets sent_tickets_pkey; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.sent_tickets
    ADD CONSTRAINT sent_tickets_pkey PRIMARY KEY (id);


--
-- Name: stock_adjustment stock_adjustment_pk; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_adjustment
    ADD CONSTRAINT stock_adjustment_pk PRIMARY KEY (id);


--
-- Name: stock_count_item stock_count_item_pkey; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_count_item
    ADD CONSTRAINT stock_count_item_pkey PRIMARY KEY (id);


--
-- Name: stock_count stock_count_pkey; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_count
    ADD CONSTRAINT stock_count_pkey PRIMARY KEY (id);


--
-- Name: stock_item_variant stock_item_variant_pk; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_item_variant
    ADD CONSTRAINT stock_item_variant_pk PRIMARY KEY (id);


--
-- Name: stock_item_variant stock_item_variant_un_item_variant; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_item_variant
    ADD CONSTRAINT stock_item_variant_un_item_variant UNIQUE (stock_item_id, variant_id);


--
-- Name: stock_movement_item stock_movement_item_pkey; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_movement_item
    ADD CONSTRAINT stock_movement_item_pkey PRIMARY KEY (id);


--
-- Name: stock_movement stock_movement_pkey; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_movement
    ADD CONSTRAINT stock_movement_pkey PRIMARY KEY (id);


--
-- Name: stock_operation stock_operation_pk; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_operation
    ADD CONSTRAINT stock_operation_pk PRIMARY KEY (id);


--
-- Name: stock stock_pkey; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock
    ADD CONSTRAINT stock_pkey PRIMARY KEY (id);


--
-- Name: stock_item stock_product_pkey; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_item
    ADD CONSTRAINT stock_product_pkey PRIMARY KEY (id);


--
-- Name: stock_item stock_product_un_stock_product; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_item
    ADD CONSTRAINT stock_product_un_stock_product UNIQUE (product_id, stock_id);


--
-- Name: webshop_order_group webshop_order_group_pk; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.webshop_order_group
    ADD CONSTRAINT webshop_order_group_pk PRIMARY KEY (id);


--
-- Name: webshop_order_group webshop_order_group_un; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.webshop_order_group
    ADD CONSTRAINT webshop_order_group_un UNIQUE (name);


--
-- Name: webshop_order_group webshop_order_group_un2; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.webshop_order_group
    ADD CONSTRAINT webshop_order_group_un2 UNIQUE (color);


--
-- Name: webshop_order_meta webshop_order_meta_pk; Type: CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.webshop_order_meta
    ADD CONSTRAINT webshop_order_meta_pk PRIMARY KEY (id);


--
-- Name: logged_actions_action_idx; Type: INDEX; Schema: audit; Owner: expo
--

CREATE INDEX logged_actions_action_idx ON audit.logged_actions USING btree (action);


--
-- Name: logged_actions_action_tstamp_tx_stm_idx; Type: INDEX; Schema: audit; Owner: expo
--

CREATE INDEX logged_actions_action_tstamp_tx_stm_idx ON audit.logged_actions USING btree (action_tstamp_stm);


--
-- Name: logged_actions_relid_idx; Type: INDEX; Schema: audit; Owner: expo
--

CREATE INDEX logged_actions_relid_idx ON audit.logged_actions USING btree (relid);


--
-- Name: stock_adjustment_reference_idx; Type: INDEX; Schema: public; Owner: expo
--

CREATE INDEX stock_adjustment_reference_idx ON public.stock_adjustment USING btree (reference);


--
-- Name: stock_count_item_un_item_variant_idx; Type: INDEX; Schema: public; Owner: expo
--

CREATE UNIQUE INDEX stock_count_item_un_item_variant_idx ON public.stock_count_item USING btree (stock_count_id, stock_item_id, COALESCE(stock_item_variant_id, '-1'::integer));


--
-- Name: stock_movement_item_un_item_variant_idx; Type: INDEX; Schema: public; Owner: expo
--

CREATE UNIQUE INDEX stock_movement_item_un_item_variant_idx ON public.stock_movement_item USING btree (stock_movement_id, stock_item_id, COALESCE(stock_item_variant_id, '-1'::integer));


--
-- Name: webshop_order_meta_order_id_idx; Type: INDEX; Schema: public; Owner: expo
--

CREATE UNIQUE INDEX webshop_order_meta_order_id_idx ON public.webshop_order_meta USING btree (order_id);


--
-- Name: tz_vw_product_variants _RETURN; Type: RULE; Schema: public; Owner: expo
--

CREATE OR REPLACE VIEW public.tz_vw_product_variants AS
 SELECT p.id AS product_id,
    v.id AS variant_id,
    concat(p.name, COALESCE(NULLIF(max((v.name)::text), ''::text), ((
        CASE
            WHEN (v.id IS NULL) THEN ''::text
            ELSE concat(' - ', string_agg(attv.value, ','::text))
        END)::character varying)::text)) AS full_name,
    COALESCE(max((v.name)::text), ((string_agg(attv.value, ','::text))::character varying)::text) AS variant_name,
    max(c.rate) AS tax_rate,
    max((c.name)::text) AS category,
    p.pasteque_product_id,
    p.version,
    p.number,
    p.category_code,
    p.criteria,
    p.name,
    p.supplier,
    p.barcode,
    p.price,
    p.import_date,
    p.attributes,
    p.label,
    p.active,
    p.id,
    json_agg(json_build_object('attribute_name', att.name, 'attribute_id', att.id, 'value_id', av.id, 'code', attv.code, 'value', attv.value, 'color',
        CASE att.type
            WHEN 'color'::text THEN attv.color
            ELSE NULL::character varying
        END)) FILTER (WHERE (av.id IS NOT NULL)) AS attribute_values,
    ( SELECT (EXISTS ( SELECT pp.id
                   FROM pasteque.products pp
                  WHERE (pp.id = p.number))) AS "exists") AS has_pasteque
   FROM (((((public.product p
     LEFT JOIN ( SELECT v_1.id,
            v_1.product_id,
            v_1.name
           FROM public.product_variant v_1
        UNION ALL
         SELECT NULL::integer AS int4,
            ( SELECT DISTINCT vx.product_id
                   FROM public.product_variant vx) AS product_id,
            NULL::character varying AS "varchar") v ON ((v.product_id = p.id)))
     LEFT JOIN public.product_variant_attribute_value av ON ((av.variant_id = v.id)))
     LEFT JOIN public.product_attribute_value attv ON ((av.attribute_value_id = attv.id)))
     LEFT JOIN public.product_attribute att ON ((att.id = av.attribute_id)))
     LEFT JOIN public.product_category c ON (((p.category_code)::text = (c.code)::text)))
  GROUP BY p.id, v.id
  ORDER BY p.number, v.id;


--
-- Name: tz_vw_products _RETURN; Type: RULE; Schema: public; Owner: expo
--

CREATE OR REPLACE VIEW public.tz_vw_products AS
 SELECT p.id,
    p.pasteque_product_id,
    p.version,
    p.number,
    p.category_code,
    p.criteria,
    p.name,
    p.supplier,
    p.barcode,
    p.price,
    p.import_date,
    p.attributes,
    p.label,
    p.active,
    max(c.rate) AS tax_rate,
    max((c.name)::text) AS category,
    ( SELECT (EXISTS ( SELECT pp.id
                   FROM pasteque.products pp
                  WHERE (pp.id = p.number))) AS "exists") AS has_pasteque,
    ( SELECT json_agg(json_build_object('name', COALESCE(v.name, (v.default_name)::character varying), 'id', v.id, 'attributes', v.attributes)) AS json_agg
           FROM ( SELECT v_1.id,
                    v_1.product_id,
                    v_1.name,
                    json_agg(json_build_object('attribute_name', att.name, 'attribute_id', att.id, 'value_id', av.id, 'code', attv.code, 'value', attv.value, 'color',
                        CASE att.type
                            WHEN 'color'::text THEN attv.color
                            ELSE NULL::character varying
                        END)) AS attributes,
                    string_agg(attv.value, ','::text) AS default_name
                   FROM (((public.product_variant v_1
                     LEFT JOIN public.product_variant_attribute_value av ON ((av.variant_id = v_1.id)))
                     LEFT JOIN public.product_attribute_value attv ON ((av.attribute_value_id = attv.id)))
                     LEFT JOIN public.product_attribute att ON ((att.id = av.attribute_id)))
                  GROUP BY v_1.id) v
          WHERE (v.product_id = p.id)) AS variants
   FROM (public.product p
     LEFT JOIN public.product_category c ON (((p.category_code)::text = (c.code)::text)))
  GROUP BY p.id
  ORDER BY p.id;


--
-- Name: vw_product_attributes _RETURN; Type: RULE; Schema: public; Owner: expo
--

CREATE OR REPLACE VIEW public.vw_product_attributes AS
 SELECT pa.id,
    pa.code,
    pa.description,
    pa.name,
    pa.type,
    COALESCE(json_agg(json_build_object('id', pav.id, 'code', pav.code, 'value', pav.value, 'color', pav.color) ORDER BY pav."position", pav.id) FILTER (WHERE (pav.id IS NOT NULL)), '[]'::json) AS "values"
   FROM (public.product_attribute pa
     LEFT JOIN public.product_attribute_value pav ON ((pav.attribute_id = pa.id)))
  GROUP BY pa.id;


--
-- Name: vw_product_variants _RETURN; Type: RULE; Schema: public; Owner: expo
--

CREATE OR REPLACE VIEW public.vw_product_variants AS
 SELECT p.id AS product_id,
    v.id AS variant_id,
    concat(p.name, COALESCE(NULLIF(max((v.name)::text), ''::text), ((
        CASE
            WHEN (v.id IS NULL) THEN ''::text
            ELSE concat(' - ', string_agg(attv.value, ','::text))
        END)::character varying)::text)) AS full_name,
    COALESCE((v.name)::text, ((string_agg(attv.value, ','::text))::character varying)::text) AS variant_name,
    v.name,
    v.sku,
    v.code AS variant_code,
    c.rate AS tax_rate,
    (c.name)::text AS category,
    p.pasteque_product_id,
    p.version,
    p.number,
    p.category_code,
    p.criteria,
    p.name AS product_name,
    p.sku AS product_sku,
    p.supplier,
    p.barcode,
    p.price,
    p.import_date,
    p.attributes,
    p.label,
    p.active,
    p.id,
    COALESCE(max(attv."position"), v.id) AS "position",
    json_agg(json_build_object('attribute_name', att.name, 'attribute_type', att.type, 'attribute_id', att.id, 'attribute_code', att.code, 'value_id', attv.id, 'value_code', attv.code, 'id', av.id, 'value', attv.value, 'color',
        CASE att.type
            WHEN 'color'::text THEN attv.color
            ELSE NULL::character varying
        END) ORDER BY attv."position", attv.id) FILTER (WHERE (attv.id IS NOT NULL)) AS attribute_values,
    ( SELECT (EXISTS ( SELECT pp.id
                   FROM pasteque.products pp
                  WHERE (pp.id = p.number))) AS "exists") AS has_pasteque
   FROM (((((public.product p
     LEFT JOIN public.product_variant v ON ((v.product_id = p.id)))
     LEFT JOIN public.product_variant_attribute_value av ON ((av.variant_id = v.id)))
     LEFT JOIN public.product_attribute_value attv ON ((av.attribute_value_id = attv.id)))
     LEFT JOIN public.product_attribute att ON ((att.id = av.attribute_id)))
     LEFT JOIN public.product_category c ON (((p.category_code)::text = (c.code)::text)))
  GROUP BY p.id, v.id, v.name, v.code, c.rate, c.name
  ORDER BY p.number, v.id;


--
-- Name: customer audit_trigger_row; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.customer FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: inventory audit_trigger_row; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.inventory FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: payment_method audit_trigger_row; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.payment_method FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: product audit_trigger_row; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.product FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: product_attribute audit_trigger_row; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.product_attribute FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: product_attribute_value audit_trigger_row; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.product_attribute_value FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: product_category audit_trigger_row; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.product_category FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: product_variant audit_trigger_row; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.product_variant FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: product_variant_attribute_value audit_trigger_row; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.product_variant_attribute_value FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: receipt audit_trigger_row; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.receipt FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: report audit_trigger_row; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.report FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: sales_journal audit_trigger_row; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.sales_journal FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: sales_journal_entry audit_trigger_row; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.sales_journal_entry FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: stock audit_trigger_row; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.stock FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: stock_count audit_trigger_row; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.stock_count FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: stock_item audit_trigger_row; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.stock_item FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: stock_movement audit_trigger_row; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_row AFTER INSERT OR DELETE OR UPDATE ON public.stock_movement FOR EACH ROW EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: customer audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.customer FOR EACH STATEMENT EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: inventory audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.inventory FOR EACH STATEMENT EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: payment_method audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.payment_method FOR EACH STATEMENT EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: product audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.product FOR EACH STATEMENT EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: product_attribute audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.product_attribute FOR EACH STATEMENT EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: product_attribute_value audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.product_attribute_value FOR EACH STATEMENT EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: product_category audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.product_category FOR EACH STATEMENT EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: product_variant audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.product_variant FOR EACH STATEMENT EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: product_variant_attribute_value audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.product_variant_attribute_value FOR EACH STATEMENT EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: receipt audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.receipt FOR EACH STATEMENT EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: report audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.report FOR EACH STATEMENT EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: sales_journal audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.sales_journal FOR EACH STATEMENT EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: sales_journal_entry audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.sales_journal_entry FOR EACH STATEMENT EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: stock audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.stock FOR EACH STATEMENT EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: stock_count audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.stock_count FOR EACH STATEMENT EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: stock_item audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.stock_item FOR EACH STATEMENT EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: stock_movement audit_trigger_stm; Type: TRIGGER; Schema: public; Owner: expo
--

CREATE TRIGGER audit_trigger_stm AFTER TRUNCATE ON public.stock_movement FOR EACH STATEMENT EXECUTE PROCEDURE audit.if_modified_func('true');


--
-- Name: product_attribute_value product_attribute_value_product_attribute_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.product_attribute_value
    ADD CONSTRAINT product_attribute_value_product_attribute_id_fkey FOREIGN KEY (attribute_id) REFERENCES public.product_attribute(id) ON DELETE CASCADE;


--
-- Name: product product_category_code_fkey; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_category_code_fkey FOREIGN KEY (category_code) REFERENCES public.product_category(code);


--
-- Name: product_variant_attribute_value product_variant_attribute_value_attribute_value_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.product_variant_attribute_value
    ADD CONSTRAINT product_variant_attribute_value_attribute_value_id_fkey FOREIGN KEY (attribute_value_id, attribute_id) REFERENCES public.product_attribute_value(id, attribute_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: product_variant_attribute_value product_variant_attribute_value_product_variant_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.product_variant_attribute_value
    ADD CONSTRAINT product_variant_attribute_value_product_variant_id_fkey FOREIGN KEY (variant_id) REFERENCES public.product_variant(id) ON DELETE CASCADE;


--
-- Name: product_variant product_variant_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.product_variant
    ADD CONSTRAINT product_variant_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.product(id) ON DELETE CASCADE;


--
-- Name: receipt receipt_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.receipt
    ADD CONSTRAINT receipt_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES public.customer(id);


--
-- Name: sales_journal_entry sales_journal_entry_payment_method_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.sales_journal_entry
    ADD CONSTRAINT sales_journal_entry_payment_method_id_fkey FOREIGN KEY (payment_method_id) REFERENCES public.payment_method(id);


--
-- Name: sales_journal_entry sales_journal_entry_sales_journal_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.sales_journal_entry
    ADD CONSTRAINT sales_journal_entry_sales_journal_id_fkey FOREIGN KEY (sales_journal_id) REFERENCES public.sales_journal(id);


--
-- Name: stock_adjustment stock_adjustment_fk_operation; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_adjustment
    ADD CONSTRAINT stock_adjustment_fk_operation FOREIGN KEY (operation_id) REFERENCES public.stock_operation(id);


--
-- Name: stock_adjustment stock_adjustment_fk_product; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_adjustment
    ADD CONSTRAINT stock_adjustment_fk_product FOREIGN KEY (product_id) REFERENCES public.product(id);


--
-- Name: stock_adjustment stock_adjustment_fk_product_variant; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_adjustment
    ADD CONSTRAINT stock_adjustment_fk_product_variant FOREIGN KEY (variant_id) REFERENCES public.product_variant(id);


--
-- Name: stock_adjustment stock_adjustment_fk_stock_item; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_adjustment
    ADD CONSTRAINT stock_adjustment_fk_stock_item FOREIGN KEY (stock_item_id) REFERENCES public.stock_item(id) ON DELETE SET NULL;


--
-- Name: stock_adjustment stock_adjustment_fk_stock_item_variant; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_adjustment
    ADD CONSTRAINT stock_adjustment_fk_stock_item_variant FOREIGN KEY (stock_item_variant_id) REFERENCES public.stock_item_variant(id) ON DELETE SET NULL;


--
-- Name: stock_count stock_count_fk_operation_id; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_count
    ADD CONSTRAINT stock_count_fk_operation_id FOREIGN KEY (operation_id) REFERENCES public.stock_operation(id);


--
-- Name: stock_count stock_count_inventory_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_count
    ADD CONSTRAINT stock_count_inventory_id_fkey FOREIGN KEY (inventory_id) REFERENCES public.inventory(id) ON DELETE SET NULL;


--
-- Name: stock_count_item stock_count_item_fk_count_id; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_count_item
    ADD CONSTRAINT stock_count_item_fk_count_id FOREIGN KEY (stock_count_id) REFERENCES public.stock_count(id) ON DELETE CASCADE;


--
-- Name: stock_count_item stock_count_item_fk_stock_id; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_count_item
    ADD CONSTRAINT stock_count_item_fk_stock_id FOREIGN KEY (stock_id) REFERENCES public.stock(id) ON DELETE CASCADE;


--
-- Name: stock_count_item stock_count_item_fk_stock_item; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_count_item
    ADD CONSTRAINT stock_count_item_fk_stock_item FOREIGN KEY (stock_item_id) REFERENCES public.stock_item(id);


--
-- Name: stock_count_item stock_count_item_fk_stock_item_variant; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_count_item
    ADD CONSTRAINT stock_count_item_fk_stock_item_variant FOREIGN KEY (stock_item_variant_id) REFERENCES public.stock_item_variant(id);


--
-- Name: stock_count stock_count_stock_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_count
    ADD CONSTRAINT stock_count_stock_id_fkey FOREIGN KEY (stock_id) REFERENCES public.stock(id);


--
-- Name: stock_item_variant stock_item_variant_fk_product_variant; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_item_variant
    ADD CONSTRAINT stock_item_variant_fk_product_variant FOREIGN KEY (variant_id) REFERENCES public.product_variant(id) ON DELETE CASCADE;


--
-- Name: stock_item_variant stock_item_variant_fk_stock_item; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_item_variant
    ADD CONSTRAINT stock_item_variant_fk_stock_item FOREIGN KEY (stock_item_id) REFERENCES public.stock_item(id) ON DELETE CASCADE;


--
-- Name: stock_movement stock_movement_fk_destination_operation_id; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_movement
    ADD CONSTRAINT stock_movement_fk_destination_operation_id FOREIGN KEY (destination_operation_id) REFERENCES public.stock_operation(id);


--
-- Name: stock_movement stock_movement_fk_destination_stock_id; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_movement
    ADD CONSTRAINT stock_movement_fk_destination_stock_id FOREIGN KEY (destination_stock_id) REFERENCES public.stock(id);


--
-- Name: stock_movement stock_movement_fk_operation_id; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_movement
    ADD CONSTRAINT stock_movement_fk_operation_id FOREIGN KEY (operation_id) REFERENCES public.stock_operation(id);


--
-- Name: stock_movement_item stock_movement_item_fk_movement_id; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_movement_item
    ADD CONSTRAINT stock_movement_item_fk_movement_id FOREIGN KEY (stock_movement_id) REFERENCES public.stock_movement(id) ON DELETE CASCADE;


--
-- Name: stock_movement_item stock_movement_item_fk_stock_id; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_movement_item
    ADD CONSTRAINT stock_movement_item_fk_stock_id FOREIGN KEY (stock_id) REFERENCES public.stock(id) ON DELETE CASCADE;


--
-- Name: stock_movement_item stock_movement_item_fk_stock_item; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_movement_item
    ADD CONSTRAINT stock_movement_item_fk_stock_item FOREIGN KEY (stock_item_id) REFERENCES public.stock_item(id);


--
-- Name: stock_movement_item stock_movement_item_fk_stock_item_variant; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_movement_item
    ADD CONSTRAINT stock_movement_item_fk_stock_item_variant FOREIGN KEY (stock_item_variant_id) REFERENCES public.stock_item_variant(id);


--
-- Name: stock_movement stock_movement_stock_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_movement
    ADD CONSTRAINT stock_movement_stock_id_fkey FOREIGN KEY (stock_id) REFERENCES public.stock(id);


--
-- Name: stock_operation stock_operation_fk; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_operation
    ADD CONSTRAINT stock_operation_fk FOREIGN KEY (stock_id) REFERENCES public.stock(id);


--
-- Name: stock_item stock_product_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_item
    ADD CONSTRAINT stock_product_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.product(id) ON DELETE CASCADE;


--
-- Name: stock_item stock_product_stock_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.stock_item
    ADD CONSTRAINT stock_product_stock_id_fkey FOREIGN KEY (stock_id) REFERENCES public.stock(id) ON DELETE CASCADE;


--
-- Name: webshop_order_meta webshop_order_meta_fk; Type: FK CONSTRAINT; Schema: public; Owner: expo
--

ALTER TABLE ONLY public.webshop_order_meta
    ADD CONSTRAINT webshop_order_meta_fk FOREIGN KEY (group_id) REFERENCES public.webshop_order_group(id);


--
-- Name: FOREIGN DATA WRAPPER postgres_fdw; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON FOREIGN DATA WRAPPER postgres_fdw TO expo;


--
-- Name: FOREIGN SERVER local_pasteque; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON FOREIGN SERVER local_pasteque TO expo;


--
-- Name: TABLE cashregisters; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.cashregisters TO expo;


--
-- Name: TABLE categories; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.categories TO expo;


--
-- Name: TABLE compositiongroups; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.compositiongroups TO expo;


--
-- Name: TABLE compositionproducts; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.compositionproducts TO expo;


--
-- Name: TABLE currencies; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.currencies TO expo;


--
-- Name: TABLE customers; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.customers TO expo;


--
-- Name: TABLE discountprofiles; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.discountprofiles TO expo;


--
-- Name: TABLE discounts; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.discounts TO expo;


--
-- Name: TABLE fiscaltickets; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.fiscaltickets TO expo;


--
-- Name: TABLE floors; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.floors TO expo;


--
-- Name: TABLE images; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.images TO expo;


--
-- Name: TABLE options; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.options TO expo;


--
-- Name: TABLE paymentmodereturns; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.paymentmodereturns TO expo;


--
-- Name: TABLE paymentmodes; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.paymentmodes TO expo;


--
-- Name: TABLE paymentmodevalues; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.paymentmodevalues TO expo;


--
-- Name: TABLE places; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.places TO expo;


--
-- Name: TABLE products; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE pasteque.products TO expo;


--
-- Name: TABLE resources; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.resources TO expo;


--
-- Name: TABLE roles; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.roles TO expo;


--
-- Name: TABLE sessioncats; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.sessioncats TO expo;


--
-- Name: TABLE sessioncattaxes; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.sessioncattaxes TO expo;


--
-- Name: TABLE sessioncustbalances; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.sessioncustbalances TO expo;


--
-- Name: TABLE sessionpayments; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.sessionpayments TO expo;


--
-- Name: TABLE sessions; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.sessions TO expo;


--
-- Name: TABLE sessiontaxes; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.sessiontaxes TO expo;


--
-- Name: TABLE tariffareaprices; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.tariffareaprices TO expo;


--
-- Name: TABLE tariffareas; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.tariffareas TO expo;


--
-- Name: TABLE taxes; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.taxes TO expo;


--
-- Name: TABLE ticketlines; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.ticketlines TO expo;


--
-- Name: TABLE ticketpayments; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.ticketpayments TO expo;


--
-- Name: TABLE tickets; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.tickets TO expo;


--
-- Name: TABLE tickettaxes; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.tickettaxes TO expo;


--
-- Name: TABLE users; Type: ACL; Schema: pasteque; Owner: postgres
--

GRANT SELECT ON TABLE pasteque.users TO expo;


--
-- PostgreSQL database dump complete
--


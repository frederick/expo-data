import { Pool, Client, PoolClient } from 'pg';
import Knex from 'knex';

export const knex = Knex({
	client: 'postgres',
	connection: {
		host: process.env.PGHOST,
		port: parseInt(process.env.PGPORT?? '5432'),
		database: process.env.PGDATABASE,
		user: process.env.PGUSER,
		password: process.env.PGPASSWORD
	}
});

export default knex;
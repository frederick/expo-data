const parseCSV = require('csv-parse/lib/sync')

function readTAList(str) {
    let lines = str.split('\n').filter(l => l.trim().length > 0)
    if (lines[0].indexOf('TITRE') == 0) {
        // .. metadata
        lines = lines.slice(1);
    }
   
    const data = parseCSV(lines.join('\n'), {
        delimiter: str.indexOf('\t') > -1 ? '\t' : ';',
        columns: true
    });

    return data

}

async function combineTAListWithWebshopData(list) {
    const { query } = require('./client');
    const data = readTAList(list)
    if (data.length === 0) {
        return []
    }
    const { result: orders } = await query(`
        SELECT * FROM sales_flat_order
        WHERE increment_id IN (?)
    `, [data.map(de => parseInt(de.ORDER_ID))])
    for (let d of data) {
        d.order = orders.find(o => parseInt(o.increment_id) == parseInt(d.ORDER_ID))
    }

    return data

}

module.exports = {
    combineTAListWithWebshopData,
    readTAList
}
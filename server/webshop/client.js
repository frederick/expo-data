const mysql = require('mysql');
function createConnection() {
    const con = mysql.createConnection({
        host: process.env.WEBSHOP_MYSQL_HOST,
        port: parseInt(process.env.WEBSHOP_MYSQL_PORT),
        database: process.env.WEBSHOP_MYSQL_DATABASE,
        user: process.env.WEBSHOP_MYSQL_USER,
        password: process.env.WEBSHOP_MYSQL_PASSWORD,
        timezone: 'UTC'
    })
    return con
}

async function query(query, values) {
    const con = createConnection();
    return new Promise((resolve, reject) => {
        con.query(query, values, (err, result, fields) => {
            con.end();
            if (err) {
                return reject(err);
            }

            resolve({ result, fields });
        })
    })
}

module.exports = {
    createConnection,
    query
}
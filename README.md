# Expo-data

## Overview
**expo-data** is a product and inventory management application which can be used together with pasteque (https://framagit.org/pasteque).
Mainly created for internal use, some of its features may be of interest to others.

## Getting started
**expo-data** stores information in its own PostgreSQL database. Its product database can then be synchronized with a pasteque database.
1. Create a database and a user "expo" on postgresql server of your choice (12 and 13 should work fine)
2. Create a server/.env file based on the .env.sample file
3. Import the sql schema dump in server/db/expo.sql
  -  :information_source: For SQL reports and product synchronization, expo-data needs direct access to a pasteque postgresql database. This connection is created within the SQL script expo.sql. You can find it within the script by searching for `CREATE SERVER local_pasteque`. You can then change the username for the user to use to connect to the database (if it is remote database you also need to add a password). For a local database you can use the same user as you use for the expo database. You then need to grant the specified user read access to the pasteque database and write access to the product table (arwd).
4. Import some sample data server/db/example.sql
5. Build the project: `docker build -t expo-data .`
6. Run the project: `docker run --env-file=server/.env --rm -p 3000:3000 expo-data`
7. Create an access token for a sample user with admin privileges: `cd server; node utils/createToken.js`
8. Access the project in a web browser under http://localhost:3000/expo-data and paste the token